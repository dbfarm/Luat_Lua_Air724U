# LuatOS-Air

> ！！重要更新！！：
* 1、 [LuatOS-Air-iCool](https://gitee.com/openLuat/iCool)项目合并到product目录，不再单独维护，后续会开放更多有趣的产品 :sunglasses: 
* 2、Luat_lua_Air724项目更名为LuatOS-Air 
* 3、 **发行版本为稳定发布版本，可用于量产** 


## 一、介绍

本项目是基于合宙Cat1模块的Lua语言开发环境、开发简单快速、上手方便、无需学习复杂的语法。相较于传统**MCU+蜂窝模组**开发方式，有如下优势：
* 1，替代**MCU+蜂窝模组**的架构，只需要蜂窝模组, 并最大限度发挥蜂窝模组的各项功能；
* 2，相较于普通MCU，模组自带的处理器性能更强，外设更丰富；
* 3，功能完善：支持常见通信协议、云平台接入、常用外设和传感器、FOTA等常用功能；
* 4，丰富例程，完善的注释和文章指导，开发更加容易上手；
* 5，Lua脚本更加高效，无需编译直接运行，提高开发效率；
* 6，无需处理复杂AT逻辑，Lua API接口更符合程序开发思维。

### 1、LuatIDE
LuatIDE告别了传统开发，多工具来回切换的开发方式。**一个工具涵盖了编辑、下载、运行、单步调试、日志输出、工程管理等众多功能。** 

目前处于公测阶段欢迎大家[下载LuatIDE]((https://marketplace.visualstudio.com/items?itemName=luater.luatide) )使用。

* 公测沟通反馈群：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/180739_a25e320f_1221708.png "B633DFF2-5F8C-4595-B0EF-1344A14B017B.png")

### 2、软件框架：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/154843_c8ee5c92_1221708.png "WPS图片-修改尺寸(1).png")
## 二、常用功能列表

* 1，支持所有主流通信协议，包括： TCP，UDP，HTTP，HTTPS,FTP，MQTT,COAP,LWM2M;
* 2，适配所有主流的传感器和外设；
* 3，适配所有主流的云平台：阿里云，腾讯云，华为云，onenet，机智云，电信云，并提供私有云搭建的示例；
* 4，支持两种UI开发框架：DISP（极简），[LVGL](http://doc.openluat.com/article/1246/0)（功能完善）
* 5，集成了加密算法；
* 6，支持远程升级；
* 7，支持远程调试；
* 8，支持定位服务；
* 9，使用Lua 脚本开发业务逻辑，学习和调试成本都非常低，并且运行效率高；
* 10，工具链完善，包括项目管理，调试工具，烧录工具，都是简单易用的；
* 11，支持[CORE固件在线定制](https://doc.openluat.com/article/2728)功能，自由组合最大限度利用模块内存。

## 三、 CORE固件
CORE固件是C语言编写的Lua虚拟机运行环境，基于官方的Lua5.1版本，增加了大量符合蜂窝通信模组的新特性。CORE固件可以有如下两种获取方式：
#### 1. 在线定制固件
在线定制固件是根据`CORE`已经支持的功能列表，按照客户产品功能需求进行在线固件定制，在线定制最大限度的保持了lua运行的空间，理论上和官方发布的固件一样稳定，而且和官方固件一样支持更新和FOTA升级，[具体介绍和使用参考可选编译使用说明。](https://doc.openluat.com/article/2728)

#### 2. 官方发布固件

[合宙Air720U&724U&722U Lua 固件更新说明](https://doc.openluat.com/article/1334)

## 四、开发

### 1. 准备开发工具

[Luatools工具下载&介绍](https://doc.openluat.com/wiki/26?wiki_page_id=3065)

### 2. 下载core

具体修改记录见release notes.txt

### 3. 运行demo

用Luatools工具下载core文件和Lua工程目录中的`lib和相应demo文件`就可以运行了。

[LuatOS-Air脚本开发介绍](https://doc.openluat.com/wiki/27?wiki_page_id=3050)


## 五、部分产品展示
![输入图片说明](https://cdn.openluat-luatcommunity.openluat.com/images/20211118144559891_20210928143044425_Idle.png "iCool手机主界面展示")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/154541_c5199189_1221708.png "对讲机-格式.png")![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/154609_d601f1e4_1221708.png "扫码充电-格式.png")


## 六、相关链接

* [LuatIDE](https://doc.openluat.com/wiki/26?wiki_page_id=3064) 基于VSCode的Luat集成开发环境，包含自动提示、下载脚本、调试等功能
* [合宙官网](http://www.openluat.com)
* [合宙商城](https://luat.taobao.com) 模块,开发板,外设,均有销售
* [LuatOS](https://gitee.com/openLuat/LuatOS) LuatOS是运行在嵌入式硬件,用户编写lua代码就可完成各种功能
* [CSDK](https://gitee.com/openLuat/Luat_CSDK_Air724U) 针对使用合宙Cat1模块而准备的一套C语言的软件开发环境
* [Lua版本LittleVGL开发介绍](http://doc.openluat.com/article/1246/0)
* [Air724UG模块自带蓝牙Lua开发指南](http://doc.openluat.com/article/1598/0) 
* [LuatOS-Air二次开发教程](https://doc.openluat.com/wiki/26) 手把手入门教程

## 七、授权协议

[MIT License](LICENSE)

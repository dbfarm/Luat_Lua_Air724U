## uart.setup( id, baud, databits, parity, stopbits,[msgmode],[txDoneReport],[flowcontrol] )

uart初始化配置

**参数**

|参数|类型|释义|取值|
|-|-|-|-|
|id|number|串口号|1(UART1),2(UART2),3(UART3),0x81(USB)|
|baud|number|波特率|可选1200，2400，4800，9600，10400，14400，19200，28800，38400，230400，460800，921600|
|databits|number|数据位|只有一个取值8|
|parity|number|校验位|取值uart.PAR_EVEN, uart.PAR_ODD或uart.PAR_NONE|
|stopbits|number|停止位|取值uart.STOP_1，uart.STOP_2|
|msgmode|number|0 或者默认 - 消息通知，1 - 无消息上报需要用户主动轮询|0/1|
|txDoneReport|number|txdone消息上报开关|0：关闭，1：打开|
|flowcontrol|number|硬流控功能|0：关闭(默认)，1：打开|

**返回值**

|返回值|类型|释义|取值|
|-|-|-|-|
|result|number| 串口的真实波特率|  |

---
# 其他接口见:
https://doc.openluat.com/wiki/21?wiki_page_id=2250
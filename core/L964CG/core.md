## 一、介绍
CORE固件是C语言编写的Lua虚拟机运行环境，基于官方的Lua5.1版本，增加了大量符合蜂窝通信模组的新特性。
## 二、如何获取CORE固件
### 2.1，使用官方发布的固件
#### 最新版本

 版本名 | 特色功能
---|---
 [LuatOS-Air_V1005_ASR1603E_TTS_VOLTE_FLOAT](http://openluat-erp.oss-cn-hangzhou.aliyuncs.com/erp_site_file/product_file/[%E9%9D%9E%E9%87%8F%E4%BA%A7%E7%89%88%E6%9C%AC]sw_file_20210918185125_LuatOS-Air_V1005_ASR1603E_TTS_VOLTE_FLOAT.zip)  | 电话,浮点,LCD,AUDIO,TTS(需要1603E) 
[LuatOS-Air_V1005_ASR1603E_FLOAT](http://openluat-erp.oss-cn-hangzhou.aliyuncs.com/erp_site_file/product_file/[%E9%9D%9E%E9%87%8F%E4%BA%A7%E7%89%88%E6%9C%AC]sw_file_20210918184015_LuatOS-Air_V1005_ASR1603S_FLOAT.zip)| 浮点运算(需要1603S) 

#### 发布说明

见同级目录下的:
**Release_Notes_LuatOS-Air_ASR1603_VXXXX.xls 文件**
    




## 三、下载固件
下载固件通过Luatools工具，选择不同途径获取的文件，解压后选择`.zip`文件，再选择对应的lua脚本进行下载。[具体步骤，参考Luatools使用手册](https://doc.openluat.com/wiki/3?wiki_page_id=701)
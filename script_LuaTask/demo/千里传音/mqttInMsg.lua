--- 模块功能：MQTT客户端数据接收处理
-- @author openLuat
-- @module mqtt.mqttInMsg
-- @license MIT
-- @copyright openLuat
-- @release 2018.03.28
module(...,package.seeall)
require "mqttOutMsg"
require  "http"
require "audioMng"

-------------------添加路径--------------------------

--local USER_DIR_PATH = "/user_dir"
local USER_DIR_PATH = "/lua"
--rtos.make_dir(USER_DIR_PATH)

-----------------添加路径----------------------------
---------------获取table的长度/或者每一项的长度--------
local function table_leng(t) --获取table的长度
    local leng = 0
    for k, v in pairs(t) do leng = leng + 1 end
    return leng;
end
---------------获取table的长度/或者每一项的长度--------

-----------------读取下载之后的JSON数据内容，并在完成之后删除掉----------------------
local function readfile(filename)--打开指定文件并输出内容

    local filehandle=io.open(filename,"r")--第一个参数是文件名，第二个是打开方式，'r'读模式,'w'写模式，对数据进行覆盖,'a'附加模式,'b'加在模式后面表示以二进制形式打开
    if filehandle then          --判断文件是否存在
        local fileval=filehandle:read("*all")--读出文件内容
      if  fileval  then
           local tjiondata,result=json.decode(fileval)
           if result then
              return tjiondata
           else
              log.info("readfile json fail")
           end
           --print(fileval)  --如果文件存在，打印文件内容
           filehandle:close()--关闭文件
           os.remove(filename)
      else
           print("文件为空")--文件不存在
      end
    else
        print("文件不存在或文件输入格式不正确") --打开失败
    end

end
-----------------读取下载之后的JSON数据内容，并在完成之后删除掉----------------------


local function moneyStrToAmrFile(castType,money)
    local broadcastType=castType
    local tAudioFile = {}
    local tdownFile={"SYS_TONE_0","SYS_TONE_1","SYS_TONE_2","SYS_TONE_3","SYS_TONE_4","SYS_TONE_5","SYS_TONE_6","SYS_TONE_7","SYS_TONE_8","SYS_TONE_9"}
    if money then    
        local integerStr,decimalStr = "0",""
        local integer ="0"
        log.info("mony",money,type(money))
        if money:match("%.") then
            integer,decimalStr = money:match("(%d*)%.(%d*)")
            if integer=="" then integer="0" end     
        else
            integer = money
        end
        log.info("integer",integer,type(integer)) 

        for i=1,integer:len() do
            table.insert(tAudioFile,USER_DIR_PATH.."/"..tdownFile[integer:sub(i,i)+1].."."..broadcastType)
        end

        if #tAudioFile==0 then table.insert(tAudioFile,USER_DIR_PATH.."/"..tdownFile[1].."."..broadcastType) end
        --小数部分
        if decimalStr~="" then
            table.insert(tAudioFile,USER_DIR_PATH.."/".."SYS_TONE_dian".."."..broadcastType)
            for i=1,decimalStr:len() do
                table.insert(tAudioFile,USER_DIR_PATH.."/"..tdownFile[decimalStr:sub(i,i)+1].."."..broadcastType)
            end
        end
    end
    return tAudioFile
end

local tMessageId = {}

--插入成功返回true
--如果重复表示插入失败，返回false
--最多存储最近50条
local function insertMessageId(id)
    for i=#tMessageId,1,-1 do
        if id==tMessageId[i] then
            return false
        end
    end
    if #tMessageId>=50 then
        table.remove(tMessageId,1)
    end
    table.insert(tMessageId,id)
    return true
end

local function procMsg(unpackData)
    local requestId = unpackData.params.id
    local format_type = unpackData.params.format
    local speechs_len = table_leng(unpackData.params.speechs)
    -- 判断是否为重复消息
    if not insertMessageId(requestId) then
        log.warn("mqttInMsg.procMsg", "dup request id")
        return
    end
    for i = 1, speechs_len, 1 do
        local money = unpackData.params.speechs[i]
        if money then
            -- 格式化收款金额到小数点后两位
            local integer, fraction = money:match("(%d+)%.(%d+)")
            if fraction then
                local cnt = 0
                if fraction:len() > 2 then
                    fraction = fraction:sub(1, 2)
                end
                for i = fraction:len(), 1, -1 do
                    if fraction:sub(i, i) == "0" then
                        cnt = cnt + 1
                    else
                        break
                    end
                end
                fraction = fraction:sub(1, fraction:len() - cnt)
                money = integer ..((fraction:len() > 0) and ("." .. fraction) or "")
            end

            if not audioMng.isBufferFull() then
                sys.publish("TTS_ALIYUN_NEW_REQ", moneyStrToAmrFile(format_type, money), "MONEY")
                --moneyStrToAmrFile(format_type, money)
            end

        end
    end
end









local function FunctionGetName(result,prompt,head,rcvFileName)
    if result then
        log.info("filename",rcvFileName)
    else
        log.info("FunctionGetName  http fail",rcvFileName)
    end
end












local function isFunctioncbFnc(result,prompt,head,rcvFileName)
    if result then
        local strdata=readfile(rcvFileName)
        
        sys.publish("http_down_event",strdata)
    else
        log.info("http down fail")
    end
end


local function SpeechList(audiosize,audioType,audioid)
    local torigin={
        params={SpeechList={{
            size=audiosize,
            format=audioType,
            id=audioid
        }}}
    }
    local  jsondata=json.encode(torigin)
    sys.publish("/sys/a13ALVjv53M/pehzpBLGD5ore8uzsT1t/thing/event/property/post",jsondata,0)
    
end

--------------------------------下载每个音频-----------------------------------------------------
sys.taskInit(function ()
      while true do
        local result,Urlstrdta=sys.waitUntil("http_down_event")
        log.info("http_down_event",result)
        local audioslen= table_leng(Urlstrdta.audios)
        for i = 1,audioslen, 1 do
            local url,id,format,size=Urlstrdta.audios[i].url,Urlstrdta.audios[i].id,Urlstrdta.audios[i].format,Urlstrdta.audios[i].size
            http.request("GET",url,nil,nil,nil,30000,FunctionGetName, USER_DIR_PATH.."/"..id.."."..format) 
            sys.wait(200) ---可以根据下载任务大小适当修改延时时间
            SpeechList(size,format,id)--根据下载上传语料表 每次上传一个
            sys.wait(200)
        end
        sys.wait(200)
      end
    
end)

--------------------------------下载每个音频-----------------------------------------------------





local function isFunction(subtopic,data)
    --/*+ add 语料推送数据处理 \wanglong\2022.9.24\*/
    if data.method=="thing.service.SpeechPost" then 
        local url=data.params.url
        local id=data.id
        local version=data.version
        local jobcodeid=data.params.jobcode
        http.request("GET",url,nil,nil,nil,30000, isFunctioncbFnc, "downfile.json")
        local torigih={params={jobcode=jobcodeid,result=0}} 
        local  jsondata=json.encode(torigih)
        ------------------语料推送事件回报，需要改成自己的设备，以及产品-------------------------------------------------------------------
        sys.publish("publish_thing_event","/sys/a13ALVjv53M/pehzpBLGD5ore8uzsT1t/thing/event/SpeechUpdateResponse/post",jsondata,0) 
       ------------------语料推送事件回报改成自己的设备，以及产品-------------------------------------------------------------------
    --/*- add 语料推送数据处理 \wanglong\2022.9.24\*/
    --/*+ add SyncSpeechBroadcast同步组合播报数据处理 \wanglong\2022.9.24\*/
    elseif  data.method == "thing.service.SyncSpeechBroadcast" then
        --local datatopic=subtopic
        local id1=data.id
        local task_id1=data.params.id
        local timestamp1=data.params.timestamp
        --log.info("subtopic",datatopic)
        --local Rrpctopic=string.gsub(datatopic,"/request/","/response/")
        local Rrpctopic="/sys/a13ALVjv53M/pehzpBLGD5ore8uzsT1t/thing/service/SyncSpeechBroadcast_reply"
        --------------------同步推送的回应--------------------------------------------------------------
        local RrpcPayload={
            code=200,
            data={
                result=0,
                task_id=task_id1,
            },
            id=id1
        }
        local RrpcPayloaden=json.encode(RrpcPayload)
        log.info("RRPC_TOPIC",RrpcPayloaden)
        sys.publish("publish_thing_event",Rrpctopic,RrpcPayloaden,0)
        --------------------同步推送的回应--------------------------------------------------------------   
        procMsg(data)
    --/*- add SyncSpeechBroadcast同步组合播报数据处理 \wanglong\2022.9.24\*/
    elseif data.method == "thing.service.SpeechBroadcast"  then
        procMsg(data)
    end
end



--- MQTT客户端数据接收处理
-- @param mqttClient，MQTT客户端对象
-- @return 处理成功返回true，处理出错返回false
-- @usage mqttInMsg.proc(mqttClient)
function proc(mqttClient)
    local result,data
    while true do
        result,data = mqttClient:receive(60000,"APP_SOCKET_SEND_DATA")
        --接收到数据
        if result then
            log.info("mqttclient",data.topic,data.payload)
            local tjdata,resultout=json.decode(data.payload)
            if resultout then
                isFunction(data.topic,tjdata)
            else
                break
            end    
            --TODO：根据需求自行处理data.payload
        else
            break
        end
    end
	
    return result or data=="timeout" or data=="APP_SOCKET_SEND_DATA"
end

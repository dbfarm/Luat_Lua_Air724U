--- 模块功能：音频功能测试.
-- @author openLuat
-- @module audio.testAudio
-- @license MIT
-- @copyright openLuat
-- @release 2018.03.19

module(...,package.seeall)
require"audio"
require"common"
require"nvm"
local tBuffer = {}

--uid存储最多50条
local UID_MAX_CNT = 50
--存储的已播放uid列表
local tUid = {}


function isUidExist(uid)
    for i=1,#tUid do
        if uid==tUid[i] then return true end
    end
end

--当前播放状态：
--IDLE：空闲状态
--MONEY：收款播报状态
--ADVERTISE：广告播放状态
--OTHER：其余播报状态
--STOPING：主动停止播报状态
local playStatus = "IDLE"

local tVol = {1,2,3,5,6}

sys.taskInit(function()
    while true do
        if #tBuffer>0 then
            local item = table.remove(tBuffer,1)
            if item.para and type(item.para)=="table" and isUidExist(item.para.uid) then
                log.warn("audioMng.dup")
            else                
                playStatus = item.typ or "OTHER"
                nvm.set("KeyplayStatus",playStatus)
                
                if item.para and type(item.para)=="table" and item.para.lcdShow then
                    local transactions = nvm.get("transaction")
                    sys.publish("SEG_LCD_SHOW_NUMBER_REQ",item.para.lcdShow,nil,#transactions)
                end
                local audioType = ((type(item.data)=="table") or (item.data:sub(-4,-1)==".amr") or (item.data:sub(-4,-1)==".mp3")) and "FILE" or "TTS"
                local audioData
                if type(item.data)=="table" then
                    audioData = item.data
                else
                    if item.data:sub(-4,-1)==".amr" or item.data:sub(-4,-1)==".mp3" then
                        if item.data:sub(1,1)=="/" then
                            audioData = item.data
                        else
                            audioData = "/lua/"..item.data
                        end
                    else
                        audioData = "[p100][n2]"..item.data.."[p100]"
                    end
                end
                audio.play(0,
                    audioType,
                    audioData,
                    tVol[nvm.get("volume")],
                    function(result)
                        sys.publish("TTS_PLAY_END")
                        log.info("audioMng.tuid play end",(type(item.para)=="table") and item.para.uid,result,#tUid)
                        if item.para then
                            if type(item.para)=="function" then
                                item.para(result==0)
                            else
                                if result==0 then
                                    if #tUid>=UID_MAX_CNT then
                                        table.remove(tUid,1)
                                    end
                                    table.insert(tUid,item.para.uid)
                                end                           
                            end
                        end
                    end)
                sys.waitUntil("TTS_PLAY_END")
            end
        else
            playStatus = "IDLE"
            sys.waitUntil("TTS_PLAY_NEW_REQ")
        end
    end
end)

function isBufferFull()
    return #tBuffer>=(UID_MAX_CNT+20)
end

local function insertItem(s,typ,para)
    table.insert(tBuffer,{data=s,typ=typ,para=para})
    if #tBuffer==1 and playStatus=="IDLE" then sys.publish("TTS_PLAY_NEW_REQ") end
end

--播放优先级规则：
--收款播报：缓存，顺序播报
--其余播报，新播报打断旧播报；如果有收款播报，则新播报直接丢弃
--uid：阿里云下载的收款播报的uid
sys.subscribe("TTS_ALIYUN_NEW_REQ",function(s,typ,para)
    if (type(s)=="table") or (s and s:len()>0) then        
        if typ=="MONEY" then
            --收款播报请求
            if playStatus=="IDLE" or playStatus=="MONEY" then
                insertItem(s,typ,para)
            elseif playStatus=="OTHER" then
                insertItem(s,typ,para)
            elseif playStatus=="STOPING" then
                insertItem(s,typ,para)
            end

            if s=="volSetMode.amr" then
                for i=#tBuffer-1,1,-1 do
                    if tBuffer[i].data=="volSetMode.amr" then
                        table.remove(tBuffer,i)
                    end
                end
            end
        else
            --其余播报请求
            if playStatus=="IDLE" then
                insertItem(s,typ,para)
            elseif playStatus=="MONEY" then
                
            elseif playStatus=="OTHER" then

            elseif playStatus=="STOPING" then
                local bHasMoney
                for i=#tBuffer,1,-1 do
                    if tBuffer[i].typ=="MONEY" then
                        bHasMoney=true
                    else
                        table.remove(tBuffer,i)
                    end
                end
                if not bHasMoney then
                    insertItem(s,typ,para)
                end
            end 
        end    
    end
end)

pcall(audiocore.pa,13,2)
audiocore.setpa(audiocore.CLASS_D)







module(..., package.seeall)

function create_bg_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))
    -- lvgl.style_set_bg_grad_color (s, lvgl.STATE_DEFAULT, lvgl.color_make(0x00, 0x00, 0x00))
    -- lvgl.style_set_bg_grad_dir   (s, lvgl.STATE_DEFAULT, lvgl.GRAD_DIR_VER)

    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.bg_opa or 0)

    lvgl.style_set_radius(s, lvgl.STATE_DEFAULT, v.radius or 20)
    lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color or 0xFFFFFF))
    lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.border_width or 0)
    lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.border_opa or 255)
    return s
end

function create_cell_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    lvgl.style_set_text_color(s, lvgl.STATE_DEFAULT,
                              lvgl.color_hex(v.font_color or 0xF5F5F5))
    lvgl.style_set_text_font(s, lvgl.STATE_DEFAULT, v.font or font24)

    return s
end

-- 表格点击事件回调
function event_handler(obj, event)
    res, row, col = lvgl.table_get_pressed_cell(obj)
    if (event == lvgl.EVENT_CLICKED) then
        log.info("table", event)

        log.info("table", row, col)
    end

end

function create(p, v)
    -- 创建一个 Table
    local t = lvgl.table_create(p, nil)
    -- 设置 Table 的行数
    lvgl.table_set_row_cnt(t, #v.content_array[1])
    -- 设置 Table 的列数
    lvgl.table_set_col_cnt(t, #v.content_array)

    lvgl.obj_set_event_cb(t, v.cb or event_handler)

    -- 设置单元格内容与第几种样式样式
    -- 第一种为默认样式，不特殊设置样式的单元格，默认为第一样式
    for i = 1, #v.content_array do
        for j = 1, #v.content_array[i] do
            lvgl.table_set_cell_value(t, j - 1, i - 1,
                                      v.content_array[i][j].text)
            lvgl.table_set_cell_type(t, j - 1, i - 1, v.content_array[i][j].type)
        end
    end

    -- 设置 Table 的显示大小
    -- lvgl.obj_set_size(t, v.W or 150, v.H or 60)
    -- 设置 Table 的位置
    lvgl.obj_align(t, v.align_to, v.align or lvgl.ALIGN_CENTER, v.align_x or 0,
                   v.align_y or 0)

    -- 添加样式
    if v.cell_style then
        for i = 1, #v.cell_style do
            lvgl.obj_add_style(t, v.cell_style[i].part,
                               create_cell_style(v.cell_style[i]))
        end
    end
    if v.bg_style then
        lvgl.obj_add_style(t, v.bg_style.part, create_bg_style(v.bg_style))
    end
    return t
end

module(..., package.seeall)

function create(p, v)
    local obj = lvgl.cpicker_create(p, nil)
    if not v then return obj end
    -- if not v.click then lvgl.obj_set_click(obj, false) end
    lvgl.obj_set_size(obj, v.W or 300, v.H or 300)

    --  lvgl.cpicker_set_type(obj,lvgl.CPICKER_TYPE_RECT/DISC) 更改颜色选择器的类型
    lvgl.cpicker_set_type(obj, lvgl.CPICKER_TYPE_RECT)
    -- lvgl.cpicker_set_type(obj, lvgl.CPICKER_TYPE_DISC)

    --  lvgl.cpicker_set_hue/saturation/value(obj,x) 手动设置colro，
    -- 或者使用 lvgl.cpicker_set_hsv(obj,hsv) 或 lvgl.cpicker_set_color(obj,rgb) 一次全部设置

    -- 色调H
    -- 用角度度量，取值范围为0°～360°，从红色开始按逆时针方向计算，红色为0°，绿色为120°,蓝色为240°。
    -- 它们的补色是：黄色为60°，青色为180°,紫色为300°；
    -- 饱和度S
    -- 饱和度S表示颜色接近光谱色的程度。一种颜色，可以看成是某种光谱色与白色混合的结果。
    -- 其中光谱色所占的比例愈大，颜色接近光谱色的程度就愈高，颜色的饱和度也就愈高。
    -- 饱和度高，颜色则深而艳。光谱色的白光成分为0，饱和度达到最高。通常取值范围为0%～100%，值越大，颜色越饱和。
    -- 明度V
    -- 明度表示颜色明亮的程度，对于光源色，明度值与发光体的光亮度有关；
    -- 对于物体色，此值和物体的透射比或反射比有关。通常取值范围为0%（黑）到100%（白）。

    -- lvgl.cpicker_set_hue(obj, 300)
    -- lvgl.cpicker_set_saturation(obj, 30)
    -- lvgl.cpicker_set_value(obj, 255)

    -- lvgl.cpicker_set_hsv(obj, lvgl.color_rgb_to_hsv(0xff, 0xff, 0))
    -- lvgl.cpicker_set_color(obj, lvgl.color_hsv_to_rgb(0, 50, 100))

    -- lvgl.cpicker_set_color_mode(obj,lvgl.CPICKER_COLOR_MODE_HUE/SATURATION/VALUE) 手动选择当前颜色。
    -- lvgl.CPICKER_COLOR_MODE_HUE
    -- lvgl.CPICKER_COLOR_MODE_SATURATION
    -- lvgl.CPICKER_COLOR_MODE_VALUE

    lvgl.cpicker_set_color_mode(obj, lvgl.CPICKER_COLOR_MODE_SATURATION)

    -- 固定颜色（不要长按更改）
    -- lvgl.cpicker_set_color_mode_fixed(obj, true)

    -- 使旋钮自动将所选颜色显示为背景色。
    lvgl.cpicker_set_knob_colored(obj, true)

    -- 拾色器的主要部分称为 lvgl.CPICKER_PART_BG 。
    -- 以圆形形式，它使用scale_width设置圆的宽度，并使用pad_inner在圆和内部预览圆之间填充。
    -- 在矩形模式下，半径可以用于在矩形上应用半径。

    -- 该对象具有称为的虚拟部分 lvgl.CPICKER_PART_KNOB ，它是在当前值上绘制的矩形（或圆形）。
    -- 它使用所有矩形（如样式属性和填充）使其大于圆形或矩形背景的宽度。
    if v.style then
        lvgl.obj_add_style(obj, lvgl.CPICKER_PART_MAIN, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.CPICKER_PART_KNOB,
                           create_style(v.style.knob))
    end

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    return obj
end

-- create(lvgl.scr_act(), {
--     style = {
--         bg = {bg = {radius = 10, color = 0x0000, opa = 255}},
--         knob = {
--             pad = {all = 40},
--             bg = {radius = 10, color = 0xff00ff, opa = 255}
--         }
--     }
-- })

module(..., package.seeall)

function del_shadow_cont(obj, event)
    if (event == lvgl.EVENT_CLICKED) then
        -- sys.publish("del_key_borad")
        -- sys.publish("del_shadow")
        lvgl.obj_del(shadow_cont)
    end
end

-- sys.subscribe("del_shadow", function() lvgl.obj_del(shadow_cont) end)

function create(v)
    _G.shadow_cont = cont.create(lvgl.scr_act(), {
        W = LCD_W,
        H = LCD_H,
        event = del_shadow_cont,
        click = true,
        style = {border = {width = 0}, bg = {color = 0, opa = 200}}
    })
    setting_btns_cont = cont.create(shadow_cont, v.cont or {
        W = LCD_W * 28 / 32,
        H = LCD_H * 5 / 8,
        click = false,
        style = {border = {width = 0}, bg = {color = 0x39dcc9, radius = 30}}
    })
    exit_btn = label.create(setting_btns_cont, {
        text = "X",
        align = lvgl.ALIGN_IN_TOP_RIGHT,
        align_x = -20,
        align_y = 10,
        click = true,
        event = del_shadow_cont,
        style = {
            border = {width = 0},
            text = {line_space = 0, font = style.font48}
        }
    })
    if v.content_fun then v.content_fun(setting_btns_cont, v.msg) end

end


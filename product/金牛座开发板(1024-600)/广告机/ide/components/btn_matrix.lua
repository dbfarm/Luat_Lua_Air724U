module(..., package.seeall)
-- 默认回调
local function event_handler(obj, event)
    if (event == lvgl.EVENT_VALUE_CHANGED) then
        txt = lvgl.btnmatrix_get_active_btn_text(obj)
        -- hhh = lvgl.btnmatrix_get_active_btn(obj)
        local temp = lvgl.btnmatrix_get_active_btn(obj)
        print("矩阵键盘默认回调")
        print(temp)
        print(txt)
    end
end

function create(p, v)
    local obj = lvgl.btnmatrix_create(p, nil)
    lvgl.obj_set_size(obj, v.W or 400, v.H or 200)
    lvgl.btnmatrix_set_recolor(obj, v.recolor or true)
    lvgl.btnmatrix_set_map(obj, v.btnArray or {"。", "。"})
    -- 设置btnMatrix的第九个按键所占宽度的比例
    -- lvgl.btnmatrix_set_btn_width(obj, 9, 3);
    -- lvgl.btnmatrix_set_btn_ctrl(obj, 9, lvgl.BTNMATRIX_CTRL_CHECKABLE);
    -- lvgl.btnmatrix_set_btn_ctrl(obj, 11, lvgl.BTNMATRIX_CTRL_CHECK_STATE);
    lvgl.obj_align(obj, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    lvgl.obj_set_event_cb(obj, v.cb or event_handler)

    lvgl.obj_add_style(obj, lvgl.BTNMATRIX_PART_BG,
                       create_style(v.style.bg))
    lvgl.obj_add_style(obj, lvgl.BTNMATRIX_PART_BTN,
                       create_style(v.style.btn))

    return btnMatrix
end
local btnMatrix_data = {
    btnArray = {
        "1", "2", "3", "\n", "4", "5", "6", "\n", "7", "8", "9", "\n", "0",
        "#FF0000 删除", ""
    },
    W = LCD_W,
    H = LCD_H / 2,
    recolor = true,
    -- cb = event_handler,
    style = {
        bg = {
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            }
        },
        btn = {
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            },
            text = {font = style.font48}
        }
    }
}
-- create(lvgl.scr_act(), btnMatrix_data)

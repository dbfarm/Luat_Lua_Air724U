module(..., package.seeall)

function create(p, v)
    obj = lvgl.switch_create(p, nil)
    lvgl.obj_set_click(obj, v.click or true)
    lvgl.obj_set_size(obj, v.W or 200, v.H or 100)

    if v.event then lvgl.obj_set_event_cb(obj, v.event) end

    --     变更状态
    -- 可以通过单击或通过下面的函数更改开关的状态：
    -- lvgl.switch_on(switch, lvgl.ANIM_ON/OFF) 开
    -- lvgl.switch_off(switch, lvgl.ANIM_ON/OFF) 关
    -- lvgl.switch_toggle(switch, lvgl.ANOM_ON/OFF) 切换开关的位置
    -- 动画时间
    -- 切换开关状态时的动画时间可以使用 lvgl.switch_set_anim_time(switch, anim_time) 进行调整。

    -- lvgl.switch_on(obj, lvgl.ANIM_ON)
    -- lvgl.switch_off(obj, lvgl.ANIM_ON)
    -- lvgl.switch_set_anim_time(obj, v.anim_time)

    -- lvgl.SWITCH_PART_BG : 主要部分
    -- lvgl.SWITCH_PART_INDIC : 指标（虚拟部分）
    -- lvgl.SWITCH_PART_KNOB : 旋钮（虚拟部分）
    -- 零件和样式与 滑杆(lvgl.slider) 情况相同。阅读其文档以获取详细说明。
    if v.style then
        lvgl.obj_add_style(obj, lvgl.SWITCH_PART_BG,
                           create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.SWITCH_PART_INDIC,
                           create_style(v.style.indic))
        lvgl.obj_add_style(obj, lvgl.SWITCH_PART_KNOB,
                           create_style(v.style.knob))
    end
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    return obj
end
switch_data = {
    W = 300,
    H = 100,
    style = {
        bg = {
            outline = {width = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 50, color = 0xff0000, grad = {color = 0x0f0f0f}}
        },
        indic = {
            outline = {width = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 10, color = 0x00ff00, grad = {color = 0x0f0f0f}}
        },
        knob = {
            pad = {all = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 50, color = 0x0000ff, grad = {color = 0x0f0f0f}}
        }
    }
}
-- create(lvgl.scr_act(), switch_data)

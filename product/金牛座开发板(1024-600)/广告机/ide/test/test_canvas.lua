module(..., package.seeall)
function stringTolist(str)
    str = str .. ""
    local num_data = {}
    for i = 1, string.len(str) do
        num_data[i] = {}
        num_data[i].num = tonumber(string.sub(str, i, i))
    end
    return num_data
end

function set_num_list(obj_list, num_list)
    num_list = stringTolist(num_list)
    local len = #num_list > #obj_list and #obj_list or #num_list
    for i = 1, len do set_num(obj_list[i].obj.obj_data, num_list[i].num) end
end

function set_num(obj_list, num)

    local data = num == 0 and num_data[10] or num_data[num]
    local negative_data = num == 0 and negative_num_data[10] or
                              negative_num_data[num]
    for i = 1, #negative_data do
        lvgl.obj_set_style_local_bg_opa(obj_list[negative_data[i]].obj,
                                        lvgl.CONT_PART_MAIN, lvgl.STATE_DEFAULT,
                                        0)
    end
    for i = 1, #data do
        lvgl.obj_set_style_local_bg_opa(obj_list[data[i]].obj,
                                        lvgl.CONT_PART_MAIN, lvgl.STATE_DEFAULT,
                                        255)
    end
end

function create(p, v)
    local parent = cont.create(p, v.p)
    local num_obj_data = {
        {align = lvgl.ALIGN_IN_TOP_MID, row = true},
        {align = lvgl.ALIGN_IN_BOTTOM_MID, row = true},
        {align = lvgl.ALIGN_CENTER, row = true},
        {align = lvgl.ALIGN_IN_TOP_LEFT}, {align = lvgl.ALIGN_IN_BOTTOM_LEFT},
        {align = lvgl.ALIGN_IN_TOP_RIGHT}, {align = lvgl.ALIGN_IN_BOTTOM_RIGHT}
    }

    -- 不透明部分显示数字
    num_data = {
        {6, 7}, {1, 2, 3, 5, 6}, {1, 2, 3, 6, 7}, {3, 4, 6, 7}, {1, 2, 3, 4, 7},
        {1, 2, 3, 4, 5, 7}, {1, 6, 7}, {1, 2, 3, 4, 5, 6, 7},
        {1, 2, 3, 4, 6, 7}, {1, 2, 4, 5, 6, 7}
    }
    -- 显示数字的相反数据，用来刷新残留数据
    negative_num_data = {
        {1, 2, 3, 4, 5}, {4, 7}, {4, 5}, {1, 2, 5}, {5, 6}, {6}, {2, 3, 4, 5},
        {}, {5}, {3}
    }
    if not v.partly then v.partly = 8 end
    s_h = lvgl.obj_get_height(parent) / (v.partly)
    s_w = lvgl.obj_get_width(parent)

    for i = 1, #num_obj_data do
        num_obj_data[i].obj = cont.create(parent, {
            W = num_obj_data[i].row and s_w or s_h,
            H = num_obj_data[i].row and s_h or s_h * (v.partly + 1) / 2,
            align = num_obj_data[i].align,
            style = {
                border = {width = 0},
                bg = {radius = 50, opa = 220, color = 0xff0f00}
            }
        })
    end
    if v.num then set_num(num_obj_data, v.num) end
    return {p = parent, obj_data = num_obj_data}
end

function create_p(p, v)
    if not v.inner then v.inner = 10 end
    if not v.W then v.W = 240 end
    if not v.H then v.H = 240 / 4 end
    local all_cont = cont.create(p, {
        W = 240,
        H = 240 / 3,
        layout = lvgl.LAYOUT_ROW_MID,
        fit = lvgl.FIT_TIGHT,
        align = v.align or lvgl.ALIGN_CENTER,
        style = {
            pad = {left = 0, right = 0, top = 0, bottom = 0, inner = v.inner},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 0, color = 0xff0ff0, opa = 0}
        }
    })

    local num_data = v.num and stringTolist(v.num) or {}
    local num_data_len = v.num and #num_data or v.len
    for i = 1, num_data_len do
        if not v.num then num_data[i] = {} end
        num_data[i].obj = create(all_cont, {
            partly = v.partly, -- 数值越大线越细
            num = num_data[i].num,
            p = {
                W = (v.W - ((num_data_len - 1) * v.inner)) / num_data_len or 20,
                H = v.H,
                -- align = num_data[i].align,
                click = false,
                style = {
                    margin = {left = 0, right = 0, top = 0, bottom = 0},
                    border = {width = 0},
                    bg = {radius = 0, opa = 0, color = 0x0f0ff0}
                }
            },
            s = {}
        })
    end
    return {p = all_cont, obj_data = num_data}
end

local date_obj_data = create_p(lvgl.scr_act(), {
    align = lvgl.ALIGN_IN_TOP_MID,
    num = "121311",
    partly = 12
})
local time_obj_data = create_p(lvgl.scr_act(), {len = 4, partly = 5})
local sec_obj_data = create_p(lvgl.scr_act(), {
    align = lvgl.ALIGN_IN_BOTTOM_MID,
    len = 2,
    partly = 7
})



function add_zero(v)
    if v < 10 then
        return "0" .. v
    else
        return v
    end
end



sys.taskInit(function()
    local index = 0
    local index_ = false
    while true do
        local t = os.date("*t")
        log.info("printTime",
                 string.format("%04d-%02d-%02d %02d:%02d:%02d", t.year, t.month,
                               t.day, t.hour, t.min, t.sec))

        set_num_list(sec_obj_data.obj_data, add_zero(t.sec))
        set_num_list(time_obj_data.obj_data, add_zero(t.hour) .. add_zero(t.min))
        set_num_list(date_obj_data.obj_data, add_zero(t.year % 100) ..
                         add_zero(t.mon) .. add_zero(t.day))

        -- for j = 1, #group_num_data do
        --     set_num(group_num_data[j].obj.obj_data_, (index + j) % 10)
        -- end
        index = index + 1
        sys.wait(300)
    end
end)

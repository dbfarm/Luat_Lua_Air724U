module(..., package.seeall)
---------------------------


local function gotoPage(page)
    _G.HOME=false
    page.init()
end
event_handler = function(obj, event)
    if event == lvgl.EVENT_CLICKED then
        if lvgl.obj_get_id(obj)==lvgl.obj_get_id(airimg) then
            gotoPage(activity_airCtr)
            print("Clicked airimg")
        elseif lvgl.obj_get_id(obj)==lvgl.obj_get_id(mediaimg) then
            gotoPage(activity_media)
            print("Clicked mediaimg")
        elseif lvgl.obj_get_id(obj)==lvgl.obj_get_id(updataimg) then
            gotoPage(activity_updata)
            print("Clicked upimg")
        elseif lvgl.obj_get_id(obj)==lvgl.obj_get_id(testimg) then
            gotoPage(activity_test)
            print("test")
        end

    end
end


function makeButton(cont)
    local body = lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(body,false)
    lvgl.obj_set_size(body, 1024, 180)
    lvgl.cont_set_layout(body, lvgl.LAYOUT_PRETTY_MID)
    lvgl.obj_align(body, cont, lvgl.ALIGN_IN_TOP_MID, 0, 200)
    lvgl.obj_add_style(body, lvgl.CONT_PART_MAIN, stytle.stytle_divBox)



    airimg = lvgl.img_create(body, nil)
    lvgl.obj_set_click(airimg,true)
    lvgl.obj_set_event_cb(airimg, event_handler)
    lvgl.img_set_src(airimg, "/lua/air.png")
    lvgl.obj_add_style(airimg, lvgl.CONT_PART_MAIN, stytle.stytle_icon)

    mediaimg = lvgl.img_create(body, nil)
    lvgl.obj_set_click(mediaimg,true)
    lvgl.obj_set_event_cb(mediaimg, event_handler)
    lvgl.img_set_src(mediaimg, "/lua/media.png")
    lvgl.obj_add_style(mediaimg, lvgl.CONT_PART_MAIN, stytle.stytle_icon)

    updataimg = lvgl.img_create(body, nil)
    lvgl.obj_set_click(updataimg,true)
    lvgl.obj_set_event_cb(updataimg, event_handler)
    lvgl.img_set_src(updataimg, "/lua/updata.png")
    lvgl.obj_add_style(updataimg, lvgl.CONT_PART_MAIN, stytle.stytle_icon)

    testimg = lvgl.img_create(body, nil)
    lvgl.obj_set_click(testimg,true)
    lvgl.obj_set_event_cb(testimg, event_handler)
    lvgl.img_set_src(testimg, "/lua/test.png")
    lvgl.obj_add_style(testimg, lvgl.CONT_PART_MAIN, stytle.stytle_icon)


------------------------------------------------------------------     imageBtn
    -- airimg = lvgl.imgbtn_create(body, nil)
    -- lvgl.imgbtn_set_src(airimg, lvgl.BTN_STATE_RELEASED, "/lua/air.png")
    -- lvgl.imgbtn_set_src(airimg, lvgl.BTN_STATE_PRESSED, "/lua/air_click.png")
    -- lvgl.obj_set_event_cb(airimg, event_handler)
    -- lvgl.obj_add_style(airimg, lvgl.CONT_PART_MAIN, stytle.stytle_icon)


    -- mediaimg = lvgl.imgbtn_create(body, nil)
    -- lvgl.imgbtn_set_src(mediaimg, lvgl.BTN_STATE_RELEASED, "/lua/media.png")
    -- lvgl.imgbtn_set_src(mediaimg, lvgl.BTN_STATE_PRESSED, "/lua/media_click.png")
    -- lvgl.obj_set_event_cb(mediaimg, event_handler)
    -- lvgl.obj_add_style(mediaimg, lvgl.CONT_PART_MAIN, stytle.stytle_icon)


    -- updataimg = lvgl.imgbtn_create(body, nil)
    -- lvgl.imgbtn_set_src(updataimg, lvgl.BTN_STATE_RELEASED, "/lua/updata.png")
    -- lvgl.imgbtn_set_src(updataimg, lvgl.BTN_STATE_PRESSED, "/lua/updata_click.png")
    -- lvgl.obj_set_event_cb(updataimg, event_handler)
    -- lvgl.obj_add_style(updataimg, lvgl.CONT_PART_MAIN, stytle.stytle_icon)


    -- testimg = lvgl.imgbtn_create(body, nil)
    -- lvgl.imgbtn_set_src(testimg, lvgl.BTN_STATE_RELEASED, "/lua/test.png")
    -- lvgl.imgbtn_set_src(testimg, lvgl.BTN_STATE_PRESSED, "/lua/test_click.png")
    -- lvgl.obj_set_event_cb(testimg, event_handler)
    -- lvgl.obj_add_style(testimg, lvgl.CONT_PART_MAIN, stytle.stytle_icon)





    local function makeLable(obj,text,al)
        local label = lvgl.label_create(cont,nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label, "#FFFFFF "..text)
        lvgl.obj_align(label,obj, al, 0, 10)
    end
    makeLable(airimg,"空调控制器",lvgl.ALIGN_OUT_BOTTOM_MID)
    makeLable(mediaimg,"多媒体下载",lvgl.ALIGN_OUT_BOTTOM_MID)
    makeLable(updataimg,"远程升级",lvgl.ALIGN_OUT_BOTTOM_MID)
    makeLable(testimg,"测试",lvgl.ALIGN_OUT_BOTTOM_MID)
end

function makeTopArea(cont)

    local font = lvgl.font_load("/lua/timefont.bin")
    local dateFont = lvgl.font_load("/lua/datefont.bin")

    skyCont = lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(skyCont,false)
    lvgl.obj_set_size(skyCont, 1024, 200)
    -- lvgl.obj_set_auto_realign(skyCont, true)
    lvgl.obj_align(skyCont, cont, lvgl.ALIGN_IN_TOP_MID, 0, 0)
    lvgl.obj_add_style(skyCont, lvgl.CONT_PART_MAIN, stytle.stytle_divBox)

    _G.skyimg = lvgl.img_create(skyCont, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(skyimg, "/lua/icon_empty.png")
    -- 图片居中
    lvgl.obj_align(skyimg, skyCont, lvgl.ALIGN_CENTER, 120, 10)

    _G.TEMPLabel=lvgl.label_create(skyCont, nil)
    lvgl.label_set_recolor(TEMPLabel, true)
    lvgl.obj_set_style_local_text_font(TEMPLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font)
    lvgl.label_set_text(TEMPLabel, "#FFFFFF ".."--")
    lvgl.obj_align(TEMPLabel, skyimg, lvgl.ALIGN_OUT_RIGHT_MID,25,-15)

    _G.weatherTextLabel=lvgl.label_create(skyCont, nil)
    lvgl.label_set_recolor(weatherTextLabel, true)
    -- lvgl.obj_set_style_local_text_font(TEMPLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font)
    lvgl.label_set_text(weatherTextLabel, "#FFFFFF ".."")
    lvgl.obj_align(weatherTextLabel, TEMPLabel, lvgl.ALIGN_OUT_BOTTOM_LEFT,5,15)

    local tempIcon = lvgl.img_create(skyCont, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(tempIcon, "/lua/tempIcon.png")
    -- 图片居中
    lvgl.obj_align(tempIcon, TEMPLabel, lvgl.ALIGN_OUT_RIGHT_MID, 3,-10)

    service_weather.weatherService(skyimg,TEMPLabel,weatherTextLabel)

    ----------------------
    _G.TIMELabel = lvgl.label_create(skyCont, nil)
    lvgl.label_set_recolor(TIMELabel, true)
    lvgl.obj_set_style_local_text_font(TIMELabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font)
    lvgl.label_set_text(TIMELabel, "#FFFFFF ".."正在获取时间...")
    lvgl.obj_align(TIMELabel, skyCont, lvgl.ALIGN_IN_TOP_MID, -330,50)

    _G.DATELabel = lvgl.label_create(skyCont, nil)
    lvgl.label_set_recolor(DATELabel, true)
    lvgl.obj_set_style_local_text_font(DATELabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, dateFont)
    lvgl.label_set_text(DATELabel, "#FFFFFF ".."- -")
    lvgl.obj_align(DATELabel, TIMELabel, lvgl.ALIGN_OUT_BOTTOM_LEFT, 5,20)

    service_time.timeService(TIMELabel,DATELabel)

    local localtionImg = lvgl.img_create(skyCont, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(localtionImg, "/lua/locationIcon.png")
    -- 图片居中
    lvgl.obj_align(localtionImg,TIMELabel, lvgl.ALIGN_OUT_BOTTOM_LEFT, 0, 50)

    _G.locationLabel=lvgl.label_create(skyCont, nil)
    lvgl.label_set_recolor(locationLabel, true)
    lvgl.label_set_text(locationLabel, "#FFFFFF ".."--")
    lvgl.obj_align(locationLabel, localtionImg, lvgl.ALIGN_OUT_RIGHT_BOTTOM, 5,-5)
    service_location.addrService(_G.locationLabel)

end

function makeRoot(cont)
    lvgl.obj_clean(contentBox)
    -- statusLabel.setBackImg("/lua/backIcon_ept.bin")
    -- lvgl.btn_set_state(backimg, lvgl.BTN_STATE_DISABLED)
    lvgl.obj_set_hidden(backimg,true)
    statusLabel.setWinTitle("")
end


function makeFoot(cont)
    -----------------
    local function makeLable(text,al,x,y)
        local label = lvgl.label_create(cont, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label, "#FFFFFF "..text)
        lvgl.obj_align(label,cont, al, x,y)
    end

    makeLable("工程版本：V"..VERSION,lvgl.ALIGN_IN_BOTTOM_LEFT,30,-10)
    makeLable(VERSION_PRI,lvgl.ALIGN_IN_BOTTOM_RIGHT,-30,-10)
    _G.iccidLabel = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(iccidLabel, true)
    lvgl.label_set_text(iccidLabel, "#FFFFFF ".."正在获取ICCID")
    lvgl.obj_align(iccidLabel,cont, lvgl.ALIGN_IN_BOTTOM_MID, 0,-10)
    service_sim.ICCIDservice(iccidLabel)
end



function init()
    _G.HOME=true

    makeRoot(contentBox)
    makeTopArea(contentBox)
    makeButton(contentBox)
    makeFoot(contentBox)
    -- _G.HOME=true
    -- sys.taskInit(iccidTest,contentBox)
    -- sys.taskInit(makeRoot,contentBox)
    -- lvgl.obj_clean(backIcon)
end

-- sys.taskInit(function()
--     init()
-- end)

module(..., package.seeall)

_G.connLabel=nil
_G.fragmentConnect=true
_G.ipProtocol="tcp"

local setCont

event_handler_btn = function(obj, event)
    if event == lvgl.EVENT_CLICKED then
        _G.BEEP()
        log.info("UI makeQRbtn",_G.fragmentConnect)
        if _G.fragmentConnect then
            lvgl.label_set_text(_G.connLabel, "#FFFFFF 断开连接")
            _G.fragmentConnect = false
            sys.publish("CONNECT_IND")
        else
            lvgl.label_set_text(_G.connLabel, "#FFFFFF 连接")
            _G.fragmentConnect=true
            sys.publish("SOCKET_DISCONNECT_IND")
            socketOutMsg.sndTest("disconnect")
        end
    end
end

function init(sysCont)

    function makeConnBtn()
        local btn = lvgl.cont_create(sysCont, nil)
        lvgl.obj_set_size(btn, 200, 50)
        lvgl.obj_add_style(btn, lvgl.BTN_PART_MAIN, style.style_QRbtnBg)
        lvgl.obj_set_event_cb(btn, event_handler_btn)
        _G.connLabel=lvgl.label_create(btn, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(_G.connLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font28)
        end
        lvgl.label_set_recolor(_G.connLabel, true)
        lvgl.label_set_text(_G.connLabel, "#FFFFFE 连接")
        lvgl.obj_align(_G.connLabel, btn, lvgl.ALIGN_CENTER, 0, 0)

    end

    function makeLable(text,x,y)
        local label=lvgl.label_create(sysCont, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font28)
        end
        lvgl.label_set_text(label, text)
        lvgl.obj_align(label, sysCont, lvgl.ALIGN_IN_LEFT_MID, x, y)
    end

    event_handler = function(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            local opt = lvgl.dropdown_get_selected(obj)
            log.info("Option:", lvgl.dropdown_get_selected(obj))
            _G.BEEP()
            if opt == 1 then
                _G.ipProtocol = "udp"
            else
                _G.ipProtocol = "tcp"
            end
        end
    end

    function makeDropdown()
        local itemCont=lvgl.cont_create(setCont, nil)
        lvgl.obj_set_click(itemCont,false)
        lvgl.obj_set_size(itemCont, 210,40)
        lvgl.obj_add_style(itemCont, lvgl.CONT_PART_MAIN, style.style_divBox)

        local label=lvgl.label_create(itemCont, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font28)
        end
        lvgl.label_set_text(label, "协议:")
        lvgl.obj_align(label, itemCont, lvgl.ALIGN_IN_LEFT_MID, 0, 0)

        local dd = lvgl.dropdown_create(itemCont, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_SELECTED,lvgl.STATE_DEFAULT, font28)
            lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_LIST,lvgl.STATE_DEFAULT, font28)
            lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_MAIN,lvgl.STATE_DEFAULT, font28)
        end
        lvgl.dropdown_set_options(dd, [[TCP
UDP]])
        lvgl.obj_set_event_cb(dd, event_handler)
        lvgl.obj_set_size(dd, 150,40)
        lvgl.obj_align(dd, itemCont, lvgl.ALIGN_IN_LEFT_MID, 60, 0)
    end

    --创建limit输入框
    local function makeLimitBox(cont,mode,p1)
        local limitCont=lvgl.cont_create(cont, nil)
        lvgl.obj_set_click(limitCont,false)
        lvgl.obj_set_size(limitCont, 155,50)
        lvgl.obj_add_style(limitCont, lvgl.CONT_PART_MAIN, style.style_divBox)
        lvgl.obj_align(limitCont,cont, lvgl.ALIGN_IN_RIGHT_MID, 5, 2)

        --输入框回调
        local function paraChange(obj)
            local data = lvgl.textarea_get_text(obj)
            log.info("UI input data",data)
            if mode == "addr" then
                _G.ipAddress =  data
            elseif mode == "prot" then
                _G.ipPort = data
            end
        end

        local inputLeft=inputBox.init(limitCont,150,40,lvgl.ALIGN_CENTER,5, 0,p1,paraChange)
    end

    makeLable("连接控制",0,0)

    --设置
    setCont=lvgl.cont_create(sysCont, nil)
    lvgl.obj_set_click(setCont,false)
    lvgl.obj_set_size(setCont, 210,260)
    lvgl.obj_add_style(setCont, lvgl.CONT_PART_MAIN, style.style_divBox)

    --协议下拉框
    makeDropdown()

    --地址输入框
    local itemCont=lvgl.cont_create(setCont, nil)
    lvgl.obj_set_click(itemCont,false)
    lvgl.obj_set_size(itemCont, 210,80)
    lvgl.obj_add_style(itemCont, lvgl.CONT_PART_MAIN, style.style_divBox)
    lvgl.obj_align(itemCont, setCont, lvgl.ALIGN_CENTER, 0, 0)

    local label=lvgl.label_create(itemCont, nil)
    if _G.Font=="vector"then
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font28)
    end
    lvgl.label_set_text(label, "地址:")
    lvgl.obj_align(label, itemCont, lvgl.ALIGN_IN_LEFT_MID, 0, 0)

    makeLimitBox(itemCont,"addr",_G.ipAddress)

    --端口输入框
    local itemCont=lvgl.cont_create(setCont, nil)
    lvgl.obj_set_click(itemCont,false)
    lvgl.obj_set_size(itemCont, 210,80)
    lvgl.obj_add_style(itemCont, lvgl.CONT_PART_MAIN, style.style_divBox)
    lvgl.obj_align(itemCont, setCont, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)

    local label=lvgl.label_create(itemCont, nil)
    if _G.Font=="vector"then
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font28)
    end
    lvgl.label_set_text(label, "端口:")
    lvgl.obj_align(label, itemCont, lvgl.ALIGN_IN_LEFT_MID, 0, 0)

    makeLimitBox(itemCont,"prot",_G.ipPort)

    makeConnBtn()

end

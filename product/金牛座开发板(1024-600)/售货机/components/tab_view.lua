module(..., package.seeall)

function create(p, v)
    -- 创建一个 TabView
    demo_TabView = lvgl.tabview_create(p, nil)
    -- 设置 TabView 的显示大小
    lvgl.obj_set_size(demo_TabView, v.W or 440, v.H or 600)
    -- 设置 TabView 的位置
    lvgl.obj_align(demo_TabView, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    -- 添加样式
    -- lvgl.obj_add_style(demo_TabView, lvgl.TABVIEW_PART_BG, demo_ThemeStyle_Bg)
    -- lvgl.obj_add_style(demo_TabView, lvgl.TABVIEW_PART_TAB_BG,
    --                    demo_ThemeStyle_Bg)

    -- 添加页面
    demo_Tab1 = lvgl.tabview_add_tab(demo_TabView, "电影")
    demo_Tab2 = lvgl.tabview_add_tab(demo_TabView, "电视剧")
    return demo_TabView
end

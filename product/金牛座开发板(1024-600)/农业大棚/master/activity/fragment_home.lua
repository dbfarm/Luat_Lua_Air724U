module(..., package.seeall)

_G.fragmentHome=false

function makeRunningBox(cont)

    local runningCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(runningCont,false)
    lvgl.obj_set_size(runningCont, 645, 500)
    lvgl.obj_add_style(runningCont, lvgl.CONT_PART_MAIN, style.style_contentBox)
    lvgl.obj_align(runningCont, cont, lvgl.ALIGN_IN_TOP_RIGHT, -10, 0)

    runningBox=lvgl.cont_create(runningCont, nil)
    lvgl.obj_set_size(runningBox, 600, 460)
    lvgl.obj_add_style(runningBox, lvgl.CONT_PART_MAIN, style.style_msgBg)
    lvgl.obj_align(runningBox, runningCont, lvgl.ALIGN_CENTER, 0, 0)
    -- lvgl.cont_set_layout(runningBox,lvgl.LAYOUT_COLUMN_LEFT)

    return runningBox
end

function makeSystemStatusBox(cont)
    local sysCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(sysCont,false)
    lvgl.obj_set_size(sysCont, 330, 500)
    lvgl.obj_add_style(sysCont, lvgl.CONT_PART_MAIN, style.style_contentBox)
    lvgl.obj_align(sysCont, cont, lvgl.ALIGN_IN_TOP_LEFT,10, 0)
    lvgl.cont_set_layout(sysCont,lvgl.LAYOUT_CENTER)
    return sysCont
end

function init(cont)
    _G.fragmentHome=true
    lvgl.obj_clean(cont)
    local runningBox=makeRunningBox(cont)
    local systemStatusBox=makeSystemStatusBox(cont)

    fragment_RunStatus.init(runningBox)
    fragment_systemStatus.init(systemStatusBox,runningBox)

end

function UnInit()
    fragment_RunStatus.unInit()
    fragment_systemStatus.unInit()
    fragment_qrcode.uninit()
    fragment_paraSetting.uninit()
    _G.fragmentHome=false
end

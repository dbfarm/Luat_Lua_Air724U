module(..., package.seeall)

local function cloneTab(tb)
    local ret = {}
    for k,v in pairs(tb) do
        ret[k] = type(v) == "table" and cloneTab(v) or v
    end
    return ret
end

local img=nil
local imgtab=cloneTab(UIConfig.UIParams.screensaver)
local timer=nil
local index=1

local function init(time)
    log.info("屏保","开始屏保")
    if time==nil then time=1000 end

    if img then return end
    local function click(obj,event)
        if event == lvgl.EVENT_CLICKED then
            _G.BEEP()
            log.info("屏保","退出屏保")
            sys.publish("SCREENSAVER_UNINIT")--退出屏保
        end
    end

    local function changeIMG()
        lvgl.img_set_src(img,string.format("/lua/%s",imgtab[index+1]))
        index=index+1
        index=index%#imgtab
    end

        img=lvgl.img_create(_G.body, nil)
        lvgl.obj_set_click(img,true)
        lvgl.img_set_src(img,string.format("/lua/%s",imgtab[1]))
        lvgl.obj_align(img, nil, lvgl.ALIGN_CENTER, 0, 0)
        lvgl.obj_set_event_cb(img,click)

        timer=sys.timerLoopStart(changeIMG,time)
end

local function uninit()
    if not img then return end
    sys.timerStop(timer)
    lvgl.obj_del(img)
    img=nil
end

sys.subscribe("SCREENSAVER_INIT",init)
sys.subscribe("SCREENSAVER_UNINIT",uninit)
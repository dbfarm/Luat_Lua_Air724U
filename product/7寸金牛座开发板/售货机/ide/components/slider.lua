module(..., package.seeall)
local event_handler = function(obj, event)
    if event == lvgl.EVENT_VALUE_CHANGED then end
end

local function create_knob_style(v)
    -- SLIDER_PART_KNOB
    local s = set_common_style(v)

    return s
end

local function create_indic_style(v)
    -- SLIDER_PART_INDIC
    local s = set_common_style(v)

    return s
end

function create_main_style(v)
    local s = set_common_style(v)

    return s
end

function create(p, v)

    obj = lvgl.slider_create(p, nil)
    lvgl.obj_set_click(obj, v.click or true)
    lvgl.obj_set_size(obj, v.W or 100, v.H or 100)
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    lvgl.slider_set_range(obj, v.range_s or 0, v.range_e or 100)

    if v.event then lvgl.obj_set_event_cb(obj, v.event) end

    -- lvgl.obj_set_event_cb(obj, v.cb or event_handler)
    -- lvgl.obj_add_style(obj, lvgl.SLIDER_PART_BG, set_common_style(v.bg))

    -- lvgl.obj_add_style(obj, lvgl.SLIDER_PART_INDIC,
    --                    create_indic_style(v.indic))
    -- lvgl.obj_add_style(obj, lvgl.SLIDER_PART_KNOB, create_knob_style(v.knob))

    return obj
end

-- create(lvgl.scr_act(), {W = 400, H = 100})

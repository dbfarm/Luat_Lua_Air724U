module(..., package.seeall)

function create_bg_style(v)
    if not v then v = {} end
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 边框
    if v.border then s = set_border(s, v.border) end
    -- 阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 外框线
    if v.outline then s = set_outline(s, v.outline) end
    -- 内边距
    if v.pad then s = set_pad(s, v.pad) end
    return s
end
function create_major_style(v)
    if not v then v = {} end
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 边框
    if v.border then s = set_border(s, v.border) end
    -- 阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 外框线
    if v.outline then s = set_outline(s, v.outline) end
    -- 文字样式
    if v.text then s = set_text(s, v.text) end
    -- 背景线样式
    if v.line then s = set_line(s, v.line) end
    -- 内边距
    if v.pad then s = set_pad(s, v.pad) end

    return s
end
function create_needle_style(v)
    if not v then v = {} end
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 边框
    if v.border then s = set_border(s, v.border) end
    -- 阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 外框线
    if v.outline then s = set_outline(s, v.outline) end
    -- 背景线样式
    if v.line then s = set_line(s, v.line) end
    -- 内边距
    if v.pad then s = set_pad(s, v.pad) end
    return s
end
function create(p, v)
    local obj = lvgl.gauge_create(p, nil)
    lvgl.obj_set_size(obj, v.R or 400, v.R or 400)

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_IN_TOP_MID,
                   v.align_x or 0, v.align_y or 0)
    -- 量规可以显示多于一根针。使用 lvgl.gauge_set_needle_count(gauge, needle_num, color_array) 
    -- 函数针数和每根针具有颜色的数组。数组必须是静态或全局变量，因为仅存储其指针。
    -- lvgl.gauge_set_needle_count(obj, 1, lvgl.color_hex(v.needle_color or 0x000000))
    lvgl.gauge_set_needle_count(obj, 1,
                                {lvgl.color_hex(v.needle_color or 0xFF0000)})

    -- 量规的范围可以通过 lvgl.gauge_set_range(gauge, min, max) 指定。默认范围是0..100。
    lvgl.gauge_set_range(obj, v.range_start or 0, v.range_end or 100)

    -- 要临界值，请使用 lvgl.gauge_set_critical_value(gauge, value)。
    -- 此值之后，比例尺颜色将更改为scale_end_color。默认临界值为80。
    lvgl.gauge_set_critical_value(obj, v.critical_value or 90)

    -- 可以使用 lvgl.gauge_set_value(gauge, needle_id, value) 来针的值。
    lvgl.gauge_set_value(obj, 0, 30)

    -- 可以使用 lvgl.gauge_set_scale(gauge, angle, line_num, label_cnt) 
    -- 函数来调整刻度角度以及刻度线和标签的数量。默认为220度，6个比例标签和21条线。
    lvgl.gauge_set_scale(obj, v.scale or 220, 51, 11)

    -- 量表的刻度可以偏移。可以通过 lvgl.gauge_set_angle_offset(gauge, angle) 进行调整。
    if v.angle_offset then lvgl.gauge_set_angle_offset(obj, v.angle_offset) end

    -- lvgl.gauge_set_needle_img(obj, "/lua/bg_5.jpg", 0, 0)
    -- lvgl.obj_set_style_local_image_recolor_opa(obj, lvgl.GAUGE_PART_NEEDLE,
    --                                            lvgl.STATE_DEFAULT, 0);

    -- 量规的主要部分称为 lvgl.GAUGE_PART_MAIN 。它使用典型的背景样式属性绘制背景，
    -- 并使用线和比例样式属性绘制“较小”比例线。它还使用text属性比例标签的样式。
    -- pad_inner用于刻度线和刻度标签之间的空间。
    lvgl.obj_add_style(obj, lvgl.GAUGE_PART_MAIN, create_bg_style(v.style.main))

    -- lvgl.GAUGE_PART_MAJOR 是一个虚拟小部件，它使用line和scale样式属性描述了主
    -- 要的比例尺线（添加了标签）。
    lvgl.obj_add_style(obj, lvgl.GAUGE_PART_MAJOR,
                       create_major_style(v.style.major))

    -- lvgl.GAUGE_PART_NEEDLE 也是虚拟小部件，它通过线型属性来描述针。
    -- 的大小和典型的背景属性用于描述在所述针（多个）的枢转点的矩形（或圆形）。
    -- pad_inner用于使针比刻度线的外半径小。
    lvgl.obj_add_style(obj, lvgl.GAUGE_PART_NEEDLE,
                       create_needle_style(v.style.needle))

    return obj
end

-- create(lvgl.scr_act(), {
--     R = 450,
--     align = lvgl.ALIGN_CENTER,
--     align_x = 0,
--     align_y = 0,
--     critical_value = 100,
--     style = {
--         main = {
--             pad = {all = 50},
--             border = {color = 0x00ff0f, width = 2, opa = 200},
--             shadow = {spread = 30, color = 0xff00f0},
--             bg = {
--                 radius = 10,
--                 color = 0x0f0ff0,
--                 opa = 150,
--                 grad = {color = 0x0f0f0f}
--             }
--         },
--         major = {
--             pad = {all = 200},
--             border = {color = 0x00ff0f, width = 2, opa = 200},
--             shadow = {spread = 30, color = 0xff00f0},
--             bg = {
--                 radius = 10,
--                 color = 0x0f0ff0,
--                 opa = 150,
--                 grad = {color = 0x0f0f0f}
--             },
--             line = {color = 0xafccfc, width = 3, opa = 100, rounded = false},
--             text = {
--                 font = style.font24,
--                 color = 0xff0000,
--                 opa = 100,
--                 letter_space = 2,
--                 line_space = 0
--             }
--         },
--         needle = {
--             pad = {inner = 100},
--             line = {color = 0xafccfc, width = 3, opa = 100, rounded = false},
--             border = {color = 0x00ff0f, width = 2, opa = 200},
--             shadow = {spread = 30, color = 0xff00f0},
--             bg = {
--                 radius = 10,
--                 color = 0x0f0ff0,
--                 opa = 150,
--                 grad = {color = 0x0f0f0f}
--             }
--         }
--     }
-- })

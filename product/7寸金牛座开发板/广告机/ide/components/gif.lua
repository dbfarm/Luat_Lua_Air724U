module(..., package.seeall)

function create(p, v)

    -- local obj = lvgl.gif_create_from_file(p, v.src)

    local obj = lvgl.gif_create(p, nil)

    if v.H and v.W then lvgl.obj_set_size(obj, v.W, v.H) end
    if not v.click then lvgl.obj_set_click(obj, false) end

    if v.src then lvgl.gif_set_src(obj, v.src) end

    -- 播放速度：延时越大，播放速度越慢，模拟器暂不支持
    -- lvgl.gif_set_delay(obj, v.delay)
    lvgl.obj_align(obj, v.align_to or p, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    function after(n)
        return function(obj, event)
            if event == lvgl.EVENT_LEAVE then
                -- gif文件内设定的循环次数播放结束，会触发EVENT_LEAVE
                print("play end")
            end
            if event == lvgl.EVENT_REFRESH then
                -- 每次播放结束都会触发EVENT_REFRESH，可在这里编写播放次数等逻辑
                n = n - 1
                if n == 0 then
                    lvgl.obj_del(obj)
                    print("over")
                    return
                end
                print("next play")
            end
        end
    end

    if v.loop then lvgl.obj_set_event_cb(obj, after(v.loop)) end
end

-- create(lvgl.scr_act(),
--        {W = 200, H = 100, loop = 3, delay = 300, src = "/lua/test_1.gif"})

module(..., package.seeall)
--------------
require "sim"

require "net"

local OperatorsTable={
	CMCC={"00","02","04","07"},
	CUCC={"01"},
	CTCC={"03","11"}
}

local signalIconTable={"sin1.bin","sin2.bin","sin3.bin","sin4.bin"}

function ICCIDservice(ic)
    sys.taskInit(function()
        while sim.getIccid()==nil do sys.wait(500) end
        if _G.HOME and ic==_G.iccidLabel then 
            lvgl.label_set_text(ic, "#FFFFFF SIM卡号ICCID："..sim.getIccid())
            lvgl.obj_align(ic,nil, lvgl.ALIGN_IN_BOTTOM_MID, 0,-10)
        end
    end)
end

local function getOperators(s)
    for o,m in pairs(OperatorsTable) do
		for _,v in ipairs(m) do
			if v==s then 
                return  o=="CMCC" and "中国移动" or (o=="CUCC" and "中国联通" or "中国电信")
            end
		end
	end
	return "UNKNOWN"
end

function signalService(OperatorsLabel,icon)
    sys.taskInit(function()
        sys.timerLoopStart(function() lvgl.img_set_src(icon, "/lua/"..(signalIconTable[math.floor(net.getRssi()/8)+1] or signalIconTable[1])) end,1000)
            while #sim.getMnc()==0 do 
                sys.wait(500) 
            end
            local MNC=sim.getMnc()
            lvgl.label_set_text(OperatorsLabel, "#FFFFFF "..getOperators(MNC))
            -- lvgl.obj_align(OperatorsLabel,statusBox, lvgl.ALIGN_IN_RIGHT_MID, -20, 0) 
            lvgl.obj_align(OperatorsLabel,icon, lvgl.ALIGN_OUT_LEFT_MID, -8, 3)
    end)

end

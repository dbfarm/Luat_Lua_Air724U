PROJECT = "7inch_mediaAirController"
VERSION = "2.0.0"
VERSION_PRI="V6.3.324.0"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"


require "log"
require "sys"
require "http"
require "misc"
require "pins"
-- require "mipi_lcd_GC9503"
require "mipi_rgb"
tp = require "tp"
-- tp = require "TP_NS2009"
require "service_sim"
require "service_time"
require "service_location"
require "service_weather"
require "service_download"


lvgl.init(function()
end, tp.input)
-- lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)


require "stytle"
require "statusLabel"
_G.contentBox=lvgl.cont_create(lvgl.scr_act(), nil)
lvgl.obj_set_click(contentBox,false)
lvgl.obj_set_size(contentBox, 854, 435)
-- lvgl.obj_set_auto_realign(Titlecont, true)
lvgl.obj_align(contentBox, nil, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
lvgl.obj_add_style(contentBox, lvgl.CONT_PART_MAIN, stytle.stytle_contentBox)



require "activity_HOME"
require "activity_airCtr"
require "activity_media"
require "activity_updata"
require "activity_test"


activity_HOME.init()
-- activity_airCtr.init()
-- activity_media.init()
-- activity_updata.init()
-- activity_test.init()


require "ProjectSwitch"

sys.init(0, 0)
sys.run()

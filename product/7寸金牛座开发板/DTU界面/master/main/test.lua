module(..., package.seeall)

local UI=UIConfig.UIParams
local moto=UI.moto
local wind=UI.wind
local energy=UI.energy
local message=UIConfig.Message
local area=UI.area
----------------------------------------------------------------------
sys.taskInit(function()
    local i=0
    local j=0
    while true do
        i=i+1
        i=i%20
        j=j+1
        j=j%20
        wind.send_wind=i+5
        wind.return_wind=j+10
        sys.publish("UI_WIND")
        sys.wait(200)
    end
end)


sys.taskInit(function()
    local i=0
    while true do
        i=i+1
        i=i%30
        moto.now=i+80
        sys.publish("UI_MOTO")
        sys.wait(200)
    end
end)

sys.taskInit(function()
    local i=8
    local j=6
    math.randomseed(os.time())
    while true do
        i=i+1
        i=i%20
        j=j+1
        j=j%20
        energy.kv=30+i+math.random()
        energy.kvar=50+j+math.random()
        sys.publish("UI_ENERGY")
        sys.wait(200)
    end
end)

sys.taskInit(function ()
    local function ss(t)
        if math.random() > 0.5 then 
            if #t.data<3 then
                table.insert(t.data,1)
            end
        else
            if #t.data>1 then
                table.remove (t.data)
            end
        end
    end
    while true do
        for k, v in pairs(message) do
            if type(v)=="table" then ss(v) end
        end
        sys.publish("UI_MESSAGE")
        sys.wait(800)
    end
end)

sys.taskInit(function()

    while true do
        -- print("-------------------------------------------------")
        for k, v in pairs(area) do
            if math.random() > 0.8 then
                area[k] = not v
            end
        end
        sys.publish("UI_AREA")
        sys.wait(700)
    end
end)


----------------

sys.taskInit(function()
    local cc={"urgent","error","warn","nomal","info"}
    while true do
        
        local i=math.random(1,5)
        local m={}
        m.level=cc[i]
        m.text="实验室爆炸"
        -- addMessage(m)
        -- -- print("-********************",#massageTable)
        -- -- for key, value in pairs(massageTable) do
        -- --     print(key,value)
        -- -- end
        -- -- print("----------------------",#massageTable)
        sys.publish("UI_ERROR",m)
        sys.wait(1000)
    end
end)
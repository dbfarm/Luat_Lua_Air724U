PROJECT = "7inch_DTU_UI"
VERSION = "9.9.9"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"

require "log"
require "sys"
require "http"
require "misc"
require "pins"
require "mipi_rgb"
-- require "mipi_lcd_GC9503"
require "tp"
require "service_sim"
require "service_time"
require "style"
require "UIConfig"

-- 测试需要打开 "test"
require "test"


-- lvgl.obj_set_style_local_text_font=function (...)
--     return
-- end

_G.BEEP=function() pio.pin.plus(13,250,500,20) end

spi.setup(spi.SPI_1,1,1,8,45000000,1)
font16=lvgl.font_load(spi.SPI_1,16,4,88)
font18=lvgl.font_load(spi.SPI_1,18,4,110)
font22=lvgl.font_load(spi.SPI_1,22,4,114)
font24=lvgl.font_load(spi.SPI_1,24,4,110)
font32=lvgl.font_load(spi.SPI_1,32,4,150)
font36=lvgl.font_load(spi.SPI_1,36,4,156)
-- font24=lvgl.font_load(spi.SPI_1,16,2,40)
-- font32=lvgl.font_load(spi.SPI_1,16,2,40)
-- font36=lvgl.font_load(spi.SPI_1,16,2,40)

lvgl.init(function()
end, tp.input)
-- lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)



require "activity_status"

--正式调试打开activity_status.activityInit()
activity_status.activityInit()





require "ProjectSwitch"

sys.init(0, 0)
sys.run()

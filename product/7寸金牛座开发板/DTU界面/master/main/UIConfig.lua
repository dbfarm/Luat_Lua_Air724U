module(..., package.seeall)
--参与UI交互的数据定义
--控制模式按照枚举定义，点击轮换
--系统状态的报警可通过点击5下清除，比如点击5下--滤芯：更换--将状态切换为正常

--UI的大棚数与时控的段数可根据这个表来动态配置，比如这个表配置了10大棚，UI就是10大棚。。。


--Ui数据总表
UIParams = {
    moto={
        P=120,
        now=87
    },
    wind={
        send_wind=15,
        return_wind=18
    },
    energy={
        kv=18,
        kvar=19
    },
    area={
        A=false,
        B=true,
        C=true,
        D=true,
        E=true,
        F=true,
        G=true
    }
}

Message={
    urgent={
        style=style.style_arc_urgent,
        data={
            1,
        }
    },
    error={
        style=style.style_arc_error,
        data={
            1,
        }
    },
    warn={
        style=style.style_arc_warn,
        data={
            1,
        }
    },
    nomal={
        style=style.style_arc_nomal,
        data={
            1,
        }
    },
    info={
        style=style.style_arc_info,
        data={
            1,
        }
    }
}

function getMessage()
    local t=Message
    t.cnt=0
    for k,v in pairs(t) do
        if type(v)=="table" then
            t.cnt=t.cnt+#v.data 
        end
    end

    local temp={}
    for k,v in pairs(t) do
        if type(v)=="table" and #v.data>0 then 
            table.insert(temp,v)
        end
    end

    for i=1,#temp do
        local agle=math.floor((#temp[i].data/t.cnt)*360)
        temp[i].start=temp[i-1]~=nil and temp[i-1].ending or 0
        temp[i].ending=temp[i].start+agle
        if i==#temp then temp[i].ending=360 end
    end

    return Message
end

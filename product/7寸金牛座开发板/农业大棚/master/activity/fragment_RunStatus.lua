module(..., package.seeall)

local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end

function paddingRunningBox(runningBox,t)

    local labelBox=lvgl.cont_create(runningBox, nil)
    lvgl.obj_set_click(labelBox,false)
    lvgl.obj_set_size(labelBox, 560,40) 
    lvgl.obj_add_style(labelBox, lvgl.CONT_PART_MAIN, style.style_divBox)

    function makeLable(text,x,y)
        local label=lvgl.label_create(labelBox, nil)
        -- lvgl.label_set_recolor(label, true) 
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        lvgl.label_set_text(label, text)
        lvgl.obj_align(label, labelBox, lvgl.ALIGN_IN_LEFT_MID, x, y)
    end

    makeLable("模式",115,0)
    makeLable("光强",190,0)
    makeLable("状态",310,0)
    makeLable("开关",450,0)
    makeLable("设置",508,0)

    local line=lvgl.cont_create(labelBox, nil)
    lvgl.obj_set_click(line,false)
    lvgl.obj_set_size(line, 560, 1) 
    lvgl.obj_add_style(line, lvgl.CONT_PART_MAIN, style.style_line)
    lvgl.obj_align(line, labelBox, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)

    local itemList = lvgl.page_create(runningBox, nil)
    lvgl.obj_set_size(itemList, 560, 335)
    lvgl.obj_set_click(itemList,false)
    lvgl.obj_add_style(itemList, lvgl.CONT_PART_MAIN, style.style_list)
    lvgl.page_set_scrl_layout(itemList,lvgl.LAYOUT_COLUMN_MID)
    lvgl.obj_align(itemList,labelBox, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
    lvgl.page_set_scrollbar_mode(itemList, lvgl.SCRLBAR_MODE_DRAG)

    function makeItem(cont,t)
        local itemCont=lvgl.cont_create(cont, nil)
        lvgl.obj_set_click(itemCont,false)
        lvgl.obj_set_size(itemCont, 560,42)
        -- lvgl.cont_set_layout(itemCont,lvgl.LAYOUT_ROW_MID)
        -- lvgl.obj_align(itemCont, cont, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
        lvgl.obj_add_style(itemCont, lvgl.CONT_PART_MAIN, style.style_divBox1)

        --支路标签
        local label=lvgl.label_create(itemCont, nil)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        lvgl.label_set_text(label, t.path..":")
        lvgl.obj_align(label,itemCont, lvgl.ALIGN_IN_LEFT_MID, 10, 0)

        --模式标签
        local ctlTab={"时间","光强"}
        local modeLabelCont=lvgl.cont_create(itemCont, nil)
        lvgl.obj_set_click(modeLabelCont,false)
        lvgl.obj_set_size(modeLabelCont, 60,30)
        lvgl.obj_add_style(modeLabelCont, lvgl.CONT_PART_MAIN, style.style_labelBgBlue)
        lvgl.cont_set_layout(modeLabelCont,lvgl.LAYOUT_CENTER)
        lvgl.obj_align(modeLabelCont,itemCont, lvgl.ALIGN_IN_LEFT_MID, 110, 0)

        local modeLabel=lvgl.label_create(modeLabelCont, nil)
        lvgl.label_set_recolor(modeLabel, true) 
        lvgl.obj_set_style_local_text_font(modeLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        lvgl.label_set_text(modeLabel,ctlTab[t.run.ctl+1])

        --湿度标签
        local humiLabel=lvgl.label_create(itemCont, nil)
        lvgl.obj_set_style_local_text_font(humiLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        lvgl.label_set_text(humiLabel,string.format('%.1fLux',t.run.humi))
        lvgl.obj_align(humiLabel,itemCont,lvgl.ALIGN_IN_LEFT_MID,175, 0)

        --状态标签
        local statusLabelCont=lvgl.cont_create(itemCont, nil)
        lvgl.obj_set_click(statusLabelCont,false)
        lvgl.obj_set_size(statusLabelCont, 150,30)
        -- lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN, style.style_labelBgGreen)
        -- lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN, style.style_labelBgRed)
        lvgl.obj_align(statusLabelCont,itemCont, lvgl.ALIGN_IN_LEFT_MID, 252, 0)

        local function paddingStatusLabelCont(statusLabelCont)
            -- lvgl.obj_clean(statusLabelCont)
            local function makeStatic(cont,style,text)
                lvgl.obj_clean(cont)
                lvgl.obj_add_style(cont, lvgl.CONT_PART_MAIN,style)
                local label=lvgl.label_create(cont, nil)
                lvgl.label_set_recolor(label, true)
                lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
                lvgl.label_set_text(label,"#FFFFFF "..text)
                lvgl.obj_align(label,cont, lvgl.ALIGN_CENTER,0, 0)
            end

            local function makecontTime(statusLabelCont,m_style,text)
                lvgl.obj_clean(statusLabelCont)
                lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN,m_style)

                local secbg=lvgl.cont_create(statusLabelCont, nil)
                lvgl.obj_set_click(secbg,false)
                lvgl.obj_set_size(secbg, 40,25)
                lvgl.obj_add_style(secbg, lvgl.CONT_PART_MAIN, style.style_labelBgYellow)
                lvgl.cont_set_layout(secbg,lvgl.LAYOUT_CENTER)
                lvgl.obj_align(secbg,statusLabelCont, lvgl.ALIGN_IN_LEFT_MID,106, 0)

                local labelLeft=lvgl.label_create(statusLabelCont, nil)
                lvgl.label_set_recolor(labelLeft, true) 
                lvgl.obj_set_style_local_text_font(labelLeft, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
                lvgl.label_set_text(labelLeft, "#FFFFFF "..text)
                lvgl.obj_align(labelLeft,secbg, lvgl.ALIGN_OUT_LEFT_MID, -5, 0)

                local labelRight=lvgl.label_create(statusLabelCont, nil)
                lvgl.label_set_recolor(labelRight, true) 
                lvgl.obj_set_style_local_text_font(labelRight, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
                lvgl.label_set_text(labelRight, "#FFFFFF 秒")
                lvgl.obj_align(labelRight,secbg, lvgl.ALIGN_OUT_RIGHT_MID, 5, 0)

                local secLabel=lvgl.label_create(secbg, nil)
                lvgl.obj_set_style_local_text_font(secLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
                lvgl.label_set_text(secLabel, t.run.counterDown)
            end

            local static_red={
                {s=0,text="补光停止"},
                {s=5,text="补光暂停"},
                {s=6,text="补光异常"},
                {s=8,text="消毒异常"},
                {s=9,text="时控停止"},
                {s=10,text="联动停止"},
                {s=11,text="自动保护"},
            }
            local static_green={
                {s=1,text="自动补光"},
                {s=2,text="自动停止"},
            }
            local cont_time={
                {s=3,text="正在补光"},
                {s=4,text="停止补光"},
                {s=7,text="正在消毒"},
            }

            local function statusAns(st)--判断状态
                local function have(t,v)
                    for k,l in pairs(t) do
                        if l.s==v then return true,l.text end
                    end
                    return false
                end
                local _,t=have(static_red,st)
                if _ then return true,1,t end
                _,t=have(static_green,st)
                if _ then return true,2,t end
                _,t=have(cont_time,st)
                if _ then return true,3,t end

                return _
            end

            local _,status,text=statusAns(t.run.status)

            if _ then
                if status==1 then
                    makeStatic(statusLabelCont,style.style_labelBgRed,text)
                elseif status==2 then
                    makeStatic(statusLabelCont,style.style_labelBgGreen,text)
                elseif status==3 then
                    makecontTime(statusLabelCont,style.style_labelBgGreen,text)
                end
            end
            -- if have(static_red,t.run.status) then
            --     lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN, style.style_labelBgRed)
            -- end
    
        end

        paddingStatusLabelCont(statusLabelCont)

        -- local secbg=lvgl.cont_create(statusLabelCont, nil)
        -- lvgl.obj_set_click(secbg,false)
        -- lvgl.obj_set_size(secbg, 40,25)
        -- lvgl.obj_add_style(secbg, lvgl.CONT_PART_MAIN, style.style_labelBgYellow)
        -- lvgl.cont_set_layout(secbg,lvgl.LAYOUT_CENTER)
        -- lvgl.obj_align(secbg,statusLabelCont, lvgl.ALIGN_IN_LEFT_MID,106, 0)

        -- local labelLeft=lvgl.label_create(statusLabelCont, nil)
        -- lvgl.label_set_recolor(labelLeft, true) 
        -- lvgl.obj_set_style_local_text_font(labelLeft, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        -- lvgl.label_set_text(labelLeft, "#FFFFFF 正在喷雾")
        -- lvgl.obj_align(labelLeft,secbg, lvgl.ALIGN_OUT_LEFT_MID, -5, 0)

        -- local labelRight=lvgl.label_create(statusLabelCont, nil)
        -- lvgl.label_set_recolor(labelRight, true) 
        -- lvgl.obj_set_style_local_text_font(labelRight, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        -- lvgl.label_set_text(labelRight, "#FFFFFF 秒")
        -- lvgl.obj_align(labelRight,secbg, lvgl.ALIGN_OUT_RIGHT_MID, 5, 0)

        -- local secLabel=lvgl.label_create(secbg, nil)
        -- lvgl.obj_set_style_local_text_font(secLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        -- lvgl.label_set_text(secLabel, t.run.counterDown)

        --开关
        local function switch_event_handler(obj, event)
            if event == lvgl.EVENT_VALUE_CHANGED then
                _G.BEEP()
                t.run.pwr=lvgl.switch_get_state(obj) and 1 or 0
                sys.publish("CTL_SWITCH",t.path, lvgl.switch_get_state(obj))
            end
        end
        local switch = lvgl.switch_create(itemCont, nil)
        lvgl.obj_set_size(switch ,60,30)
        lvgl.obj_add_style(switch, lvgl.SWITCH_PART_INDIC, style.style_switch)
        lvgl.obj_add_style(switch, lvgl.SWITCH_PART_KNOB, style.style_switch_KNOB)
        lvgl.obj_add_style(switch, lvgl.SWITCH_PART_BG, style.style_switch_BG)
        lvgl.obj_align(switch ,itemCont, lvgl.ALIGN_IN_LEFT_MID, 410, 0)
        if t.run.pwr==1 then 
            lvgl.switch_on(switch,lvgl.ANIM_OFF)
        else
            lvgl.switch_off(switch,lvgl.ANIM_OFF)
        end
        lvgl.obj_set_event_cb(switch, switch_event_handler)

        --设置
        function settingFnc(obj,event)
            if event == lvgl.EVENT_CLICKED then
                _G.BEEP()
                -- print("设置按下")
                -- _G.fragmentRunningStatusBox=false
                unInit()
                fragment_paraSetting.init(runningBox,t)
            end
        end

        local setting=lvgl.img_create(itemCont, nil)
        lvgl.img_set_src(setting,"/lua/setting_icon_big.png")
        lvgl.obj_align(setting,itemCont, lvgl.ALIGN_IN_LEFT_MID,480, 0)
        lvgl.obj_set_click(setting,true)
        lvgl.obj_set_event_cb(setting,settingFnc)

        --改变状态回调函数
        function changeStatus(what)
            -- -- print(what,_G.fragmentRunningStatusBox)
            -- if not _G.fragmentRunningStatusBox then return end
            -- -- print(modeLabel)
            if what=="CHANGE_MODE" then
                lvgl.label_set_text(modeLabel,ctlTab[t.run.ctl+1])
            elseif what=="CHANGE_HUMI" then
                lvgl.label_set_text(humiLabel,string.format('%.1fLux',t.run.humi))
            elseif what=="CHANGE_STATUS" then
                paddingStatusLabelCont(statusLabelCont)
            elseif what=="CHANGE_SWITCH" then
                if t.run.pwr==1 then 
                    lvgl.switch_on(switch,lvgl.ANIM_OFF)
                else
                    lvgl.switch_off(switch,lvgl.ANIM_OFF)
                end
            end
        end

        --[[
            
                --改变状态回调函数
        function changeStatus(what,status)
            -- -- print(what,_G.fragmentRunningStatusBox)
            -- if not _G.fragmentRunningStatusBox then return end
            -- -- print(modeLabel)
            if what=="CHANGE_MODE" then
                lvgl.label_set_text(modeLabel, "#FFFFFF "..status)
            elseif what=="CHANGE_HUMI" then
                lvgl.label_set_text(humiLabel, status)
            -- elseif what=="CHANGE_STATUS" then
            --     lvgl.label_set_text(secLabel, status)
            elseif what=="CHANGE_SWITCH" then
                if status then 
                    lvgl.switch_on(switch,lvgl.ANIM_OFF)
                else
                    lvgl.switch_off(switch,lvgl.ANIM_OFF)
                end
            end
        end
        --]]

        -- sys.subscribe(t.path,changeStatus)
        topicTableAdd(t.path,changeStatus)
        -- print(",changeStatus",changeStatus)

        

    end

    -- makeItem(itemList,"一支路","时间","36.0%",180)
    -- makeItem(itemList,"二支路","时间","33.2%",170)
    -- makeItem(itemList,"三支路","时间","38.9%",160)
    -- makeItem(itemList,"四支路","湿度","56.0%",150)
    -- makeItem(itemList,"五支路","湿度","98.6%",140)
    -- makeItem(itemList,"六支路","湿度","18.2%",130)

    for _, v in ipairs(t) do
        makeItem(itemList,v)
        -- print("make",_)
    end

end

function paddingRunningBoxSingle(runningBox,t)

    local labelBox=lvgl.cont_create(runningBox, nil)
    lvgl.obj_set_click(labelBox,false)
    lvgl.obj_set_size(labelBox, 560,60) 
    lvgl.obj_add_style(labelBox, lvgl.CONT_PART_MAIN, style.style_divBox)

    local label=lvgl.label_create(labelBox, nil)
        lvgl.label_set_recolor(label, true) 
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
        lvgl.label_set_text(label, "#217bda 单支路控制")
        lvgl.obj_align(label, labelBox, lvgl.ALIGN_IN_LEFT_MID,20,0)
    --设置
    function settingFnc(obj,event)
        if event == lvgl.EVENT_CLICKED then
            _G.BEEP()
            print("设置按下")
            unInit()
            fragment_paraSetting.init(runningBox,t)
        end
    end
    --设置
    local setting=lvgl.img_create(labelBox, nil)
    lvgl.img_set_src(setting,"/lua/setting_icon_big.png")
    lvgl.obj_align(setting,labelBox, lvgl.ALIGN_IN_RIGHT_MID, -20, 0)
    lvgl.obj_set_click(setting,true)
    lvgl.obj_set_event_cb(setting,settingFnc)

    local line=lvgl.cont_create(labelBox, nil)
    lvgl.obj_set_click(line,false)
    lvgl.obj_set_size(line, 560, 1) 
    lvgl.obj_add_style(line, lvgl.CONT_PART_MAIN, style.style_line)
    lvgl.obj_align(line, labelBox, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)

    --开关

    local function switch_event_handler(obj, event)
        if event == lvgl.EVENT_VALUE_CHANGED then
            _G.BEEP()
            print("State",t.path, lvgl.switch_get_state(obj))
            t.run.pwr=lvgl.switch_get_state(obj) and 1 or 0
        end
    end

    local switchLabel=lvgl.label_create(runningBox, nil)
    lvgl.obj_set_style_local_text_font(switchLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(switchLabel, "开关:")
    lvgl.obj_align(switchLabel, labelBox, lvgl.ALIGN_OUT_BOTTOM_LEFT, 20, 30)

    local switch = lvgl.switch_create(runningBox, nil)
    lvgl.obj_set_size(switch ,100,50)
    lvgl.obj_add_style(switch, lvgl.SWITCH_PART_INDIC, style.style_switch)
    -- lvgl.obj_add_style(switch, lvgl.SWITCH_PART_KNOB, style.style_switch)
    -- lvgl.obj_add_style(switch, lvgl.SWITCH_PART_BG, style.style_switch)
    lvgl.obj_align(switch ,switchLabel, lvgl.ALIGN_OUT_RIGHT_MID, 30, 0)
    if t.run.pwr==1 then 
        lvgl.switch_on(switch,lvgl.ANIM_OFF)
    else
        lvgl.switch_off(switch,lvgl.ANIM_OFF)
    end
    lvgl.obj_set_event_cb(switch,switch_event_handler)

    --模式
    local modeLabel=lvgl.label_create(runningBox, nil)
    lvgl.obj_set_style_local_text_font(modeLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(modeLabel, "模式:")
    lvgl.obj_align(modeLabel, switchLabel, lvgl.ALIGN_OUT_BOTTOM_LEFT, 0, 30)

    local modeLabelCont=lvgl.cont_create(runningBox, nil)
    lvgl.obj_set_click(modeLabelCont,false)
    lvgl.obj_set_size(modeLabelCont, 90,40)
    lvgl.obj_add_style(modeLabelCont, lvgl.CONT_PART_MAIN, style.style_labelBgBlue)
    lvgl.cont_set_layout(modeLabelCont,lvgl.LAYOUT_CENTER)
    lvgl.obj_align(modeLabelCont,modeLabel, lvgl.ALIGN_OUT_RIGHT_MID, 30, 0)

    local ctlTab={"时间","光强"}
    local modeLabelText=lvgl.label_create(modeLabelCont, nil)
    lvgl.label_set_recolor(modeLabelText, true) 
    lvgl.obj_set_style_local_text_font(modeLabelText, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(modeLabelText, "#FFFFFF "..ctlTab[t.run.ctl+1])

    --湿度
    local humiLabel=lvgl.label_create(runningBox, nil)
    lvgl.obj_set_style_local_text_font(humiLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(humiLabel, "湿度:")
    lvgl.obj_align(humiLabel, modeLabel, lvgl.ALIGN_OUT_BOTTOM_LEFT, 0, 30)

    local humiLabelText=lvgl.label_create(runningBox, nil)
    lvgl.obj_set_style_local_text_font(humiLabelText, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.obj_align(humiLabelText,humiLabel, lvgl.ALIGN_OUT_RIGHT_MID, 30, 0)
    lvgl.label_set_text(humiLabelText,string.format('%.1f%%',t.run.humi))

    --状态
    local statusLabel=lvgl.label_create(runningBox, nil)
    lvgl.obj_set_style_local_text_font(statusLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(statusLabel, "状态:")
    lvgl.obj_align(statusLabel,humiLabel, lvgl.ALIGN_OUT_BOTTOM_LEFT, 0, 30)

    local statusLabelCont=lvgl.cont_create(runningBox, nil)
    lvgl.obj_set_click(statusLabelCont,false)
    lvgl.obj_set_size(statusLabelCont, 250,50)
    -- lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN, style.style_labelBgGreen)
    lvgl.obj_align(statusLabelCont,statusLabel, lvgl.ALIGN_OUT_RIGHT_MID, 30, 0)


    local function paddingStatusLabelCont(statusLabelCont)
        -- lvgl.obj_clean(statusLabelCont)
        local function makeStatic(cont,style,text)
            lvgl.obj_clean(cont)
            lvgl.obj_add_style(cont, lvgl.CONT_PART_MAIN,style)
            local label=lvgl.label_create(cont, nil)
            lvgl.label_set_recolor(label, true)
            lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
            lvgl.label_set_text(label,"#FFFFFF "..text)
            lvgl.obj_align(label,cont, lvgl.ALIGN_CENTER,0, 0)
        end

        local function makecontTime(statusLabelCont,m_style,text)
            lvgl.obj_clean(statusLabelCont)
            lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN,m_style)


            local secbg=lvgl.cont_create(statusLabelCont, nil)
            lvgl.obj_set_click(secbg,false)
            lvgl.obj_set_size(secbg, 50,32)
            lvgl.obj_add_style(secbg, lvgl.CONT_PART_MAIN, style.style_labelBgYellow)
            lvgl.cont_set_layout(secbg,lvgl.LAYOUT_CENTER)
            lvgl.obj_align(secbg,statusLabelCont, lvgl.ALIGN_IN_LEFT_MID,145, 0)

            local labelLeft=lvgl.label_create(statusLabelCont, nil)
            lvgl.label_set_recolor(labelLeft, true) 
            lvgl.obj_set_style_local_text_font(labelLeft, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
            lvgl.label_set_text(labelLeft, "#FFFFFF "..text)
            lvgl.obj_align(labelLeft,secbg, lvgl.ALIGN_OUT_LEFT_MID, -5, 0)

            local labelRight=lvgl.label_create(statusLabelCont, nil)
            lvgl.label_set_recolor(labelRight, true) 
            lvgl.obj_set_style_local_text_font(labelRight, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
            lvgl.label_set_text(labelRight, "#FFFFFF 秒")
            lvgl.obj_align(labelRight,secbg, lvgl.ALIGN_OUT_RIGHT_MID, 5, 0)

            local secLabel=lvgl.label_create(secbg, nil)
            lvgl.obj_set_style_local_text_font(secLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
            lvgl.label_set_text(secLabel, t.run.counterDown)
        end

        local static_red={
            {s=0,text="补光停止"},
            {s=5,text="补光暂停"},
            {s=6,text="补光异常"},
            {s=8,text="消毒异常"},
            {s=9,text="时控停止"},
            {s=10,text="联动停止"},
            {s=11,text="自动保护"},
        }
        local static_green={
            {s=1,text="自动补光"},
            {s=2,text="自动停止"},
        }
        local cont_time={
            {s=3,text="正在补光"},
            {s=4,text="停止补光"},
            {s=7,text="正在消毒"},
        }

        local function statusAns(st)--判断状态
            local function have(t,v)
                for k,l in pairs(t) do
                    if l.s==v then return true,l.text end
                end
                return false
            end
            local _,t=have(static_red,st)
            if _ then return true,1,t end
            _,t=have(static_green,st)
            if _ then return true,2,t end
            _,t=have(cont_time,st)
            if _ then return true,3,t end

            return _
        end

        local _,status,text=statusAns(t.run.status)

        if _ then
            if status==1 then
                makeStatic(statusLabelCont,style.style_labelBgRed,text)
            elseif status==2 then
                makeStatic(statusLabelCont,style.style_labelBgGreen,text)
            elseif status==3 then
                makecontTime(statusLabelCont,style.style_labelBgGreen,text)
            end
        end
        -- if have(static_red,t.run.status) then
        --     lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN, style.style_labelBgRed)
        -- end

    end


    paddingStatusLabelCont(statusLabelCont)


    --改变状态回调函数
    function changeStatus(what,status)
        if what=="CHANGE_MODE" then
            lvgl.label_set_text(modeLabelText, ctlTab[t.run.ctl+1])
        elseif what=="CHANGE_HUMI" then
            lvgl.label_set_text(humiLabelText,string.format('%.1f%%',t.run.humi))
        elseif what=="CHANGE_STATUS" then 
            paddingStatusLabelCont(statusLabelCont)
        elseif what=="CHANGE_SWITCH" then 
            if t.run.pwr==1 then 
                lvgl.switch_on(switch,lvgl.ANIM_OFF)
            else
                lvgl.switch_off(switch,lvgl.ANIM_OFF)
            end
        end
    end

    topicTableAdd(t.path,changeStatus)

end


function initRunningBox(running_box)
    local runningTab=UIConfig.getBranchTable()

    if #runningTab==1 then
        paddingRunningBoxSingle(running_box,runningTab[1])
        else
        paddingRunningBox(running_box,runningTab)
    end

    -- sys.timerLoopStart(function(e) print(type(e)) end,1000,runningTab)

end

function init(cont)
    lvgl.obj_clean(cont)
    initRunningBox(cont)
    
end


function unInit()
    -- print("取消订阅")
    for id, callback in pairs(topicTable) do
        sys.unsubscribe(id, callback)
    end
end
module(..., package.seeall)
_G.KEYBOARD=nil
_G.KEYBOARDTEMPTEXT=nil
_G.uuu=nil

function init(cont,x,y,align, px,py,text,fnc)

    keyCb=function(obj, e)
        -- 默认处理事件
        lvgl.keyboard_def_event_cb(_G.KEYBOARD, e)
        if e == lvgl.EVENT_CLICKED then
            _G.BEEP()
        elseif(e == lvgl.EVENT_CANCEL or e ==lvgl.EVENT_APPLY)then
            _G.BEEP()
            uninitKeyBoard(e)
        end
    end

    function uninitKeyBoard(cmd)
        local temptxt=lvgl.keyboard_get_textarea(_G.KEYBOARD)
        if cmd==lvgl.EVENT_CANCEL then 
            lvgl.textarea_set_text(temptxt,_G.KEYBOARDTEMPTEXT)
        elseif cmd==lvgl.EVENT_APPLY then
            if  lvgl.textarea_get_text(temptxt)=="" then 
                lvgl.textarea_set_text(temptxt,_G.KEYBOARDTEMPTEXT)
            else
                if _G.uuu then _G.uuu(temptxt) end
            end
        end
        lvgl.keyboard_set_textarea(_G.KEYBOARD, nil)
        lvgl.obj_del(_G.KEYBOARD)
        _G.KEYBOARD = nil
        _G.KEYBOARDTEMPTEXT=nil
        _G.uuu=nil
    end

    function initKeyBoard(textArea,fnc)
        -- print("创建键盘",textArea)
        --创建一个 KeyBoard
        _G.KEYBOARD = lvgl.keyboard_create(lvgl.scr_act(), nil)
        -- lvgl.keyboard_set_mode(_G.KEYBOARD,lvgl.KEYBOARD_MODE_NUMBER)
        lvgl.keyboard_set_mode(_G.KEYBOARD,lvgl.KEYBOARD_MODE_NUM)
        lvgl.obj_set_size(_G.KEYBOARD, 300,200) 
        lvgl.obj_align(_G.KEYBOARD,textArea, lvgl.ALIGN_OUT_LEFT_MID, 0, 0)
        --设置 KeyBoard 的光标是否显示
        lvgl.keyboard_set_cursor_manage(_G.KEYBOARD, false)
        --为 KeyBoard 设置一个文本区域
        lvgl.keyboard_set_textarea(_G.KEYBOARD, textArea)
        _G.KEYBOARDTEMPTEXT=lvgl.textarea_get_text(textArea)
        lvgl.textarea_set_text(textArea,"")
        lvgl.obj_set_event_cb(_G.KEYBOARD, keyCb)
        _G.uuu=fnc
    end

    function textAreaCb(obj, e)
        if (e == lvgl.EVENT_CLICKED) and not _G.KEYBOARD then
            _G.BEEP()
            initKeyBoard(obj,fnc)
            elseif (e == lvgl.EVENT_DEFOCUSED and _G.KEYBOARD~=nil )  then
                uninitKeyBoard(lvgl.EVENT_CANCEL)
        end
    end

    local textArea = lvgl.textarea_create(cont, nil)
    lvgl.textarea_set_accepted_chars(textArea, "0123456789")
    -- lvgl.textarea_set_max_length(textArea,2)
    --[[
    lvgl.obj_set_style_local_text_font(textArea, lvgl.PAGE_PART_SCRL, lvgl.STATE_DEFAULT, font32)
    lvgl.obj_set_style_local_text_color(textArea,lvgl.TEXTAREA_PART_MAIN,lvgl.STATE_DEFAULT,lvgl.color_hex(0xFFFFFF))
    lvgl.obj_set_size(textArea, x, y)
    if text==nil then
        lvgl.textarea_set_text(textArea, "")
        else
            lvgl.textarea_set_text(textArea, text)
    end
    lvgl.textarea_set_cursor_hidden(textArea, true)
    lvgl.textarea_set_one_line(textArea, true)
    lvgl.textarea_set_text_align(textArea, lvgl.LABEL_ALIGN_CENTER)
    -- lvgl.textarea_set_accepted_chars(textArea, "0123456789")
    lvgl.obj_add_style(textArea, lvgl.CONT_PART_MAIN, style.style_labelBgBlue_noRadius)
    -- lvgl.obj_add_style(textArea, lvgl.CONT_PART_MAIN, style.style_temp)
    lvgl.obj_align(textArea, cont, align, px,py)
    lvgl.obj_set_event_cb(textArea, textAreaCb)--]]

    ----[[
    lvgl.obj_set_size(textArea, x, y)
    if text==nil then
        lvgl.textarea_set_text(textArea, "")
        else
            lvgl.textarea_set_text(textArea, text)
    end
    lvgl.textarea_set_cursor_hidden(textArea, true)
    lvgl.textarea_set_one_line(textArea, true)
    lvgl.textarea_set_text_align(textArea, lvgl.LABEL_ALIGN_CENTER)
    local s=lvgl.style_t()
    lvgl.style_init(s)
    lvgl.style_copy(s,style.style_labelBgBlue_noRadius)
    lvgl.style_set_text_font(s,lvgl.STATE_DEFAULT,font32)
    lvgl.style_set_text_color(s,lvgl.STATE_DEFAULT,lvgl.color_hex(0xFFFFFF))
    lvgl.obj_add_style(textArea, lvgl.OBJ_PART_MAIN, s)
    lvgl.obj_align(textArea, cont, align, px,py)
    lvgl.obj_set_event_cb(textArea, textAreaCb)--]]


    return textArea
end

-- local t=make(_G.body,200,40,lvgl.ALIGN_IN_RIGHT_MID, 0, 0)
-- local t1=make(_G.body,200,40,lvgl.ALIGN_IN_RIGHT_MID, 0, -100)
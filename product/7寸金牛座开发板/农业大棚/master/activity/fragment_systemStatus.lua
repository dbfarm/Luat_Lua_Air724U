module(..., package.seeall)


local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end

function init(sysCont,runningBox)
    local t=UIConfig.getSystemStatus()

    -- function makesysLable(topic,imgpath,reg,text)
    function makesysLable(topic)

        local label_click = function(obj, event)
            if event == lvgl.EVENT_CLICKED then
                _G.BEEP()
                sys.publish("SYSTEM_LABEL_CLICK",topic)
            end
        end

        local motorStatus={"正常","高温","过载"}
        local pressureStatus={"正常","过压","欠压"}
        local waterStatus={"良好","较好","一般","较差","很差"}
        local filterStatus={"正常","更换"}
        local oilStatus={"正常","更换"}

        local iconTab={
            SYSTEM_RUNNING="/lua/running_icon.png",
            SYSTEM_MOTO="/lua/moto_icon.png",
            SYSTEM_PRESSURE="/lua/pressure_icon.png",
            SYSTEM_WATER="/lua/water_icon.png",
            SYSTEM_FILTER="/lua/filter_icon.png",
            SYSTEM_OIL="/lua/oil_icon.png"
        }

        function getText(topic)
            if topic=="SYSTEM_RUNNING" then return string.format("补光:%sH",t.runTimer) end
            if topic=="SYSTEM_MOTO"    then return string.format("系统:%s",motorStatus[t.motor+1]) end
            if topic=="SYSTEM_PRESSURE" then return string.format("棚压:%s",pressureStatus[t.pressure+1]) end
            if topic=="SYSTEM_WATER" then return string.format("肥料:%s",waterStatus[t.tds+1]) end
            if topic=="SYSTEM_FILTER" then return string.format("空气:%s",filterStatus[t.filter+1]) end
            if topic=="SYSTEM_OIL" then return string.format("灌溉:%s",oilStatus[t.oil+1]) end
        end

        local imgpath=iconTab[topic]
        local text=getText(topic)

        local cnt=lvgl.cont_create(sysCont, nil)
        -- lvgl.obj_set_click(cnt,false)
        lvgl.obj_set_event_cb(cnt,label_click)
        lvgl.obj_set_size(cnt, 197, 43)
        lvgl.obj_add_style(cnt, lvgl.CONT_PART_MAIN, style.style_msgBg)
        local img=lvgl.img_create(cnt, nil)
        lvgl.img_set_src(img,imgpath)
        lvgl.obj_align(img, cnt, lvgl.ALIGN_IN_LEFT_MID, 10, 0)
        local label=lvgl.label_create(cnt, nil)    
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,text)
        lvgl.obj_align(label, img, lvgl.ALIGN_OUT_RIGHT_MID, 10, 0)

        --订阅系统运行状态
        function change()
            lvgl.label_set_text(label,getText(topic))
        end
        
        topicTableAdd(topic,change)
    end

    function makeQRbtn()
        event_handler = function(obj, event)
            if event == lvgl.EVENT_CLICKED then
                _G.BEEP()
                if _G.fragmentQRcode then
                    fragment_qrcode.uninit()
                    fragment_RunStatus.init(runningBox)
                else
                    fragment_RunStatus.unInit()
                    fragment_qrcode.init(runningBox)
                end
            end
        end
        local btn = lvgl.cont_create(sysCont, nil)
        lvgl.obj_set_size(btn, 197, 43)
        lvgl.obj_add_style(btn, lvgl.BTN_PART_MAIN, style.style_QRbtnBg)
        lvgl.obj_set_event_cb(btn, event_handler)
        local label=lvgl.label_create(btn, nil)
        lvgl.label_set_recolor(label, true) 
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        lvgl.label_set_text(label, "#FFFFFF 二维码")
        lvgl.obj_align(label, btn, lvgl.ALIGN_CENTER, 0, 0)
    end


    -- local motorStatus={"正常","高温","过载"}
    -- local pressureStatus={"正常","过压","欠压"}
    -- local waterStatus={"良好","较好","一般","较差","很差"}
    -- local filterStatus={"正常","更换"}
    -- local oilStatus={"正常","更换"}
    
    -- makesysLable("SYSTEM_RUNNING","/lua/running_icon.png","运行:%sH",t.runTimer)
    -- makesysLable("SYSTEM_MOTO","/lua/moto_icon.png","电机:%s",motorStatus[t.motor+1])
    -- makesysLable("SYSTEM_PRESSURE","/lua/pressure_icon.png","压力:%s",pressureStatus[t.pressure+1])
    -- makesysLable("SYSTEM_WATER","/lua/water_icon.png","水质:%s",waterStatus[t.tds+1])
    -- makesysLable("SYSTEM_FILTER","/lua/filter_icon.png","滤芯:%s",filterStatus[t.filter+1])
    -- makesysLable("SYSTEM_OIL","/lua/oil_icon.png","泵油:%s",oilStatus[t.oil+1])

    makesysLable("SYSTEM_RUNNING")
    makesysLable("SYSTEM_MOTO")
    makesysLable("SYSTEM_PRESSURE")
    makesysLable("SYSTEM_WATER")
    makesysLable("SYSTEM_FILTER")
    makesysLable("SYSTEM_OIL")
    makeQRbtn()

end

function unInit()
    -- print("取消订阅")
    for id, callback in pairs(topicTable) do
        sys.unsubscribe(id, callback)
    end
end
module(..., package.seeall)
---------------------------
-- font24=lvgl.font_load(spi.SPI_1,24,100,110)
require "fragment_systemStatus"
require "fragment_RunStatus"
require "fragment_qrcode"
require "fragment_paraSetting"
require "fragment_timeSetting"
require "fragment_home"
require "fragment_Screensaver"



_G.body=lvgl.cont_create(lvgl.scr_act(), nil)
lvgl.obj_set_click(body,false)
lvgl.obj_set_size(body, 800, 480) 
lvgl.obj_add_style(body, lvgl.CONT_PART_MAIN, style.style_body)


function makeHead(cont)
    headCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(headCont,false)
    lvgl.obj_set_size(headCont, 800, 60) 
    lvgl.obj_add_style(headCont, lvgl.CONT_PART_MAIN, style.style_divBox)

    local sysname=lvgl.label_create(headCont, nil)
    lvgl.obj_set_style_local_text_font(sysname, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font36)
    lvgl.label_set_recolor(sysname, true)
    lvgl.label_set_text(sysname,"#FFFFFF 农业大棚控制系统")
    lvgl.obj_align(sysname, headCont, lvgl.ALIGN_IN_LEFT_MID, 35, 0)

    --状态栏
    local statusLabelCont=lvgl.cont_create(headCont, nil)
    lvgl.obj_set_click(statusLabelCont,false)
    lvgl.obj_set_size(statusLabelCont,460, 40)
    -- lvgl.cont_set_layout(statusLabelCont,lvgl.LAYOUT_ROW_MID)
    lvgl.obj_align(statusLabelCont, headCont, lvgl.ALIGN_IN_RIGHT_MID, -20, 0)
    lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN, style.style_statusLabel)

    function timeFnc(label,text)
        lvgl.label_set_text(label,"#FFFFFF "..text)
    end
    
    --时间标签
    local dateLabel=lvgl.label_create(statusLabelCont, nil)
    lvgl.obj_set_style_local_text_font(dateLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_recolor(dateLabel, true) 
    lvgl.label_set_text(dateLabel,"")
    lvgl.obj_align(dateLabel,statusLabelCont, lvgl.ALIGN_IN_LEFT_MID,20, 0)
    sys.subscribe("SYSTEM_CHANGE_TIME",function(date)
        lvgl.label_set_text(dateLabel,"#FFFFFF "..date)
        end)

    --设置按钮

    local settingBtn=lvgl.cont_create(statusLabelCont, nil)
        -- lvgl.obj_set_click(settingBtn,true)
        lvgl.obj_set_size(settingBtn, 60,32)
        lvgl.obj_add_style(settingBtn, lvgl.CONT_PART_MAIN, style.style_labelBgBlue)
        lvgl.cont_set_layout(settingBtn,lvgl.LAYOUT_CENTER)
        lvgl.obj_align(settingBtn,statusLabelCont, lvgl.ALIGN_IN_LEFT_MID,280, 0)

    local settingLabel=lvgl.label_create(settingBtn, nil)
        lvgl.label_set_recolor(settingLabel, true) 
        lvgl.obj_set_style_local_text_font(settingLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        lvgl.label_set_text(settingLabel, "设置")

    local function settingHandler(obj, event)
            if event == lvgl.EVENT_CLICKED then
                _G.BEEP()
                -- print("设置按钮按下")
                if not _G.fragmentTimeSetting then
                    fragment_home.UnInit()
                    fragment_timeSetting.init(_G.BOX)
                    lvgl.label_set_text(settingLabel, "返回")
                else
                    fragment_timeSetting.save()
                    fragment_timeSetting.uninit()
                    fragment_home.init(_G.BOX)
                    lvgl.label_set_text(settingLabel, "设置")
                end
            end
        end

        lvgl.obj_set_event_cb(settingBtn,settingHandler)

    --4G 图标

    local signImg=lvgl.img_create(statusLabelCont, nil)
    lvgl.img_set_src(signImg,"/lua/nosin_icon.png")
    lvgl.obj_align(signImg,settingBtn, lvgl.ALIGN_OUT_RIGHT_MID, 20, 0)

    sys.subscribe("RSSI_CHANGE",function(img)
        lvgl.img_set_src(signImg,img)
    end)

end

function makeBox(cont)
    _G.BOX=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(BOX,false)
    lvgl.obj_set_size(BOX, 800,415) 
    lvgl.obj_add_style(BOX, lvgl.CONT_PART_MAIN, style.style_divBox)
    lvgl.obj_align(BOX, cont, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
end

makeHead(_G.body)
makeBox(_G.body)

function activityInit()


    makeHead(_G.body)
    makeBox(_G.body)
    fragment_home.init(_G.BOX)
    -- fragment_timeSetting.init(_G.BOX)

end
module(..., package.seeall)
--参与UI交互的数据定义
--控制模式按照枚举定义，点击轮换
--系统状态的报警可通过点击5下清除，比如点击5下--滤芯：更换--将状态切换为正常

--UI的大棚数与时控的段数可根据这个表来动态配置，比如这个表配置了10大棚，UI就是10大棚。。。


--Ui数据总表
UIParams = {
    branch={
                --大棚1
            {
                path="01大棚",
            --运行
            run = {
                pwr = 0,--开关机：0关机，1开机
                ctl = 0,--控制模式：0时间，1湿度（不同机型枚举不同，做一种打个框架即可）
                humi = 45,--湿度显示
                counterDown = 80,--倒计时显示
                --显示系统状态：
                --0系统停止（红色色），1自动喷雾（绿色），2自动停止（绿色）
                --3正在喷雾XXXX秒（绿色），4停止喷雾XXXX秒（绿色），5系统暂停（红色）
                --6水箱缺水（红色），7正在补水XXXX秒（绿色），8药箱缺药（红色）
                --9时控停止（红色），10联动停止（红0），11自动保护（红色）
                status = 0, 
            },
            --设置
            set = {
                ontime = 80,--开启时间
                offtime = 180,--关闭时间
                humih = 30,--湿度上限
                humil = 20,--湿度下限
            },
            --传感器
            sensor = {
                humi = 90,--湿度
            },
            --时控
            timeControl = {
                {
                    seg="一段",
                    monday = 0,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 0,
                    ontimeHour = 0,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 0,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
                {
                    seg="二段",
                    monday = 0,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 0,
                    ontimeHour = 0,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 0,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
                {
                    seg="三段",
                    monday = 0,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 0,
                    ontimeHour = 0,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 0,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
                {
                    seg="四段",
                    monday = 0,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 0,
                    ontimeHour = 0,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 0,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
                --[[
                {
                    seg="五段",
                    monday = 1,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 1,
                    ontimeHour = 8,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 9,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
                {
                    seg="六段",
                    monday = 0,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 1,
                    friday = 0,
                    saturday = 1,
                    sunday = 1,
                    ontimeHour = 1,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 3,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 1,--段生效开关，0失效，1生效
                },
                --]]
            },
        },

        ----[[
                        --大棚2
            {
                path="02大棚",
            --运行
            run = {
                pwr = 1,--开关机：0关机，1开机
                ctl = 1,--控制模式：0时间，1湿度（不同机型枚举不同，做一种打个框架即可）
                humi = 88,--湿度显示
                counterDown = 80,--倒计时显示
                --显示系统状态：
                --0系统停止（红色色），1自动喷雾（绿色），2自动停止（绿色）
                --3正在喷雾XXXX秒（绿色），4停止喷雾XXXX秒（绿色），5系统暂停（红色）
                --6水箱缺水（红色），7正在补水XXXX秒（绿色），8药箱缺药（红色）
                --9时控停止（红色），10联动停止（红色），11自动保护（红色）
                status = 1, 
            },
            --设置
            set = {
                ontime = 80,--开启时间
                offtime = 180,--关闭时间
                humih = 90,--湿度上限
                humil = 80,--湿度下限
            },
            --传感器
            sensor = {
                humi = 90,--湿度
            },
            --时控
            timeControl = {
                {
                    seg="一段",
                    monday = 1,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 1,
                    ontimeHour = 8,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 9,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
            },
        },
                ----[[
                        --大棚3
            {
                path="03大棚",
            --运行
            run = {
                pwr = 1,--开关机：0关机，1开机
                ctl = 1,--控制模式：0时间，1湿度（不同机型枚举不同，做一种打个框架即可）
                humi = 88,--湿度显示
                counterDown = 80,--倒计时显示
                --显示系统状态：
                --0系统停止（红色色），1自动喷雾（绿色），2自动停止（绿色）
                --3正在喷雾XXXX秒（绿色），4停止喷雾XXXX秒（绿色），5系统暂停（红色）
                --6水箱缺水（红色），7正在补水XXXX秒（绿色），8药箱缺药（红色）
                --9时控停止（红色），10联动停止（红色），11自动保护（红色）
                status = 2, 
            },
            --设置
            set = {
                ontime = 80,--开启时间
                offtime = 180,--关闭时间
                humih = 90,--湿度上限
                humil = 80,--湿度下限
            },
            --传感器
            sensor = {
                humi = 90,--湿度
            },
            --时控
            timeControl = {
                {
                    seg="一段",
                    monday = 1,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 1,
                    thursday = 0,
                    friday = 1,
                    saturday = 0,
                    sunday = 1,
                    ontimeHour = 8,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 9,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 1,--段生效开关，0失效，1生效
                },
            },
        },
                        --大棚4
            {
                path="04大棚",
            --运行
            run = {
                pwr = 0,--开关机：0关机，1开机
                ctl = 1,--控制模式：0时间，1湿度（不同机型枚举不同，做一种打个框架即可）
                humi = 88,--湿度显示
                counterDown = 60,--倒计时显示
                --显示系统状态：
                --0系统停止（红色色），1自动喷雾（绿色），2自动停止（绿色）
                --3正在喷雾XXXX秒（绿色），4停止喷雾XXXX秒（绿色），5系统暂停（红色）
                --6水箱缺水（红色），7正在补水XXXX秒（绿色），8药箱缺药（红色）
                --9时控停止（红色），10联动停止（红色），11自动保护（红色）
                status = 5, 
            },
            --设置
            set = {
                ontime = 80,--开启时间
                offtime = 180,--关闭时间
                humih = 90,--湿度上限
                humil = 80,--湿度下限
            },
            --传感器
            sensor = {
                humi = 90,--湿度
            },
            --时控
            timeControl = {
                {
                    seg="一段",
                    monday = 1,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 1,
                    ontimeHour = 8,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 9,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
            },
        },
                        --大棚5
            {
                path="05大棚",
            --运行
            run = {
                pwr = 1,--开关机：0关机，1开机
                ctl = 1,--控制模式：0时间，1湿度（不同机型枚举不同，做一种打个框架即可）
                humi = 33,--湿度显示
                counterDown = 80,--倒计时显示
                --显示系统状态：
                --0系统停止（红色色），1自动喷雾（绿色），2自动停止（绿色）
                --3正在喷雾XXXX秒（绿色），4停止喷雾XXXX秒（绿色），5系统暂停（红色）
                --6水箱缺水（红色），7正在补水XXXX秒（绿色），8药箱缺药（红色）
                --9时控停止（红色），10联动停止（红色），11自动保护（红色）
                status = 6, 
            },
            --设置
            set = {
                ontime = 80,--开启时间
                offtime = 180,--关闭时间
                humih = 90,--湿度上限
                humil = 80,--湿度下限
            },
            --传感器
            sensor = {
                humi = 90,--湿度
            },
            --时控
            timeControl = {
                {
                    seg="一段",
                    monday = 1,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 1,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 0,
                    ontimeHour = 7,--开启时间.小时
                    ontimeMinite = 30,--开启时间.分钟
                    offtimeHour = 9,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
            },
        },
                        --大棚6
            {
                path="06大棚",
            --运行
            run = {
                pwr = 1,--开关机：0关机，1开机
                ctl = 1,--控制模式：0时间，1湿度（不同机型枚举不同，做一种打个框架即可）
                humi = 88,--湿度显示
                counterDown = 80,--倒计时显示
                --显示系统状态：
                --0系统停止（红色色），1自动喷雾（绿色），2自动停止（绿色）
                --3正在喷雾XXXX秒（绿色），4停止喷雾XXXX秒（绿色），5系统暂停（红色）
                --6水箱缺水（红色），7正在补水XXXX秒（绿色），8药箱缺药（红色）
                --9时控停止（红色），10联动停止（红色），11自动保护（红色）
                status = 8, 
            },
            --设置
            set = {
                ontime = 80,--开启时间
                offtime = 180,--关闭时间
                humih = 90,--湿度上限
                humil = 80,--湿度下限
            },
            --传感器
            sensor = {
                humi = 90,--湿度
            },
            --时控
            timeControl = {
                {
                    seg="一段",
                    monday = 0,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 1,
                    friday = 0,
                    saturday = 0,
                    sunday = 1,
                    ontimeHour = 8,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 9,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 1,--段生效开关，0失效，1生效
                },
            },
        },
                        --大棚7
            {
                path="07大棚",
            --运行
            run = {
                pwr = 1,--开关机：0关机，1开机
                ctl = 1,--控制模式：0时间，1湿度（不同机型枚举不同，做一种打个框架即可）
                humi = 88,--湿度显示
                counterDown = 80,--倒计时显示
                --显示系统状态：
                --0系统停止（红色色），1自动喷雾（绿色），2自动停止（绿色）
                --3正在喷雾XXXX秒（绿色），4停止喷雾XXXX秒（绿色），5系统暂停（红色）
                --6水箱缺水（红色），7正在补水XXXX秒（绿色），8药箱缺药（红色）
                --9时控停止（红色），10联动停止（红色），11自动保护（红色）
                status = 9, 
            },
            --设置
            set = {
                ontime = 80,--开启时间
                offtime = 180,--关闭时间
                humih = 90,--湿度上限
                humil = 80,--湿度下限
            },
            --传感器
            sensor = {
                humi = 90,--湿度
            },
            --时控
            timeControl = {
                {
                    seg="一段",
                    monday = 1,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 1,
                    ontimeHour = 8,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 9,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
            },
        },
                        --大棚8
            {
                path="08大棚",
            --运行
            run = {
                pwr = 1,--开关机：0关机，1开机
                ctl = 1,--控制模式：0时间，1湿度（不同机型枚举不同，做一种打个框架即可）
                humi = 88,--湿度显示
                counterDown = 80,--倒计时显示
                --显示系统状态：
                --0系统停止（红色色），1自动喷雾（绿色），2自动停止（绿色）
                --3正在喷雾XXXX秒（绿色），4停止喷雾XXXX秒（绿色），5系统暂停（红色）
                --6水箱缺水（红色），7正在补水XXXX秒（绿色），8药箱缺药（红色）
                --9时控停止（红色），10联动停止（红色），11自动保护（红色）
                status = 11, 
            },
            --设置
            set = {
                ontime = 80,--开启时间
                offtime = 180,--关闭时间
                humih = 90,--湿度上限
                humil = 80,--湿度下限
            },
            --传感器
            sensor = {
                humi = 90,--湿度
            },
            --时控
            timeControl = {
                {
                    seg="一段",
                    monday = 1,--0关闭，1开启
                    tuesday = 0,
                    wednesday = 0,
                    thursday = 0,
                    friday = 0,
                    saturday = 0,
                    sunday = 1,
                    ontimeHour = 8,--开启时间.小时
                    ontimeMinite = 0,--开启时间.分钟
                    offtimeHour = 9,--关闭时间.小时
                    offtimeMinite = 0,--关闭时间.分钟
                    switch = 0,--段生效开关，0失效，1生效
                },
            },
        },
        --]]
    },


    --系统状态
    systemStatus = {
        runTimer = 80,--累计使用时间
        motor = 0,--电机状态，0正常，1高温，2过载
        pressure = 0,--压力状态，0正常，1过压，2欠压
        tds = 0,--水质，0良好，1，较好，2一般，3较差，4很差
        filter = 0,--滤芯状态，0正常，1更换
        oil = 0,--泵油状态，0正常，1更换
    },
    --二维码
    qrcode = 'www.baidu.com',--平台下发的二维码

    --屏保图片
    screensaver={"Screensaver2.jpg","Screensaver3.jpg","Screensaver1.jpg"}

}


function getBranchTable()
    return UIParams.branch
end

function getSystemStatus()
    return UIParams.systemStatus
end

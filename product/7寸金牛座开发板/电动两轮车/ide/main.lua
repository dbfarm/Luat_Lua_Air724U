PROJECT = "7inch_car"
VERSION = "1.0.1"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"

_G.LCD_W = 800
_G.LCD_H = 480
require "log"
require "sys"
require "http"
require "misc"
require "pins"
require "mipi"
require "tp"

require "config"
require "nvm"
require "symbol"

nvm.init("config.lua")

require "ProjectSwitch"

lvgl.init(function()
    -- if not lvgl.indev_get_emu_touch then lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270) end
    spi.setup(spi.SPI_1, 1, 1, 8, 5000000, 1)
end, tp.input)
-- spi.setup(spi.SPI_1, 1, 1, 8, 5000000, 1)
-- end, nil)

require "style"
require "label"
require "cont"
require "img"
require "btn_matrix"
require "msg_box"
require "tb"
require "tab_view"
require "arc"
require "gauge"

-- 选择home或new_car文件，可看到不同效果
-- require "home"
require "new_car"

sys.init(0, 0)
sys.run()

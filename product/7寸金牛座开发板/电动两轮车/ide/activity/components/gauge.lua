module(..., package.seeall)
function create_bg_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)

    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))
    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.bg_opa or 255)

    lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color or 0xFFFFFF))

    lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.border_width or 1)
    lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.border_opa or 255)

    return s
end

function create_major_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)

    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0x00FFFF))
    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.bg_opa or 255)

    lvgl.style_set_text_font(s, lvgl.STATE_DEFAULT, v.font or style.font24)

    return s

end

function create(p, v)
    local g = lvgl.gauge_create(p, nil)

    lvgl.obj_set_size(g, v.R or 400, v.R or 400)

    -- 量规的范围可以通过 lv_gauge_set_range(gauge, min, max) 指定。默认范围是0..100。
    lvgl.gauge_set_range(g, v.range_start or 0, v.range_end or 100)

    lvgl.obj_align(g, v.align_to, v.align or lvgl.ALIGN_IN_TOP_MID,
                   v.align_x or 0, v.align_y or 0)
    -- 量规可以显示多于一根针。使用 lv_gauge_set_needle_count(gauge, needle_num, color_array) 
    -- 函数设置针数和每根针具有颜色的数组。数组必须是静态或全局变量，因为仅存储其指针。
    lvgl.gauge_set_needle_count(g, 1, lvgl.color_hex(v.needle_color or 0x000000))

    -- 要设置临界值，请使用 lv_gauge_set_critical_value(gauge, value)。
    -- 此值之后，比例尺颜色将更改为scale_end_color。默认临界值为80。
    lvgl.gauge_set_critical_value(g, v.critical_value or 90)

    -- 可以使用 lv_gauge_set_value(gauge, needle_id, value) 来设置针的值。
    lvgl.gauge_set_value(g, 0, 30)

    -- 可以使用 lv_gauge_set_scale(gauge, angle, line_num, label_cnt) 
    -- 函数来调整刻度角度以及刻度线和标签的数量。默认设置为220度，6个比例标签和21条线。
    lvgl.gauge_set_scale(g, 320, 51, 11)

    -- 量表的刻度可以偏移。可以通过 lv_gauge_set_angle_offset(gauge, angle) 进行调整。
    lvgl.gauge_set_angle_offset(g, 0)

    lvgl.obj_add_style(g, lvgl.GAUGE_PART_MAIN, create_bg_style(v.style.bg))

    lvgl.obj_add_style(g, lvgl.GAUGE_PART_MAJOR,
                       create_major_style(v.style.major))

    return g

end

module(..., package.seeall)

font96 = lvgl.font_load(spi.SPI_1, 96, 1)
font16 = lvgl.font_load(spi.SPI_1, 16, 2)
font24 = lvgl.font_load(spi.SPI_1, 24, 2)
font46 = lvgl.font_load(spi.SPI_1, 46, 2)
font48 = lvgl.font_load(spi.SPI_1, 48, 2)
font68 = lvgl.font_load(spi.SPI_1, 50, 2)

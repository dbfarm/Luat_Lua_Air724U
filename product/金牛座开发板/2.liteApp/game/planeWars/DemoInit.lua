---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

--LCD
require "mipi_lcd_GC9503"
--触摸屏
require "tp"
require "me"
require "enemy"
require "gameUtil"

_G.HEIGHT=854

_G.WIDTH = 480


function rootInit()
	backImg1 = lvgl.img_create(lvgl.scr_act(), nil)
	-- 设置图片显示的图像
	lvgl.img_set_src(backImg1, "/lua/bg.png")
	-- 图片居中
	lvgl.obj_align(backImg1, nil, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)

	backImg2 = lvgl.img_create(lvgl.scr_act(), nil)
	-- 设置图片显示的图像
	lvgl.img_set_src(backImg2, "/lua/bg.png")
	-- 图片居中
	lvgl.obj_align(backImg2, nil, lvgl.ALIGN_IN_TOP_LEFT, 0, -HEIGHT)

	_G.label = lvgl.label_create(lvgl.scr_act(), nil)      
	lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_LEFT, 10, 10)           
	lvgl.label_set_text(label, "当前分数：".."\n最高分数：")
	
	_G.god = lvgl.label_create(lvgl.scr_act(), nil)      
	lvgl.obj_align(god, nil, lvgl.ALIGN_IN_TOP_MID, 0, 10)   
	lvgl.label_set_text(god, "")   


	-- test = lvgl.img_create(lvgl.scr_act(), nil)
	-- -- 设置图片显示的图像
	-- lvgl.img_set_src(test, "/lua/me.bin")
	-- lvgl.obj_align(test, backImg, lvgl.ALIGN_CENTER, 0, 0)
end

--------------------------------------

lvgl.init(rootInit, tp.input)
-- lvgl.init(rootInit, nil)

sys.taskInit(function()	
	sys.taskInit(function()
		local y=0
        while true do
            lvgl.obj_set_pos(backImg1,0,y)
            lvgl.obj_set_pos(backImg2,0,y-HEIGHT)
            y=y+5
            y=y%(HEIGHT+1)
            sys.wait(40)
        end
	end)

	sys.wait(200)
	me.init()
	me.touchMove()
    enemy.init()
    me.shoot()
end)
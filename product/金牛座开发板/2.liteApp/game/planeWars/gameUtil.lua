module(...,package.seeall)



local function PitInObj(x1,y1,obj)
	x2=	lvgl.obj_get_x(obj)
	y2=lvgl.obj_get_y(obj)
	local width=lvgl.obj_get_width_fit(obj)
	local height=lvgl.obj_get_height_fit(obj)
	if x1<x2 or x1>(x2+width) then return false end
	if y1<y2 or y1>(y2+height) then return false end
	return true
end

local function ObjCovObj(obj1,obj2)
	local xt={lvgl.obj_get_x(obj1),lvgl.obj_get_x(obj1)+lvgl.obj_get_width_fit(obj1)}
	local yt={lvgl.obj_get_y(obj1),lvgl.obj_get_y(obj1)+lvgl.obj_get_height_fit(obj1)}
	for k, xv in ipairs(xt) do
		for index, yv in ipairs(yt) do
			local c=PitInObj(xv,yv,obj2)
			if c then return true end
		end
	end

	return false
end

_G.G_util={
	coverTest=ObjCovObj
}
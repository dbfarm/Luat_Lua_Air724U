---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

-- require "PA"

-- Point=PA.path

_G.enemyTable={}
enemy1={
    NOMAL={"e1.bin"},
    BREAK={"e1bk1.bin",
        "e1bk2.bin",
        "e1bk3.bin",
        "e1bk4.bin",
    }
}

enemy2={
    NOMAL={"e2.bin"},
    BREAK={"e2bk1.bin",
        "e2bk2.bin",
        "e2bk3.bin",
        "e2bk4.bin",
    }
}

enemyMap={
	e1=enemy1,
	e2=enemy2,
}

math.randomseed(os.time() or 336526)

function breakEnemy(e,life)
    sys.taskInit(function()
		local width=lvgl.obj_get_width_fit(e)
		local t
        local score
		if width==57 then
			t=enemyMap.e1
            score=20
		elseif width==69 then
			t=enemyMap.e2
            score=50
		end
        if life<=0 then 
            status.score=status.score+score
            if status.score>status.scoreMax then status.scoreMax=status.score end
            lvgl.label_set_text(label,"当前分数："..status.score.."\n最高分数："..status.scoreMax)
        end
        for k, v in ipairs(t.BREAK) do
            lvgl.img_set_src(e, "/lua/"..v)
            sys.wait(100)
        end
        lvgl.obj_del(e)
    end)
end

function creatEnemy()
    if ProjectSwitch.ProjectSwitchBox then return end
    if #enemyTable>=5 then return end
    -- print("creat")
    local obj
	local life
    local index=math.random(0,WIDTH)
    local PR=math.random(1,100)
    local down
    local bb
    if PR<30 then
        obj=enemyMap.e2
        life=3
        bb=math.random(1,3)
        down=math.random(5,15)
    else
        obj=enemyMap.e1
        life=1
        bb=1
        down=math.random(15,30)
    end
    local D = lvgl.img_create(lvgl.scr_act(), nil)
    lvgl.img_set_src(D, "/lua/"..obj.NOMAL[1])
    lvgl.obj_set_pos(D,index,-lvgl.obj_get_width_fit(D))
    local et={life,D,index,down,bb}
    table.insert(enemyTable,et)
end


----------
function enemyShot(e)
    sys.taskInit(function(e)
        local bb=lvgl.img_create(lvgl.scr_act(), nil)
        local t1=math.random(-10,-3)
        local t2=math.random(3,10)
        local stepx=math.abs(t1)>math.abs(t2) and t1 or t2
        local stepy=math.random(2,10)
        lvgl.img_set_src(bb,"/lua/ebb.bin" )
        lvgl.obj_set_pos(bb, lvgl.obj_get_x(e)+(lvgl.obj_get_width_fit(e)/2),lvgl.obj_get_y(e)+lvgl.obj_get_height_fit(e))
        while lvgl.obj_get_y(bb)<HEIGHT and lvgl.obj_get_x(bb)<WIDTH and lvgl.obj_get_x(bb)>0 do
            if not ProjectSwitch.ProjectSwitchBox then 
                lvgl.obj_set_pos(bb, lvgl.obj_get_x(bb)+stepx,lvgl.obj_get_y(bb)+stepy)
                if status.life>0 then 
                    -- local kpx=lvgl.obj_get_x(bb)+(lvgl.obj_get_width_fit(bb)/2)
                    -- local kpy=lvgl.obj_get_y(bb)+lvgl.obj_get_height_fit(bb)
                    local c=G_util.coverTest(bb,plane)
                    if c then status.less() end
                end
            end
                sys.wait(20)
    
        end
        lvgl.obj_del(bb)
        end,e)
end
----------------------------------------

function enemyTask()
    if ProjectSwitch.ProjectSwitchBox then return end
    for k, v in ipairs(enemyTable) do
        local e=v[2]
        local y=lvgl.obj_get_y(e)
        local life=v[1]
        local down=v[4]
        if not (life>0 and ((y+down)<=HEIGHT)) then
            breakEnemy(e,life)
            table.remove(enemyTable,k)
        end
    end
    for k, v in ipairs(enemyTable) do
        local down=v[4]
        local e=v[2]
		local index=v[3]
		local x=lvgl.obj_get_x(e)
        local y=lvgl.obj_get_y(e)+down
		-- local step=0

		if x==index then v[3]=math.random(0,WIDTH) end
		x= x>v[3] and x-1 or x+1

        lvgl.obj_set_pos(e,x,y)

        if v[5]>0 and y>HEIGHT/4 then 
            for i = 1, v[5] do
                enemyShot(e)
            end
            v[5]=0
        end
		if status.life>0 then 
			-- local kpx=lvgl.obj_get_x(e)+(lvgl.obj_get_width_fit(e)/2)
			-- local kpy=lvgl.obj_get_y(e)+lvgl.obj_get_height_fit(e)
			local c=G_util.coverTest(e,plane)
			if c then status.less() end
		end

    end
end


function init()
    enemyT1=sys.timerLoopStart(enemyTask,50)
    enemyT2=sys.timerLoopStart(creatEnemy,500)
end
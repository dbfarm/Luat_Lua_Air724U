---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

--LCD
require "mipi_lcd_GC9503"
--触摸屏
require "tp"

local data = {type = lvgl.INDEV_TYPE_POINTER}
local function input()
	if lvgl.indev_get_emu_touch then
        return lvgl.indev_get_emu_touch()
    end
	pmd.sleep(100)
	local ret,ispress,px,py = tp.get()
	if ret then
		if lastispress == ispress and lastpx == px and lastpy == py then
			return data
		end
		lastispress = ispress
		lastpx = px
		lastpy = py
		if ispress then
			tpstate = lvgl.INDEV_STATE_PR
		else
			tpstate = lvgl.INDEV_STATE_REL
		end
	else
		return data
	end

	local topoint = {x = px,y = py}
	data.state = tpstate
	data.point = topoint

	return data
end

function demo_SwitchInit()
	function event_handler(obj, event)
		if event == lvgl.EVENT_VALUE_CHANGED then
			print("State", lvgl.obj_get_id(sw1),lvgl.obj_get_id(sw2),lvgl.obj_get_id(obj), lvgl.switch_get_state(obj))
		end
	end
	
	sw1 = lvgl.switch_create(lvgl.scr_act(), nil)
	lvgl.obj_align(sw1, nil, lvgl.ALIGN_CENTER, 0, -50)
	lvgl.obj_set_event_cb(sw1, event_handler)
	
	sw2 = lvgl.switch_create(lvgl.scr_act(), sw1)
	lvgl.switch_on(sw2, lvgl.ANIM_ON)
	lvgl.obj_align(sw2, nil, lvgl.ALIGN_CENTER, 0, 50)
	lvgl.obj_set_size(sw2,160,48)
	lvgl.obj_set_event_cb(sw2, event_handler)
end


local function init()
	lvgl.init(demo_SwitchInit, input)
	lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)
	pmd.ldoset(8,pmd.LDO_VIBR)
end

--sys.taskInit(init, nil)
init()
---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

--LCD
require "mipi_lcd_GC9503"
--触摸屏
require "tp"

local data = {type = lvgl.INDEV_TYPE_POINTER}
local function input()
	pmd.sleep(100)
	local ret,ispress,px,py = tp.get()
	if ret then
		if lastispress == ispress and lastpx == px and lastpy == py then
			return data
		end
		lastispress = ispress
		lastpx = px
		lastpy = py
		if ispress then
			tpstate = lvgl.INDEV_STATE_PR
		else
			tpstate = lvgl.INDEV_STATE_REL
		end
	else
		return data
	end

	local topoint = {x = px,y = py}
	data.state = tpstate
	data.point = topoint

	return data
end

function spinbox_increment_event_cb(obj, event)
    if event == lvgl.EVENT_SHORT_CLICKED then
        lvgl.spinbox_increment(spinbox)
    end
end

function spinbox_decrement_event_cb(obj, event)
    if event == lvgl.EVENT_SHORT_CLICKED then
        lvgl.spinbox_decrement(spinbox)
    end
end

-- 创建按钮
function cBt(cont, txt, cb)
    local btn = lvgl.btn_create(cont, nil)
    lvgl.obj_set_event_cb(btn, cb) 
    local label = lvgl.label_create(btn, nil)
    lvgl.label_set_text(label, txt)
end


function demo_SpinboxInit()
-- 容器
cont = lvgl.cont_create(lvgl.scr_act(), nil)
lvgl.disp_load_scr(cont)
lvgl.cont_set_fit(cont, lvgl.FIT_TIGHT)
lvgl.obj_align(cont, nil, lvgl.ALIGN_CENTER, 0, 0) 
lvgl.cont_set_layout(cont, lvgl.LAYOUT_CENTER)

-- 按钮一
cBt(cont, "plus", spinbox_increment_event_cb)

-- 微调框
spinbox = lvgl.spinbox_create(cont, nil)
lvgl.spinbox_set_range(spinbox, -1000, 25000)
lvgl.spinbox_set_digit_format(spinbox, 5, 2)
lvgl.spinbox_step_prev(spinbox)

-- 按钮二
cBt(cont, "minus", spinbox_decrement_event_cb)
end


local function init()
	lvgl.init(demo_SpinboxInit, input)
	pmd.ldoset(8,pmd.LDO_VIBR)
end

sys.taskInit(init, nil)
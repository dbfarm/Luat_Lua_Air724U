module(..., package.seeall)

-- 中文输入法控件demo
-- 整体功能与用法和基础的输入法控件类似，以下列出有差异的函数名。
-- chs_keyboard_create
-- chs_keyboard_set_mode：lvgl.CHSKB_MODE_QWERTY
-- chs_keyboard_set_textarea


local function lvgl_UiDesigner_DefOutCb(o, e, output)
    if e == lvgl.EVENT_CLICKED then
        lvgl.obj_set_hidden(output, false)
        lvgl.chs_keyboard_set_textarea(output, o)
    elseif e == lvgl.EVENT_DEFOCUSED then
        -- lvgl.obj_set_hidden(output, true)
    elseif e == lvgl.EVENT_VALUE_CHANGED then
        sys.publish("UI_EVENT_IND", o, e)
    end
end

local function lvgl_UiDesigner_DefInCb(o, e)
    lvgl.keyboard_def_event_cb(o, e)
    if e == lvgl.EVENT_CANCEL or e == lvgl.EVENT_APPLY then
        lvgl.obj_set_hidden(o, true)
    end
end

DefaultContStyle = lvgl.style_t()
lvgl.style_init(DefaultContStyle)
lvgl.style_set_radius(DefaultContStyle, (lvgl.STATE_DEFAULT or LV_STATE_FOCUSED or LV_STATE_PRESSED), 0)
lvgl.style_set_border_opa(DefaultContStyle, (lvgl.STATE_DEFAULT or LV_STATE_FOCUSED or LV_STATE_PRESSED), 0)

function create()
    local parent_cont = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(parent_cont, 854, 480)
    lvgl.obj_align(parent_cont, nil, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
    lvgl.obj_add_style(parent_cont, lvgl.CONT_PART_MAIN, DefaultContStyle)

    -- 输入框背景样式
    Style_LvglTextarea1_1 = lvgl.style_t()
    lvgl.style_init(Style_LvglTextarea1_1)

    -- 输入框游标样式
    Style_LvglTextarea1_2 = lvgl.style_t()
    lvgl.style_init(Style_LvglTextarea1_2)

    -- 输入框创建
    local text_area_obj = lvgl.textarea_create(parent_cont, nil)
    lvgl.obj_set_size(text_area_obj, 280, 58)
    lvgl.obj_set_click(text_area_obj, true)
    lvgl.textarea_set_text(text_area_obj, "")
    lvgl.textarea_set_placeholder_text(text_area_obj, "请输入内容")
    lvgl.textarea_set_scrollbar_mode(text_area_obj, lvgl.SCROLLBAR_MODE_OFF)
    lvgl.obj_align(text_area_obj, parent_cont, lvgl.ALIGN_IN_TOP_LEFT, 300, 55)
    lvgl.obj_add_style(text_area_obj, lvgl.TEXTAREA_PART_BG, Style_LvglTextarea1_1)
    lvgl.obj_add_style(text_area_obj, lvgl.TEXTAREA_PART_CURSOR, Style_LvglTextarea1_2)

    -- 背景样式
    Style_LvglKeyboard1_1 = lvgl.style_t()
    lvgl.style_init(Style_LvglKeyboard1_1)

    -- 按键部分样式
    Style_LvglKeyboard1_2 = lvgl.style_t()
    lvgl.style_init(Style_LvglKeyboard1_2)

    -- 创建中文输入法控件对象
    local chs_keyboard_obj = lvgl.chs_keyboard_create(parent_cont, nil)
    lvgl.obj_set_size(chs_keyboard_obj, 620, 300)
    lvgl.chs_keyboard_set_mode(chs_keyboard_obj, lvgl.CHSKB_MODE_QWERTY)
    lvgl.obj_set_click(chs_keyboard_obj, true)
    -- 默认隐藏中文输入法控件，点击输入框显示中文输入法控件
    lvgl.obj_set_hidden(chs_keyboard_obj, true)
    -- 与输入框绑定
    lvgl.chs_keyboard_set_textarea(chs_keyboard_obj, text_area_obj)
    local outputCb = function(o, e)
        lvgl_UiDesigner_DefOutCb(o, e, chs_keyboard_obj)
    end
    lvgl.obj_set_event_cb(text_area_obj, outputCb)
    lvgl.obj_set_event_cb(chs_keyboard_obj, lvgl_UiDesigner_DefInCb)
    lvgl.obj_align(chs_keyboard_obj, parent_cont, lvgl.ALIGN_IN_TOP_LEFT, 140, 140)
    lvgl.obj_add_style(chs_keyboard_obj, lvgl.KEYBOARD_PART_BG, Style_LvglKeyboard1_1)
    lvgl.obj_add_style(chs_keyboard_obj, lvgl.KEYBOARD_PART_BTN, Style_LvglKeyboard1_2)

end

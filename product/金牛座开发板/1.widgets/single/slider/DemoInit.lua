---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

--LCD
require "mipi_lcd_GC9503"
--触摸屏
require "tp"

local data = {type = lvgl.INDEV_TYPE_POINTER}
local function input()
	pmd.sleep(100)
	local ret,ispress,px,py = tp.get()
	if ret then
		if lastispress == ispress and lastpx == px and lastpy == py then
			return data
		end
		lastispress = ispress
		lastpx = px
		lastpy = py
		if ispress then
			tpstate = lvgl.INDEV_STATE_PR
		else
			tpstate = lvgl.INDEV_STATE_REL
		end
	else
		return data
	end

	local topoint = {x = px,y = py}
	data.state = tpstate
	data.point = topoint

	return data
end

function demo_SliderInit()
	--创建一个 Slider
	demo_Slider = lvgl.slider_create(lvgl.scr_act(), nil)
	--设置 Slider 的显示大小
	lvgl.obj_set_size(demo_Slider, 400, 60)
	--设置 Slider 的取值范围
	lvgl.slider_set_range(demo_Slider, 0, 100)
	--设置 Slider 的动画时间
	lvgl.slider_set_anim_time(demo_Slider, 3000)
	--设置 Slider 的值和是否开启动画
	lvgl.slider_set_value(demo_Slider, 100, lvgl.ANIM_ON)
	--设置 Slider 的位置
	lvgl.obj_align(demo_Slider, lvgl.scr_act(), lvgl.ALIGN_CENTER, 0, 0)
end


local function init()
	lvgl.init(demo_SliderInit, input)
	pmd.ldoset(8,pmd.LDO_VIBR)
end

sys.taskInit(init, nil)
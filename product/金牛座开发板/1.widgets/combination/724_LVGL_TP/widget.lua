module(..., package.seeall)

require "tp"

local LCD_W =128
local LCD_H = 160
local contentBox

--主容器
function demo_cont(p, v)
    c = lvgl.cont_create(p, nil)
    lvgl.obj_set_click(c, v.click or true)
    lvgl.obj_set_size(c, v.W, v.H)
    -- lvgl.obj_set_auto_realign(Titlecont, true)                   
    lvgl.obj_align(c, v.align_to or nil, v.align or lvgl.ALIGN_IN_TOP_MID,
                   v.align_x or 0, v.align_y or 0)
    lvgl.obj_add_style(c, lvgl.CONT_PART_MAIN, create_style(v.style))
    return c
end

--容器风格
function create_style(v)
    local s = lvgl.style_t()

    lvgl.style_init(s)

    lvgl.style_set_radius(s, lvgl.STATE_DEFAULT, v.radius or 0)
    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))

    lvgl.style_set_bg_grad_color(s, lvgl.STATE_DEFAULT,
                                 lvgl.color_hex(v.bg_grad_color or 0xFFFFFF))

    lvgl.style_set_bg_grad_dir(s, lvgl.STATE_DEFAULT,
                               v.bg_grad_dir or lvgl.GRAD_DIR_VER)

    lvgl.style_set_radius(s, lvgl.STATE_DEFAULT, v.radius or 0)

    lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color or 0xFFFFFF))

    lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.border_width or 0)
    lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.border_opa or 255)

    -- 透明度
    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.bg_opa or 255)

    -- 内边距
    lvgl.style_set_pad_left(s, lvgl.STATE_DEFAULT, v.pad_left or 0)
    lvgl.style_set_pad_right(s, lvgl.STATE_DEFAULT, v.pad_right or 0)
    lvgl.style_set_pad_top(s, lvgl.STATE_DEFAULT, v.pad_top or 0)
    lvgl.style_set_pad_bottom(s, lvgl.STATE_DEFAULT, v.pad_bottom or 0)
    lvgl.style_set_pad_inner(s, lvgl.STATE_DEFAULT, v.pad_inner or 0)

    -- 外边距
    lvgl.style_set_margin_left(s, lvgl.STATE_DEFAULT, v.margin_left or 0)
    lvgl.style_set_margin_right(s, lvgl.STATE_DEFAULT, v.margin_right or 0)
    lvgl.style_set_margin_top(s, lvgl.STATE_DEFAULT, v.margin_top or 0)
    lvgl.style_set_margin_bottom(s, lvgl.STATE_DEFAULT, v.margin_bottom or 0)
    return s
end

--尺寸判断
function size_judge(LCD_H,LCD_W,CONTROL)
    if LCD_H>LCD_W then
        lvgl.obj_set_size(CONTROL, LCD_W,LCD_H*(LCD_W/LCD_H))
    elseif LCD_H<LCD_W then
        lvgl.obj_set_size(CONTROL, LCD_W*(LCD_H/LCD_W),LCD_H)
    else
        lvgl.obj_set_size(CONTROL, LCD_W*0.7,LCD_H*0.7)
    end
end

-- 曲线
function demo_arc()
    if(not lvgl.arc_create) then 
        log.info("提示", "不支持 曲线 控件")
        return 
    end
    -- 创建曲线
    arc = lvgl.arc_create(contentBox, nil)
    -- 设置尺寸
    size_judge(LCD_H,LCD_W,arc)
    -- 设置位置居中
    lvgl.obj_align(arc, nil, lvgl.ALIGN_CENTER, 0, 0)
    -- 绘制弧度
    lvgl.arc_set_end_angle(arc, 200)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(arc)
end

-- 按键
function demo_btn()
    if(not lvgl.btn_create) then 
        log.info("提示", "不支持 按键 控件")
        return 
    end
    -- 按键回调函数
    event_btn = function(obj, event)
        if event == lvgl.EVENT_CLICKED then
            print("Clicked\n")
        elseif event == lvgl.EVENT_VALUE_CHANGED then
            print("Toggled\n")
        end
    end

    -- 按键1
    btn1 = lvgl.btn_create(contentBox, nil)
    lvgl.obj_set_event_cb(btn1, event_btn)
    lvgl.obj_align(btn1, nil, lvgl.ALIGN_CENTER, 0, -40)
    -- 按键1 的文字
    label = lvgl.label_create(btn1, nil)
    lvgl.label_set_text(label, "Button")
    -- 按键2
    btn2 = lvgl.btn_create(lvgl.scr_act(), nil)
    lvgl.obj_set_event_cb(btn2, event_btn)
    lvgl.obj_align(btn2, nil, lvgl.ALIGN_CENTER, 0, 40)
    lvgl.btn_set_checkable(btn2, true)
    lvgl.btn_toggle(btn2)
    lvgl.btn_set_fit2(btn2, lvgl.FIT_NONE, lvgl.FIT_TIGHT)
    -- 按键2 的文字
    label = lvgl.label_create(btn2, nil)
    lvgl.label_set_text(label, "Toggled")

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(btn1)
    lvgl.obj_del(btn2)
end

-- 进度条
function demo_bar()
    if(not lvgl.bar_create) then 
        log.info("提示", "不支持 进度条 控件")
        return 
    end
    -- 创建进度条
    bar = lvgl.bar_create(contentBox, nil)
    -- 设置尺寸
    lvgl.obj_set_size(bar, LCD_W*0.9,LCD_H*0.10)
    -- 设置位置居中
    lvgl.obj_align(bar, NULL, lvgl.ALIGN_CENTER, 0, 0)
    -- 设置加载完成时间
    lvgl.bar_set_anim_time(bar, 2000)
    -- 设置加载到的值
    lvgl.bar_set_value(bar, 100, lvgl.ANIM_ON)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(bar)
end

-- 日历
function demo_calendar()
    if(not lvgl.calendar_create) then 
        log.info("提示", "不支持 日历 控件")
        return 
    end
    -- 高亮显示的日期
    highlightDate = lvgl.calendar_date_t()

    -- 日历点击的回调函数
    -- 将点击日期设置高亮
    event_calendar = function(obj, event)
        if event == lvgl.EVENT_VALUE_CHANGED then
            date = lvgl.calendar_get_pressed_date(obj)
            if date then
                print(string.format("Clicked date: %02d.%02d.%d\n", date.day,
                                    date.month, date.year))
                highlightDate.year = date.year
                highlightDate.month = date.month
                highlightDate.day = date.day
                lvgl.calendar_set_highlighted_dates(obj, highlightDate, 1)
            end
        end
    end

    -- 创建日历
    calendar = lvgl.calendar_create(contentBox, nil)
    lvgl.obj_set_size(calendar, LCD_W, LCD_H)
    lvgl.obj_align(calendar, nil, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.obj_set_event_cb(calendar, event_calendar)

    -- 设置今天日期
    today = lvgl.calendar_date_t()
    today.year = 2018
    today.month = 10
    today.day = 23

    lvgl.calendar_set_today_date(calendar, today)
    lvgl.calendar_set_showed_date(calendar, today)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(calendar)
end

-- 复选框
function demo_cb()
    if(not lvgl.checkbox_create) then 
        log.info("提示", "不支持 复选框 控件")
        return 
    end
    -- 复选框回调函数
    event_cb = function(obj, event)
        if event == lvgl.EVENT_VALUE_CHANGED then
            print("State", lvgl.checkbox_is_checked(obj))
        end
    end

    -- 创建复选框
    cb = lvgl.checkbox_create(contentBox, nil)
    -- 设置标签
    lvgl.checkbox_set_text(cb, "hello")
    -- 设置居中位置
    lvgl.obj_align(cb, nil, lvgl.ALIGN_CENTER, 0, 0)
    -- 设置回调函数
    lvgl.obj_set_event_cb(cb, event_cb)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(cb)
end

-- 容器
function demo_cont_son()
    if(not lvgl.cont_create) then 
        log.info("提示", "不支持 容器 控件")
        return 
    end
    -- 创建容器
    cont = lvgl.cont_create(contentBox, nil)
    lvgl.obj_set_auto_realign(cont, true)
    lvgl.obj_align(cont, nil, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.cont_set_fit(cont, lvgl.FIT_TIGHT)
    lvgl.cont_set_layout(cont, lvgl.LAYOUT_COLUMN_MID)
    -- 添加标签
    label = lvgl.label_create(cont, nil)
    lvgl.label_set_text(label, "Short text")

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(label)
    lvgl.obj_del(cont)
end

-- 图表
function demo_chart()
    if(not lvgl.chart_create) then 
        log.info("提示", "不支持 图表 控件")
        return 
    end
    -- 创建图表
    chart = lvgl.chart_create(contentBox, nil)
    -- 设置尺寸
    size_judge(LCD_H,LCD_W,chart)
    lvgl.obj_align(chart, nil, lvgl.ALIGN_CENTER, 0, 0)

    -- 设置 Chart 的显示模式 (折线图)
    lvgl.chart_set_type(chart, lvgl.CHART_TYPE_LINE)

    ser1 = lvgl.chart_add_series(chart, lvgl.color_hex(0xFF0000))
    ser2 = lvgl.chart_add_series(chart, lvgl.color_hex(0x008000))

    -- 添加点
    for i = 0, 15 do lvgl.chart_set_next(chart, ser1, i * 10) end
    for i = 15, 0, -1 do lvgl.chart_set_next(chart, ser2, i * 10) end

    -- 刷新图表
    lvgl.chart_refresh(chart)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(chart)
end

-- 下拉框
function demo_dd()
    if(not lvgl.scr_act) then 
        log.info("提示", "不支持 下拉框 控件")
        return 
    end
    event_dd = function(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            print("Option:", lvgl.dropdown_get_symbol(obj))
        end
    end

    -- 创建下拉框
    dd = lvgl.dropdown_create(contentBox, nil)
    lvgl.dropdown_set_options(dd, 
[[Apple
Banana
Orange
Cherry
Grape
Raspberry
Melon
Orange
Lemon
Nuts]])
    -- 设置对齐
    lvgl.obj_align(dd, nil, lvgl.ALIGN_IN_TOP_MID, 0, 20)
    lvgl.obj_set_event_cb(dd, event_dd)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(dd)
end

-- 输入框
function demo_textarea()
    if(not lvgl.textarea_create) then 
        log.info("提示", "不支持 输入框 控件")
        return 
    end
    -- 按键回调
    event_key = function(obj, e)
        -- 默认处理事件
        lvgl.keyboard_def_event_cb(keyBoard, e)
        if (e == lvgl.EVENT_CANCEL) then
            lvgl.keyboard_set_textarea(keyBoard, nil)
            -- 删除 KeyBoard
            lvgl.obj_del(keyBoard)
            keyBoard = nil
        end
    end

    -- 输入框回调
    event_txt = function(obj, e)
        if (e == lvgl.EVENT_CLICKED) and not keyBoard then
            -- 创建一个 KeyBoard
            keyBoard = lvgl.keyboard_create(lvgl.scr_act(), nil)
            -- 设置 KeyBoard 的光标是否显示
            lvgl.keyboard_set_cursor_manage(keyBoard, true)
            -- 为 KeyBoard 设置一个文本区域
            lvgl.keyboard_set_textarea(keyBoard, textarea)
            lvgl.obj_set_event_cb(keyBoard, event_key)
        end
    end

    textarea = lvgl.textarea_create(contentBox, nil)
    lvgl.obj_set_size(textarea, 100, 25)
    lvgl.textarea_set_text(textarea, "please input:")
    lvgl.obj_align(textarea, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, -45)
    lvgl.obj_set_event_cb(textarea, event_txt)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(textarea)
end

-- 标签
function demo_label()
    if(not lvgl.label_create) then 
        log.info("提示", "不支持 标签 控件")
        return 
    end
    -- 创建标签
    label = lvgl.label_create(contentBox, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,
    "#0000ff color# #ff00ff words# #ff0000 of\n# align the lines \n the center and\n automatically.")
    lvgl.label_set_align(label, lvgl.LABEL_ALIGN_CENTER)
    lvgl.obj_align(label, nil, lvgl.ALIGN_CENTER, 0, 0)
    sys.wait(2000)
    -- 清除对象
    lvgl.obj_del(label)
end

-- 加载器
function demo_spinner()
    if(not lvgl.spinner_create) then 
        log.info("提示", "不支持 加载器 控件")
        return 
    end

    -- 创建加载器
    spinner = lvgl.spinner_create(contentBox, nil)
    size_judge(LCD_H,LCD_W,spinner)
    lvgl.obj_align(spinner, nil, lvgl.ALIGN_CENTER, 0, 0)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(spinner)
end

-- 开关
function demo_sw()
    if(not lvgl.switch_create) then 
        log.info("提示", "不支持 开关 控件")
        return 
    end
    -- 开关回调
    event_sw = function(obj, event)
        if event == lvgl.EVENT_VALUE_CHANGED then
            print("State", lvgl.switch_get_state(obj))
        end
    end
    -- 创建开关
    sw1 = lvgl.switch_create(contentBox, nil)
    lvgl.obj_align(sw1, nil, lvgl.ALIGN_CENTER, 0, -50)
    lvgl.obj_set_event_cb(sw1, event_sw)
    sw2 = lvgl.switch_create(contentBox, sw1)
    lvgl.switch_on(sw2, lvgl.ANIM_ON)
    lvgl.obj_align(sw2, nil, lvgl.ALIGN_CENTER, 0, 50)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(sw1)
    lvgl.obj_del(sw2)
end

-- 仪表盘
function demo_gauge()
    if(not lvgl.gauge_create) then 
        log.info("提示", "不支持 仪表盘 控件")
        return 
    end
    -- 创建一个颜色
    green = lvgl.color_make(0, 255, 0)
    -- 创建一个 Gauge
    gauge = lvgl.gauge_create(contentBox, nil)
    -- 设置 gauge 的表盘指针数
    lvgl.gauge_set_needle_count(gauge, 1, green)
    -- 设置表针的值
    lvgl.gauge_set_value(gauge, 0, 30)
    -- 设置 Gauge 的大小
    -- size_judge(LCD_H,LCD_W,gauge)
    lvgl.obj_set_size(gauge, 160, 160)
    -- lvgl.gauge_set_scale(gauge,300,8,6)
    -- 设置 Gauge 的位置
    lvgl.obj_align(gauge, nil, lvgl.ALIGN_CENTER, 0, 0)
    -- 设置 Gauge 里的刻度的最小最大值
    lvgl.gauge_set_range(gauge, 0, 200)
    -- 设置 Gauge 里的填充的面积，360为不填充
    lvgl.gauge_set_critical_value(gauge, 360)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(gauge)
end

-- 表格
function demo_tb()
    if(not lvgl.table_create) then 
        log.info("提示", "不支持 表格 控件")
        return 
    end
    -- 创建一个 Table
    tb = lvgl.table_create(lvgl.scr_act(), nil)
    -- 设置 Table 的行数
    lvgl.table_set_row_cnt(tb, 3)
    -- 设置 Table 的列数
    lvgl.table_set_col_cnt(tb, 2)

    -- 设置 Table 的内容
    lvgl.table_set_cell_value(tb, 0, 0, "阵营")
    lvgl.table_set_cell_value(tb, 1, 0, "博派")
    lvgl.table_set_cell_value(tb, 2, 0, "狂派")

    lvgl.table_set_cell_value(tb, 0, 1, "首领")
    lvgl.table_set_cell_value(tb, 1, 1, "擎天柱")
    lvgl.table_set_cell_value(tb, 2, 1, "威震天")

    -- 设置 Table 的显示大小
    size_judge(LCD_H,LCD_W,tb)
    -- 设置 Table 的位置
    lvgl.obj_align(tb, nil, lvgl.ALIGN_CENTER, 0, 0)

    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(tb)
end

--图片
function demo_img()
    if (not lvgl.img_create ) then
        log.info("提示","不支持 图片 控件")
    end

    img = lvgl.img_create(contentBox, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(img, "/lua/Luat1.png")
    -- 图片居中
    lvgl.obj_align(img, nil, lvgl.ALIGN_CENTER, 0, 0)
    sys.wait(3000)
    -- 清除对象
    lvgl.obj_del(img)
end

lvgl.init(function()
end,tp.input)

sys.taskInit(function()
    sys.wait(3000)
    contentBox = demo_cont(lvgl.scr_act(), {
        align = lvgl.ALIGN_CENTER,
        W = LCD_W,
        H = LCD_H,
        style = {bg_opa = 0}
    })
    while true do
        -- 曲线
        demo_arc()

        -- 按键
        demo_btn()

        -- 进度条
        demo_bar()

        -- 日历
        demo_calendar()

        -- 复选框
        demo_cb()

        -- 容器
        demo_cont_son()

        -- 图表
        demo_chart()

        -- 下拉框
        demo_dd()

        -- 文本输入框
        demo_textarea()

        -- 标签
        demo_label()

        -- 加载器
        demo_spinner()

        -- 开关
        demo_sw()

        -- 仪表盘
        demo_gauge()

        -- 表格
        demo_tb()

        -- 图片
        demo_img()
    end
end)
module(..., package.seeall)

function create(p, v)
    local obj = lvgl.dropdown_create(p, nil)
    lvgl.obj_set_size(obj, v.W or 100, v.H or 220)
    if v.click then lvgl.obj_set_click(obj, true) end
    -- 选项作为带有 lvgl.dropdown_set_options(dropdown, options) 的字符串传递到下拉列表。
    -- 选项应用 \n 分隔。例如： "First\nSecond\nThird" 。
    -- 该字符串将保存在下拉列表中，因此也可以保存在本地变量中。
    if v.options then lvgl.dropdown_set_options(obj, v.options) end

    -- 函数向 pos 索引插入一个新选项。
    -- 如果 pos 为 nil，则插入到末尾。
    -- 如果 pos 为 0，则插入到开头。
    lvgl.dropdown_add_option(obj, v.option, v.pos or -1)
    -- 为了节省内存，还可以使用 lvgl.dropdown_set_static_options(dropdown, options) 
    -- 从静态（常量）字符串设置选项。在这种情况下，当存在下拉列表且不能使用
    -- lvgl.dropdown_add_option 时，options字符串应处于活动状态

    -- 可以使用 lvgl.dropdown_set_selected(dropdown, id) 手动选择一个选项，其中id是选项的索引。
    -- 如果 id 为 nil，则选择第一个选项。
    if v.id then lvgl.dropdown_set_selected(obj, v.id or 0) end

    -- 该列表可以在任何一侧创建。默认值 lvgl.DROPDOWN_DOWN 可以通过功能进行修改。
    -- lvgl.dropdown_set_dir(dropdown, lvgl.DROPDOWN_DIR_LEFT/RIGHT/UP/DOWN)
    -- 如果列表垂直于屏幕之外，它将与边缘对齐。
    lvgl.dropdown_set_dir(obj, lvgl.DROPDOWN_DIR_LEFT)

    -- 可以通过 lvgl.dropdown_set_max_height(dropdown, height) 
    -- 设置下拉列表的最大高度。默认情况下，它设置为3/4垂直分辨率。
    lvgl.dropdown_set_max_height(obj, 200)

    -- 主要部分可以显示所选选项或静态文本。可以使用 lvgl.dropdown_set_show_selected(sropdown, true/false) 进行控制。
    -- 可以使用 lvgl.dropdown_set_text(dropdown, "Text") 设置静态文本。仅保存文本指针。
    -- 如果也不想突出显示所选选项，则可以将自定义透明样式用于 lvgl.DROPDOWN_PART_SELECTED 。

    -- 默认为true即显示所选选项，否则为静态文本。
    lvgl.dropdown_set_show_selected(obj, false)
    -- 只有dropdown_set_show_selected设置为false才能设置静态文本
    lvgl.dropdown_set_text(obj, "静态文本")

    -- 可以使用 lvgl.dropdown_set_symbol(dropdown, lvgl.SYMBOL_...) 将符号（通常是箭头）添加到下拉列表中
    -- 如果下拉列表的方向为 lvgl.DROPDOWN_DIR_LEFT ，则该符号将显示在左侧，否则显示在右侧。
    -- 选项旁的符号
    lvgl.dropdown_set_symbol(obj, "v")

    -- 不支持
    -- 下拉列表的打开/关闭动画时间由 lvgl.dropdown_set_anim_time(ddlist, anim_time) 调整。动画时间为零表示没有动画。
    -- lvgl.dropdown_set_anim_time(obj, 500)

    -- 调用下拉列表的主要部分， lvgl.DROPDOWN_PART_MAIN 它是一个简单的 lvgl.obj 对象。
    -- 它使用所有典型的背景属性。按下，聚焦，编辑等阶梯也照常应用。
    -- 单击主对象时创建的列表是Page。它的背景部分可以被引用， lvgl.DROPDOWN_PART_LIST 
    -- 并为矩形本身使用所有典型的背景属性，并为选项使用文本属性。要调整选项之间的间距，
    -- 请使用text_line_space样式属性。填充值可用于在边缘上留出一些空间。
    -- 页面的可滚动部分被隐藏，其样式始终为空（透明，无填充）。
    -- 滚动条可以被引用 lvgl.DROPDOWN_PART_SCROLLBAR 并使用所有典型的背景属性。
    -- 可以 lvgl.DROPDOWN_PART_SELECTED 使用所有典型的背景属性引用并使用所选的选项。
    -- 它将以其默认状态在所选选项上绘制一个矩形，并在按下状态下在被按下的选项上绘制一个矩形。

    if v.style then
        lvgl.obj_add_style(obj, lvgl.DROPDOWN_PART_MAIN,
                           create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.DROPDOWN_PART_LIST,
                           create_style(v.style.list))
        lvgl.obj_add_style(obj, lvgl.DROPDOWN_PART_SCROLLBAR,
                           create_style(v.style.scrlbar))
        lvgl.obj_add_style(obj, lvgl.DROPDOWN_PART_SELECTED,
                           create_style(v.style.selected))
    end

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    -- 要手动打开或关闭下拉列表，可以使用 lvgl.dropdown_open/close(dropdown, lvgl.ANIM_ON/OFF) 功能。
    -- 动画参数可不设置
    lvgl.dropdown_open(obj, lvgl.ANIM_ON)
    -- lvgl.dropdown_close(obj, lvgl.ANIM_ON)
    return obj

end

local dropdown_data = {
    W = 200,
    H = 100,
    options = "Apple\nBanana\nOrange\nMelon\nGrape\nRaspberry",
    option = "新插入的选项",
    id = 3,
    style = {
        bg = {
            bg = {radius = 10, color = 0xff0000, opa = 150},
            text = {font = style.font48}
        },
        list = {
            bg = {radius = 10, color = 0x00ff00, opa = 150},
            text = {font = style.font48}
        },
        scrlbar = {bg = {radius = 10, color = 0x0000ff, opa = 150}},
        selected = {bg = {radius = 10, color = 0xffcccc, opa = 150}}
    }
}
-- create(lvgl.scr_act(), dropdown_data)


--- 模块功能：USB AT 口收发数据功能测试
-- @author openLuat
-- @module update.testUpdate1
-- @license MIT
-- @copyright openLuat
-- @release 2019.05.9

module(...,package.seeall)

local UART_ID = 1  --uart.USB

--[[
函数名：usbreader
功能  ：向USB AT 口发送数据
参数  ：无
返回值：无
]]
local function usbwrite(s)
    log.info("usb send",s)
    uart.write(UART_ID, s)
end

--[[
函数名：usbreader
功能  ：从USB AT 口接收数据
参数  ：无
返回值：无
]]
local function usbreader()
    local s

    --循环读取收到的数据
    while true do
        --每次读取一行
        s = uart.read(UART_ID, "*l", 0)
        if string.len(s) ~= 0 then
                local svcData = string.toHex(s)
                log.info("usb rcv",svcData,string.toHex(s));
                usbwrite("OK\n")
                --local svcData = string.find(s, "+PARAM")
                if svcData then
                    --svcData = string.match(s, "%+PARAM=(.+)")
                    --svcData = string.sub(svcData,1,-3)
                    svcDataT = string.split(svcData, '0D')
                    print("svcDataT",svcDataT[1],svcDataT[2],svcDataT[3],svcDataT[4],svcDataT[5],svcDataT[6],svcDataT[7],svcDataT[8],svcDataT[9])
                    local Vol,Cur,CurNow,Temp,AcPower,RePower,Switch,AcEnergy,ReEnergy = tonumber(svcDataT[1])/10,tonumber(svcDataT[2])/10,tonumber(svcDataT[3])/10,tonumber(svcDataT[4])/10,tonumber(svcDataT[5])/10,tonumber(svcDataT[6])/10,tonumber(svcDataT[7])/10,tonumber(svcDataT[8])/10,tonumber(svcDataT[9])/10
                    print("Vol",Vol,Cur,CurNow,Temp,AcPower,RePower,Switch,AcEnergy,ReEnergy)

                    if Switch == "1" then
                       Switch = "合"
                    else
                       Switch = "开"
                    end
                    sys.publish("SIMULATION_RPT_RESULT",Vol,Cur,CurNow,Temp,AcPower,RePower,Switch,AcEnergy,ReEnergy)
                end
        else
            break
        end
    end
end

--uart.setup(uart.USB, 0, 0, uart.PAR_NONE, uart.STOP_1)
uart.setup(UART_ID,115200,8,uart.PAR_NONE,uart.STOP_1,nil,1)
uart.set_rs485_oe(UART_ID, pio.P0_18)

uart.on(UART_ID, "receive", usbreader)

--1分钟后，关闭usb data功能，切换为usb at功能
--sys.timerStart(uart.close,60000,uart.USB)

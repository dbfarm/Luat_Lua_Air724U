----------------------------------------------------------------------------
-- 1. This file automatically generates code for the LuatOS's UI designer
-- 2. In case of accident, modification is strictly prohibited
----------------------------------------------------------------------------

--Import event file
require "UiHandle"

local function objectHide()
	local o = {}
	local tSelf = {}
	setmetatable(o, tSelf)
	tSelf.__index = tSelf
	tSelf.__tostring = function(j)
		return j.self
	end
	tSelf.__tocall = function(j)
		return j.cb
	end
	tSelf.self = nil
	tSelf.cb = function(e) end
	return o
end

ScreenA = 
{
	create = nil, 
	free = nil,
	contFather_ScreenA = nil,
	LvglButton1 = objectHide(),
	LvglImg1 = objectHide(),
	LvglImg2 = objectHide(),
	LvglImg3 = objectHide(),
	LvglImg4 = objectHide(),
	LvglLabel1 = objectHide(),
	LvglImg5 = objectHide(),
	LvglImg6 = objectHide(),
	LvglLabel2 = objectHide(),
	LvglLabel3 = objectHide(),
	LvglLabel4 = objectHide(),
	LvglLabel5 = objectHide(),
	LvglLabel6 = objectHide(),
	LvglDropdown1 = objectHide(),
	LvglLabel7 = objectHide(),
	LvglTextarea1 = objectHide(),
	LvglLabel8 = objectHide(),
	LvglTextarea2 = objectHide(),
	LvglTextarea3 = objectHide(),
	LvglImg7 = objectHide(),
	LvglLabel10 = objectHide(),
	LvglButton3 = objectHide(),
	LvglButton2 = objectHide(),
	LvglButton4 = objectHide(),
	LvglButton5 = objectHide(),
}
--This is default style of cont which border is invisible
lvgl_UiDesigner_DefaultContStyle = lvgl.style_t()
lvgl.style_init(lvgl_UiDesigner_DefaultContStyle)
lvgl.style_set_radius(lvgl_UiDesigner_DefaultContStyle, (lvgl.STATE_DEFAULT or LV_STATE_FOCUSED or LV_STATE_PRESSED), 0)
lvgl.style_set_border_opa(lvgl_UiDesigner_DefaultContStyle, (lvgl.STATE_DEFAULT or LV_STATE_FOCUSED or LV_STATE_PRESSED), 0)

local function lvgl_UiDesigner_DefOutCb(o, e, output)
	if e == lvgl.EVENT_CLICKED then
		lvgl.obj_set_hidden(output, false)
		lvgl.keyboard_set_textarea(output, o)
	elseif e == lvgl.EVENT_DEFOCUSED then
		lvgl.obj_set_hidden(output, true)
	end
end

local function lvgl_UiDesigner_DefInCb(o, e)
	lvgl.keyboard_def_event_cb(o, e)
	if e == lvgl.EVENT_CANCEL or e == lvgl.EVENT_APPLY then
		lvgl.obj_set_hidden(o, true)
	end
end

----------------------------------------------------------------------------
--The following is the content of screen: ScreenA
---------------------------------------------------------------------------
ScreenA.create = function()
	ScreenA.contFather_ScreenA = lvgl.cont_create(lvgl.scr_act(), nil)
	lvgl.obj_set_size(ScreenA.contFather_ScreenA, 854, 489)
	lvgl.obj_align(ScreenA.contFather_ScreenA, nil, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
	lvgl.obj_add_style(ScreenA.contFather_ScreenA, lvgl.CONT_PART_MAIN, lvgl_UiDesigner_DefaultContStyle)

	--This is the BTN_PART_MAIN's style of ScreenA.LvglButton1
	Style_LvglButton1_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglButton1_1)
	lvgl.style_set_radius(Style_LvglButton1_1, lvgl.STATE_DEFAULT, 4)
	lvgl.style_set_bg_color(Style_LvglButton1_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x0088FF))
	lvgl.style_set_value_str(Style_LvglButton1_1, lvgl.STATE_DEFAULT, "点击加入")
	lvgl.style_set_value_color(Style_LvglButton1_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_value_letter_space(Style_LvglButton1_1, lvgl.STATE_DEFAULT, 4)

	--This is the base code of ScreenA.LvglButton1
	ScreenA.LvglButton1.self = lvgl.btn_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglButton1.self, 362, 79)
	lvgl.obj_set_click(ScreenA.LvglButton1.self, true)
	--This is the virtual Label of LvglButton1
	Style_LvglButton1_vLabel = lvgl.style_t()
	lvgl.style_init(Style_LvglButton1_vLabel)
	vLabel_LvglButton1 = lvgl.label_create(ScreenA.LvglButton1.self, nil)
	lvgl.label_set_text(vLabel_LvglButton1, "")
	lvgl.obj_add_style(vLabel_LvglButton1, lvgl.LABEL_PART_MAIN, Style_LvglButton1_vLabel)
	lvgl.obj_align(ScreenA.LvglButton1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 65, 608)
	lvgl.obj_add_style(ScreenA.LvglButton1.self, lvgl.BTN_PART_MAIN, Style_LvglButton1_1)
	--This is to add callback function for ScreenA.LvglButton1
	--This is callBack function of ScreenA.LvglButton1
	local handleLvglButton1 = function(obj, e)
		ScreenA.LvglButton1.cb(e)
		ScreenA.LvglButton1.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				joinUs()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglButton1.self, handleLvglButton1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg1
	Style_LvglImg1_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg1_1)

	--This is the base code of ScreenA.LvglImg1
	ScreenA.LvglImg1.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg1.self, true)
	lvgl.img_set_src(ScreenA.LvglImg1.self, "/lua/bg.png")
	lvgl.obj_align(ScreenA.LvglImg1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
	lvgl.obj_add_style(ScreenA.LvglImg1.self, lvgl.IMG_PART_MAIN, Style_LvglImg1_1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg2
	Style_LvglImg2_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg2_1)

	--This is the base code of ScreenA.LvglImg2
	ScreenA.LvglImg2.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg2.self, true)
	lvgl.img_set_src(ScreenA.LvglImg2.self, "/lua/bg2.png")
	lvgl.obj_align(ScreenA.LvglImg2.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 16, 69)
	lvgl.obj_add_style(ScreenA.LvglImg2.self, lvgl.IMG_PART_MAIN, Style_LvglImg2_1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg3
	Style_LvglImg3_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg3_1)

	--This is the base code of ScreenA.LvglImg3
	ScreenA.LvglImg3.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg3.self, true)
	lvgl.img_set_src(ScreenA.LvglImg3.self, "/lua/bg2.png")
	lvgl.obj_align(ScreenA.LvglImg3.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 298, 70)
	lvgl.obj_add_style(ScreenA.LvglImg3.self, lvgl.IMG_PART_MAIN, Style_LvglImg3_1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg4
	Style_LvglImg4_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg4_1)

	--This is the base code of ScreenA.LvglImg4
	ScreenA.LvglImg4.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg4.self, true)
	lvgl.img_set_src(ScreenA.LvglImg4.self, "/lua/bg2.png")
	lvgl.obj_align(ScreenA.LvglImg4.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 581, 67)
	lvgl.obj_add_style(ScreenA.LvglImg4.self, lvgl.IMG_PART_MAIN, Style_LvglImg4_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel1
	Style_LvglLabel1_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel1_1)
	lvgl.style_set_text_color(Style_LvglLabel1_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel1_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 36))

	--This is the base code of ScreenA.LvglLabel1
	ScreenA.LvglLabel1.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel1.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel1.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel1.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel1.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel1.self, "Socket测试系统")
	lvgl.obj_align(ScreenA.LvglLabel1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 20, 20)
	lvgl.obj_add_style(ScreenA.LvglLabel1.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel1_1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg5
	Style_LvglImg5_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg5_1)

	--This is the base code of ScreenA.LvglImg5
	ScreenA.LvglImg5.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg5.self, true)
	lvgl.img_set_src(ScreenA.LvglImg5.self, "/lua/status.png")
	lvgl.obj_align(ScreenA.LvglImg5.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 396, 18)
	lvgl.obj_add_style(ScreenA.LvglImg5.self, lvgl.IMG_PART_MAIN, Style_LvglImg5_1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg6
	Style_LvglImg6_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg6_1)

	--This is the base code of ScreenA.LvglImg6
	ScreenA.LvglImg6.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg6.self, true)
	lvgl.img_set_src(ScreenA.LvglImg6.self, "/lua/nosin_icon.png")
	lvgl.obj_align(ScreenA.LvglImg6.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 767, 26)
	lvgl.obj_add_style(ScreenA.LvglImg6.self, lvgl.IMG_PART_MAIN, Style_LvglImg6_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel2
	Style_LvglLabel2_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel2_1)
	lvgl.style_set_text_color(Style_LvglLabel2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel2_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel2
	ScreenA.LvglLabel2.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel2.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel2.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel2.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel2.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel2.self, "2022/3/31 周三 17:53:16")
	lvgl.obj_align(ScreenA.LvglLabel2.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 402, 26)
	lvgl.obj_add_style(ScreenA.LvglLabel2.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel2_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel3
	Style_LvglLabel3_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel3_1)
	lvgl.style_set_text_font(Style_LvglLabel3_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel3
	ScreenA.LvglLabel3.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel3.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel3.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel3.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel3.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel3.self, "连接控制")
	lvgl.obj_align(ScreenA.LvglLabel3.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 106, 98)
	lvgl.obj_add_style(ScreenA.LvglLabel3.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel3_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel4
	Style_LvglLabel4_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel4_1)
	lvgl.style_set_text_font(Style_LvglLabel4_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel4
	ScreenA.LvglLabel4.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel4.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel4.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel4.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel4.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel4.self, "发送数据")
	lvgl.obj_align(ScreenA.LvglLabel4.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 389, 98)
	lvgl.obj_add_style(ScreenA.LvglLabel4.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel4_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel5
	Style_LvglLabel5_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel5_1)
	lvgl.style_set_text_font(Style_LvglLabel5_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel5
	ScreenA.LvglLabel5.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel5.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel5.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel5.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel5.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel5.self, "数据接收")
	lvgl.obj_align(ScreenA.LvglLabel5.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 668, 98)
	lvgl.obj_add_style(ScreenA.LvglLabel5.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel5_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel6
	Style_LvglLabel6_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel6_1)
	lvgl.style_set_text_font(Style_LvglLabel6_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel6
	ScreenA.LvglLabel6.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel6.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel6.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel6.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel6.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel6.self, "协议:")
	lvgl.obj_align(ScreenA.LvglLabel6.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 32, 140)
	lvgl.obj_add_style(ScreenA.LvglLabel6.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel6_1)


	--This is the DROPDOWN_PART_MAIN's style of ScreenA.LvglDropdown1
	Style_LvglDropdown1_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglDropdown1_1)
	lvgl.style_set_text_font(Style_LvglDropdown1_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))
	lvgl.style_set_bg_opa(Style_LvglDropdown1_1, lvgl.STATE_DEFAULT, 255)

	--This is the DROPDOWN_PART_LIST's style of ScreenA.LvglDropdown1
	Style_LvglDropdown1_2 = lvgl.style_t()
	lvgl.style_init(Style_LvglDropdown1_2)
	lvgl.style_set_bg_opa(Style_LvglDropdown1_2, lvgl.STATE_DEFAULT, 255)
	lvgl.style_set_text_font(Style_LvglDropdown1_2, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the DROPDOWN_PART_SCROLLBAR's style of ScreenA.LvglDropdown1
	Style_LvglDropdown1_3 = lvgl.style_t()
	lvgl.style_init(Style_LvglDropdown1_3)

	--This is the DROPDOWN_PART_SELECTED's style of ScreenA.LvglDropdown1
	Style_LvglDropdown1_4 = lvgl.style_t()
	lvgl.style_init(Style_LvglDropdown1_4)

	--This is the base code of ScreenA.LvglDropdown1
	ScreenA.LvglDropdown1.self = lvgl.dropdown_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglDropdown1.self, 152)
	lvgl.obj_set_click(ScreenA.LvglDropdown1.self, true)
	lvgl.dropdown_set_options(ScreenA.LvglDropdown1.self, "TCP\nUDP")
	lvgl.obj_align(ScreenA.LvglDropdown1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 102, 140)
	lvgl.obj_add_style(ScreenA.LvglDropdown1.self, lvgl.DROPDOWN_PART_MAIN, Style_LvglDropdown1_1)
	lvgl.obj_add_style(ScreenA.LvglDropdown1.self, lvgl.DROPDOWN_PART_LIST, Style_LvglDropdown1_2)
	lvgl.obj_add_style(ScreenA.LvglDropdown1.self, lvgl.DROPDOWN_PART_SCROLLBAR, Style_LvglDropdown1_3)
	lvgl.obj_add_style(ScreenA.LvglDropdown1.self, lvgl.DROPDOWN_PART_SELECTED, Style_LvglDropdown1_4)
	--This is to add callback function for ScreenA.LvglDropdown1
	--This is callBack function of ScreenA.LvglDropdown1
	local handleLvglDropdown1 = function(obj, e)
		ScreenA.LvglDropdown1.cb(e)
		ScreenA.LvglDropdown1.cb = function(e)
			if (e == lvgl.EVENT_VALUE_CHANGED)then
				Prot_Dropdown()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglDropdown1.self, handleLvglDropdown1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel7
	Style_LvglLabel7_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel7_1)
	lvgl.style_set_text_font(Style_LvglLabel7_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel7
	ScreenA.LvglLabel7.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel7.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel7.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel7.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel7.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel7.self, "地址:")
	lvgl.obj_align(ScreenA.LvglLabel7.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 32, 232)
	lvgl.obj_add_style(ScreenA.LvglLabel7.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel7_1)


	--This is the TEXTAREA_PART_BG's style of ScreenA.LvglTextarea1
	Style_LvglTextarea1_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglTextarea1_1)
	lvgl.style_set_text_font(Style_LvglTextarea1_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the TEXTAREA_PART_CURSOR's style of ScreenA.LvglTextarea1
	Style_LvglTextarea1_2 = lvgl.style_t()
	lvgl.style_init(Style_LvglTextarea1_2)

	--This is the base code of ScreenA.LvglTextarea1
	ScreenA.LvglTextarea1.self = lvgl.textarea_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglTextarea1.self, 156, 38)
	lvgl.obj_set_click(ScreenA.LvglTextarea1.self, true)
	lvgl.textarea_set_text(ScreenA.LvglTextarea1.self, "112.125.89.8")
	lvgl.textarea_set_placeholder_text(ScreenA.LvglTextarea1.self, "请输入内容")
	lvgl.textarea_set_cursor_hidden(ScreenA.LvglTextarea1.self, true)
	lvgl.textarea_set_scrollbar_mode(ScreenA.LvglTextarea1.self, lvgl.SCROLLBAR_MODE_OFF)
	lvgl.obj_align(ScreenA.LvglTextarea1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 101, 226)
	lvgl.obj_add_style(ScreenA.LvglTextarea1.self, lvgl.TEXTAREA_PART_BG, Style_LvglTextarea1_1)
	lvgl.obj_add_style(ScreenA.LvglTextarea1.self, lvgl.TEXTAREA_PART_CURSOR, Style_LvglTextarea1_2)
	--This is to add callback function for ScreenA.LvglTextarea1
	--This is callBack function of ScreenA.LvglTextarea1
	local handleLvglTextarea1 = function(obj, e)
		ScreenA.LvglTextarea1.cb(e)
		ScreenA.LvglTextarea1.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				Addr_CLICKED()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglTextarea1.self, handleLvglTextarea1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel8
	Style_LvglLabel8_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel8_1)
	lvgl.style_set_text_font(Style_LvglLabel8_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel8
	ScreenA.LvglLabel8.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel8.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel8.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel8.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel8.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel8.self, "端口:")
	lvgl.obj_align(ScreenA.LvglLabel8.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 32, 325)
	lvgl.obj_add_style(ScreenA.LvglLabel8.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel8_1)


	--This is the TEXTAREA_PART_BG's style of ScreenA.LvglTextarea2
	Style_LvglTextarea2_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglTextarea2_1)
	lvgl.style_set_text_font(Style_LvglTextarea2_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the TEXTAREA_PART_CURSOR's style of ScreenA.LvglTextarea2
	Style_LvglTextarea2_2 = lvgl.style_t()
	lvgl.style_init(Style_LvglTextarea2_2)

	--This is the base code of ScreenA.LvglTextarea2
	ScreenA.LvglTextarea2.self = lvgl.textarea_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglTextarea2.self, 156, 38)
	lvgl.obj_set_click(ScreenA.LvglTextarea2.self, true)
	lvgl.textarea_set_text(ScreenA.LvglTextarea2.self, "38392")
	lvgl.textarea_set_placeholder_text(ScreenA.LvglTextarea2.self, "请输入内容")
	lvgl.textarea_set_cursor_hidden(ScreenA.LvglTextarea2.self, true)
	lvgl.textarea_set_scrollbar_mode(ScreenA.LvglTextarea2.self, lvgl.SCROLLBAR_MODE_OFF)
	lvgl.obj_align(ScreenA.LvglTextarea2.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 101, 316)
	lvgl.obj_add_style(ScreenA.LvglTextarea2.self, lvgl.TEXTAREA_PART_BG, Style_LvglTextarea2_1)
	lvgl.obj_add_style(ScreenA.LvglTextarea2.self, lvgl.TEXTAREA_PART_CURSOR, Style_LvglTextarea2_2)
	--This is to add callback function for ScreenA.LvglTextarea2
	--This is callBack function of ScreenA.LvglTextarea2
	local handleLvglTextarea2 = function(obj, e)
		ScreenA.LvglTextarea2.cb(e)
		ScreenA.LvglTextarea2.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				Port_CLICKED()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglTextarea2.self, handleLvglTextarea2)


	--This is the TEXTAREA_PART_BG's style of ScreenA.LvglTextarea3
	Style_LvglTextarea3_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglTextarea3_1)
	lvgl.style_set_text_font(Style_LvglTextarea3_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the TEXTAREA_PART_CURSOR's style of ScreenA.LvglTextarea3
	Style_LvglTextarea3_2 = lvgl.style_t()
	lvgl.style_init(Style_LvglTextarea3_2)

	--This is the base code of ScreenA.LvglTextarea3
	ScreenA.LvglTextarea3.self = lvgl.textarea_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglTextarea3.self, 202, 250)
	lvgl.obj_set_click(ScreenA.LvglTextarea3.self, true)
	lvgl.textarea_set_text(ScreenA.LvglTextarea3.self, "socket send test")
	lvgl.textarea_set_placeholder_text(ScreenA.LvglTextarea3.self, "请输入内容")
	lvgl.textarea_set_cursor_hidden(ScreenA.LvglTextarea3.self, true)
	lvgl.textarea_set_scrollbar_mode(ScreenA.LvglTextarea3.self, lvgl.SCROLLBAR_MODE_OFF)
	lvgl.obj_align(ScreenA.LvglTextarea3.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 326, 136)
	lvgl.obj_add_style(ScreenA.LvglTextarea3.self, lvgl.TEXTAREA_PART_BG, Style_LvglTextarea3_1)
	lvgl.obj_add_style(ScreenA.LvglTextarea3.self, lvgl.TEXTAREA_PART_CURSOR, Style_LvglTextarea3_2)
	--This is to add callback function for ScreenA.LvglTextarea3
	--This is callBack function of ScreenA.LvglTextarea3
	local handleLvglTextarea3 = function(obj, e)
		ScreenA.LvglTextarea3.cb(e)
		ScreenA.LvglTextarea3.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				SendData_CLICKED()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglTextarea3.self, handleLvglTextarea3)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg7
	Style_LvglImg7_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglImg7_1)

	--This is the base code of ScreenA.LvglImg7
	ScreenA.LvglImg7.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg7.self, true)
	lvgl.img_set_src(ScreenA.LvglImg7.self, "/lua/recvbg.png")
	lvgl.obj_align(ScreenA.LvglImg7.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 610, 136)
	lvgl.obj_add_style(ScreenA.LvglImg7.self, lvgl.IMG_PART_MAIN, Style_LvglImg7_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel10
	Style_LvglLabel10_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglLabel10_1)
	lvgl.style_set_text_font(Style_LvglLabel10_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel10
	ScreenA.LvglLabel10.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel10.self, 202)
	lvgl.obj_set_click(ScreenA.LvglLabel10.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel10.self, lvgl.LABEL_LONG_BREAK)
	lvgl.label_set_align(ScreenA.LvglLabel10.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.obj_set_width(ScreenA.LvglLabel10.self, 202)
	lvgl.obj_set_height(ScreenA.LvglLabel10.self, 250)
	lvgl.label_set_text(ScreenA.LvglLabel10.self, "socket recv test")
	lvgl.obj_align(ScreenA.LvglLabel10.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 610, 136)
	lvgl.obj_add_style(ScreenA.LvglLabel10.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel10_1)


	--This is the BTN_PART_MAIN's style of ScreenA.LvglButton3
	Style_LvglButton3_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglButton3_1)
	lvgl.style_set_radius(Style_LvglButton3_1, lvgl.STATE_DEFAULT, 11)
	lvgl.style_set_bg_color(Style_LvglButton3_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x025BB5))
	lvgl.style_set_text_color(Style_LvglButton3_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglButton3_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglButton3
	ScreenA.LvglButton3.self = lvgl.btn_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglButton3.self, 200, 50)
	lvgl.obj_set_click(ScreenA.LvglButton3.self, true)
lvgl.btn_set_state(ScreenA.LvglButton3.self, lvgl.BTN_STATE_RELEASED)
	--This is the virtual Label of LvglButton3
	Style_LvglButton3_vLabel = lvgl.style_t()
	lvgl.style_init(Style_LvglButton3_vLabel)
	lvgl.style_set_text_color(Style_LvglButton3_vLabel, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglButton3_vLabel, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))
	vLabel_LvglButton3 = lvgl.label_create(ScreenA.LvglButton3.self, nil)
	lvgl.label_set_text(vLabel_LvglButton3, "连接")
	lvgl.obj_add_style(vLabel_LvglButton3, lvgl.LABEL_PART_MAIN, Style_LvglButton3_vLabel)
	lvgl.obj_align(ScreenA.LvglButton3.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 48, 405)
	lvgl.obj_add_style(ScreenA.LvglButton3.self, lvgl.BTN_PART_MAIN, Style_LvglButton3_1)
	--This is to add callback function for ScreenA.LvglButton3
	--This is callBack function of ScreenA.LvglButton3
	local handleLvglButton3 = function(obj, e)
		ScreenA.LvglButton3.cb(e)
		ScreenA.LvglButton3.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				Connect_CLICKED()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglButton3.self, handleLvglButton3)


	--This is the BTN_PART_MAIN's style of ScreenA.LvglButton2
	Style_LvglButton2_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglButton2_1)
	lvgl.style_set_radius(Style_LvglButton2_1, lvgl.STATE_DEFAULT, 11)
	lvgl.style_set_bg_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x2774EE))
	lvgl.style_set_text_font(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglButton2
	ScreenA.LvglButton2.self = lvgl.btn_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglButton2.self, 62, 30)
	lvgl.obj_set_click(ScreenA.LvglButton2.self, true)
lvgl.btn_set_state(ScreenA.LvglButton2.self, lvgl.BTN_STATE_RELEASED)
	--This is the virtual Label of LvglButton2
	Style_LvglButton2_vLabel = lvgl.style_t()
	lvgl.style_init(Style_LvglButton2_vLabel)
	lvgl.style_set_text_color(Style_LvglButton2_vLabel, lvgl.STATE_DEFAULT, lvgl.color_hex(0x2774EE))
	lvgl.style_set_text_font(Style_LvglButton2_vLabel, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))
	vLabel_LvglButton2 = lvgl.label_create(ScreenA.LvglButton2.self, nil)
	lvgl.label_set_text(vLabel_LvglButton2, "设置")
	lvgl.obj_add_style(vLabel_LvglButton2, lvgl.LABEL_PART_MAIN, Style_LvglButton2_vLabel)
	lvgl.obj_align(ScreenA.LvglButton2.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 681, 24)
	lvgl.obj_add_style(ScreenA.LvglButton2.self, lvgl.BTN_PART_MAIN, Style_LvglButton2_1)
	--This is to add callback function for ScreenA.LvglButton2
	--This is callBack function of ScreenA.LvglButton2
	local handleLvglButton2 = function(obj, e)
		ScreenA.LvglButton2.cb(e)
		ScreenA.LvglButton2.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				Font_Set()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglButton2.self, handleLvglButton2)


	--This is the BTN_PART_MAIN's style of ScreenA.LvglButton4
	Style_LvglButton4_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglButton4_1)
	lvgl.style_set_radius(Style_LvglButton4_1, lvgl.STATE_DEFAULT, 11)
	lvgl.style_set_bg_color(Style_LvglButton4_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x025BB5))
	lvgl.style_set_text_color(Style_LvglButton4_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglButton4_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglButton4
	ScreenA.LvglButton4.self = lvgl.btn_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglButton4.self, 200, 50)
	lvgl.obj_set_click(ScreenA.LvglButton4.self, true)
lvgl.btn_set_state(ScreenA.LvglButton4.self, lvgl.BTN_STATE_RELEASED)
	--This is the virtual Label of LvglButton4
	Style_LvglButton4_vLabel = lvgl.style_t()
	lvgl.style_init(Style_LvglButton4_vLabel)
	lvgl.style_set_text_color(Style_LvglButton4_vLabel, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglButton4_vLabel, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))
	vLabel_LvglButton4 = lvgl.label_create(ScreenA.LvglButton4.self, nil)
	lvgl.label_set_text(vLabel_LvglButton4, "发送数据")
	lvgl.obj_add_style(vLabel_LvglButton4, lvgl.LABEL_PART_MAIN, Style_LvglButton4_vLabel)
	lvgl.obj_align(ScreenA.LvglButton4.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 328, 405)
	lvgl.obj_add_style(ScreenA.LvglButton4.self, lvgl.BTN_PART_MAIN, Style_LvglButton4_1)
	--This is to add callback function for ScreenA.LvglButton4
	--This is callBack function of ScreenA.LvglButton4
	local handleLvglButton4 = function(obj, e)
		ScreenA.LvglButton4.cb(e)
		ScreenA.LvglButton4.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				Send_CLICKED()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglButton4.self, handleLvglButton4)


	--This is the BTN_PART_MAIN's style of ScreenA.LvglButton5
	Style_LvglButton5_1 = lvgl.style_t()
	lvgl.style_init(Style_LvglButton5_1)
	lvgl.style_set_radius(Style_LvglButton5_1, lvgl.STATE_DEFAULT, 8)
	lvgl.style_set_bg_color(Style_LvglButton5_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x025BB5))
	lvgl.style_set_text_color(Style_LvglButton5_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglButton5_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglButton5
	ScreenA.LvglButton5.self = lvgl.btn_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglButton5.self, 200, 50)
	lvgl.obj_set_click(ScreenA.LvglButton5.self, true)
lvgl.btn_set_state(ScreenA.LvglButton5.self, lvgl.BTN_STATE_RELEASED)
	--This is the virtual Label of LvglButton5
	Style_LvglButton5_vLabel = lvgl.style_t()
	lvgl.style_init(Style_LvglButton5_vLabel)
	lvgl.style_set_text_color(Style_LvglButton5_vLabel, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglButton5_vLabel, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))
	vLabel_LvglButton5 = lvgl.label_create(ScreenA.LvglButton5.self, nil)
	lvgl.label_set_text(vLabel_LvglButton5, "已接收16个字节")
	lvgl.obj_add_style(vLabel_LvglButton5, lvgl.LABEL_PART_MAIN, Style_LvglButton5_vLabel)
	lvgl.obj_align(ScreenA.LvglButton5.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 610, 405)
	lvgl.obj_add_style(ScreenA.LvglButton5.self, lvgl.BTN_PART_MAIN, Style_LvglButton5_1)
end
ScreenA.free = function()
	lvgl.obj_del(ScreenA.contFather_ScreenA)
	ScreenA.contFather_ScreenA = nil
end


----------------------------------------------------------------------------
-----------------------This is the Initial of lvglGUI-----------------------
----------------------------------------------------------------------------
function lvglUiInitial()
	ScreenA.create()
end

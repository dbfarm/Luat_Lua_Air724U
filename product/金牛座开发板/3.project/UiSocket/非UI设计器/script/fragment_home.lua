module(..., package.seeall)

_G.fragmentHome=false

function makeSystemSetBox(cont)
    local sysCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(sysCont,false)
    lvgl.obj_set_size(sysCont, 260, 400) 
    lvgl.obj_add_style(sysCont, lvgl.CONT_PART_MAIN, style.style_contentBox)
    lvgl.obj_align(sysCont, cont, lvgl.ALIGN_IN_TOP_LEFT,20, 0)
    lvgl.cont_set_layout(sysCont,lvgl.LAYOUT_CENTER)
    return sysCont
end

function makeSystemSendBox(cont)
    local sendCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(sendCont,false)
    lvgl.obj_set_size(sendCont, 260, 400) 
    lvgl.obj_add_style(sendCont, lvgl.CONT_PART_MAIN, style.style_contentBox)
    lvgl.obj_align(sendCont, cont, lvgl.ALIGN_IN_TOP_MID,0, 0)
    lvgl.cont_set_layout(sendCont,lvgl.LAYOUT_CENTER)
    return sendCont
end

function makeSystemRecvBox(cont)
    local recvCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(recvCont,false)
    lvgl.obj_set_size(recvCont, 260, 400) 
    lvgl.obj_add_style(recvCont, lvgl.CONT_PART_MAIN, style.style_contentBox)
    lvgl.obj_align(recvCont, cont, lvgl.ALIGN_IN_TOP_RIGHT,-20, 0)
    lvgl.cont_set_layout(recvCont,lvgl.LAYOUT_CENTER)
    return recvCont
end

function init(cont)
    _G.fragmentHome=true
    lvgl.obj_clean(cont)
    local systemSetBox=makeSystemSetBox(cont)    
    local systemSendBox=makeSystemSendBox(cont)
    local SystemRecvBox=makeSystemRecvBox(cont)

    fragment_systemSet.init(systemSetBox)
    fragment_systemSocket.init(systemSendBox,SystemRecvBox)
end

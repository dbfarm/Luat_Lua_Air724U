PROJECT = "ui_socket"
VERSION = "1.0.1"

require "log"
require "sys"
require "http"
require "misc"
require "pins"
require "mipi_lcd_GC9503"
require "tp"
require "service_sim"
require "service_time"
require "style"
require "inputBox"
require "socketTask"

require "fragment_home"
require "fragment_systemSet"
require "fragment_systemSocket"
require "fragment_fontSet"

require"config"
require"nvm"

nvm.init("config.lua")

--真机
if not lvgl.indev_get_emu_touch then
    _G.BEEP=function() pio.pin.plus(13,250,500,20) end
else
--模拟器
    _G.BEEP=function() print("beep") end
end

--去除字体设置
--lvgl.obj_set_style_local_text_font=function (...)
--    return
--end

spi.setup(spi.SPI_1,1,1,8,45000000,1)
font24=lvgl.font_load(spi.SPI_1,24,4,110)
font32=lvgl.font_load(spi.SPI_1,32,4,150)
font36=lvgl.font_load(spi.SPI_1,36,4,156)

_G.fragmentFontSetting = false

lvgl.init(function()
end, tp.input)
lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)

_G.body=lvgl.cont_create(lvgl.scr_act(), nil)
lvgl.obj_set_click(body,false)
lvgl.obj_set_size(body, 854, 480)
lvgl.obj_add_style(body, lvgl.CONT_PART_MAIN, style.style_body)

_G.Font = nvm.get("Font")

function makeHead(cont)
    headCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(headCont,false)
    lvgl.obj_set_size(headCont, 854, 60)
    lvgl.obj_add_style(headCont, lvgl.CONT_PART_MAIN, style.style_divBox)


    local sysname=lvgl.label_create(headCont, nil)
    if _G.Font=="vector"then
        lvgl.obj_set_style_local_text_font(sysname, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font36)
    end
    lvgl.label_set_recolor(sysname, true)
    lvgl.label_set_text(sysname,"#FFFFFF Socket测试系统")
    lvgl.obj_align(sysname, headCont, lvgl.ALIGN_IN_LEFT_MID, 35, 0)

    --状态栏
    local statusLabelCont=lvgl.cont_create(headCont, nil)
    lvgl.obj_set_click(statusLabelCont,false)
    if _G.Font=="vector"then
        lvgl.obj_set_size(statusLabelCont,445, 40)
    else
        lvgl.obj_set_size(statusLabelCont,390, 40)
    end
    -- lvgl.cont_set_layout(statusLabelCont,lvgl.LAYOUT_ROW_MID)
    lvgl.obj_align(statusLabelCont, headCont, lvgl.ALIGN_IN_RIGHT_MID, -20, 0)
    lvgl.obj_add_style(statusLabelCont, lvgl.CONT_PART_MAIN, style.style_statusLabel)

    function timeFnc(label,text)
        lvgl.label_set_text(label,"#FFFFFF "..text)
    end

    --时间标签
    local dateLabel=lvgl.label_create(statusLabelCont, nil)
    if _G.Font=="vector"then
        lvgl.obj_set_style_local_text_font(dateLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    end
    lvgl.label_set_recolor(dateLabel, true)
    lvgl.label_set_text(dateLabel,"")
    lvgl.obj_align(dateLabel,statusLabelCont, lvgl.ALIGN_IN_LEFT_MID,20, 0)
    sys.subscribe("SYSTEM_CHANGE_TIME",function(date)
        lvgl.label_set_text(dateLabel,"#FFFFFF "..date)
        end)

    --设置按钮
    local settingBtn=lvgl.cont_create(statusLabelCont, nil)
        lvgl.obj_set_size(settingBtn, 60,32)
        lvgl.obj_add_style(settingBtn, lvgl.CONT_PART_MAIN, style.style_msgBg)
        lvgl.cont_set_layout(settingBtn,lvgl.LAYOUT_CENTER)
        if _G.Font=="vector"then
            lvgl.obj_align(settingBtn,statusLabelCont, lvgl.ALIGN_IN_LEFT_MID,282, 0)
        else
            lvgl.obj_align(settingBtn,statusLabelCont, lvgl.ALIGN_IN_LEFT_MID,232, 0)
        end

    local settingLabel=lvgl.label_create(settingBtn, nil)
        lvgl.label_set_recolor(settingLabel, true)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(settingLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
        end
        lvgl.label_set_text(settingLabel, "#217bda 设置")

    local function settingHandler(obj, event)
            if event == lvgl.EVENT_CLICKED then
                _G.BEEP()
                -- print("设置按钮按下")
                if not _G.fragmentFontSetting then
                    fragment_fontSet.init(_G.BOX)
                    lvgl.label_set_text(settingLabel, "#217bda 返回")
                    _G.fragmentFontSetting = true

                else
                    --fragment_fontSet.init(unInit)
                    fragment_home.init(_G.BOX)
                    lvgl.label_set_text(settingLabel, "#217bda 设置")
                    _G.fragmentFontSetting = false
                end
            end
        end

        lvgl.obj_set_event_cb(settingBtn,settingHandler)

    --4G 图标
    local signImg=lvgl.img_create(statusLabelCont, nil)
    lvgl.img_set_src(signImg,"/lua/nosin_icon.png")
    lvgl.obj_align(signImg,statusLabelCont, lvgl.ALIGN_IN_RIGHT_MID, -20, 0)

    sys.subscribe("RSSI_CHANGE",function(img)
        lvgl.img_set_src(signImg,img)
    end)
end

function makeBox(cont)
    _G.BOX=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(BOX,false)
    lvgl.obj_set_size(BOX, 854,415)
    lvgl.obj_add_style(BOX, lvgl.CONT_PART_MAIN, style.style_divBox)
    lvgl.obj_align(BOX, cont, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
end

makeHead(_G.body)
makeBox(_G.body)

fragment_home.init(_G.BOX)

function fontChange()
    sys.restart('设置字体重启')
end

sys.subscribe("Font_CHANGE",fontChange)

sys.init(0, 0)
sys.run()

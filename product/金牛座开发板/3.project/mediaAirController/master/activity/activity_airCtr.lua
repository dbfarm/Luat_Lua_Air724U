module(..., package.seeall)


function powerEvent(obj, event)
    if event == lvgl.EVENT_VALUE_CHANGED then
        print("State", lvgl.switch_get_state(obj))
    end
end


function creatLable(obj,text,css)
    local btext = lvgl.label_create(obj, nil) 
    lvgl.label_set_text(btext, text)
    lvgl.obj_align(btext,tepimg, css, 0, 0) 
end

-- function backEvent(obj, event)
--     if event == lvgl.EVENT_CLICKED then
--         sys.taskInit(function()
--             print("back")
--             -- lvgl.obj_clean(contentBox)
--             -- lvgl.obj_del(qrimg)
--             -- print("clean")
--             activity_HOME.init()
--             -- print("home init")
--         end)
--     end
-- end


--------------

function makeRoot(cont)
    lvgl.obj_clean(contentBox)
    -- statusLabel.setBackImg("/lua/backIcon.png")
    -- lvgl.btn_set_state(backimg,lvgl.STATE_DEFAULT)
    lvgl.obj_set_hidden(backimg,false)
    statusLabel.setWinTitle("空调控制器")
end

function makeQR(cont)
    qrimg = lvgl.img_create(cont, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(qrimg, "/lua/qrcode.png")
    lvgl.obj_set_size(qrimg, 260, 260)
    -- 图片居中
    lvgl.obj_align(qrimg, nil, lvgl.ALIGN_IN_TOP_LEFT, 60, 55)
end

function makePower(cont)
    switchLabel = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(switchLabel, true)   
    lvgl.label_set_text(switchLabel, "#FFFFFF 开/关机")
    -- lvgl.obj_align(switchLabel,cont, lvgl.ALIGN_CENTER, -20, -118) 
    lvgl.obj_align(switchLabel,qrimg, lvgl.ALIGN_OUT_RIGHT_TOP, 80,5)

    local sw1 = lvgl.switch_create(cont, nil)
    lvgl.obj_set_size(sw1, 80, 30)
    lvgl.obj_align(sw1, switchLabel, lvgl.ALIGN_IN_LEFT_MID, 100, 0)
    lvgl.obj_set_event_cb(sw1, powerEvent)
end

function makeFilter(cont)

    local slider_event_cb = function(obj, event)
        -- print(event)
        if event == lvgl.EVENT_PRESSED then 
            print("按下")
        elseif event==lvgl.EVENT_RELEASED then
            local val = (lvgl.slider_get_value(obj) or "0").."%"
            print("滤网时间："..val)
        end
    end

    filterLabel = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(filterLabel, true)   
    lvgl.label_set_text(filterLabel, "#FFFFFF 滤网时间")
    lvgl.obj_align(filterLabel,switchLabel, lvgl.ALIGN_IN_LEFT_MID, 0, 68) 

    local slider = lvgl.slider_create(cont, nil)
    lvgl.obj_set_size(slider,250,15)
    lvgl.obj_align(slider, filterLabel, lvgl.ALIGN_IN_LEFT_MID, 100, 0)
    lvgl.obj_set_event_cb(slider, slider_event_cb)
end

function makeTep(cont)
    tepLabel = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(tepLabel, true)   
    lvgl.label_set_text(tepLabel, "#FFFFFF 温度")
    lvgl.obj_align(tepLabel,filterLabel, lvgl.ALIGN_IN_LEFT_MID, 0, 68) 

    local tepimg = lvgl.img_create(cont, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(tepimg, "/lua/tepico.png")
    -- 图片居中
    lvgl.obj_align(tepimg, tepLabel, lvgl.ALIGN_IN_LEFT_MID, 100, 0)

    local tepNum=19

        --温度值
        teptext = lvgl.label_create(cont, nil)
        -- lvgl.label_set_recolor(modeLabel, true)   
        lvgl.label_set_text(teptext, tepNum)
        lvgl.obj_align(teptext,tepimg, lvgl.ALIGN_CENTER, -5, 0) 

    local TepAdd = function(obj, event)
        if event == lvgl.EVENT_CLICKED then
            tepNum=tepNum+1>32 and 32 or tepNum+1
            lvgl.label_set_text(teptext, tepNum)
            print("当前温度："..tepNum)
        end
    end

    local TepLess = function(obj, event)
        if event == lvgl.EVENT_CLICKED then
            tepNum=tepNum-1<16 and 16 or tepNum-1
            lvgl.label_set_text(teptext, tepNum)
            print("当前温度："..tepNum)
        end
    end
    
    -- local s = lvgl.style_create()

    -- lvgl.style_set_bg_color(s, lvgl.CONT_PART_MAIN, lvgl.color_make(0x01, 0xa2, 0xb1))
    -- lvgl.style_set_border_width(s, lvgl.CONT_PART_MAIN, 0)
    -- lvgl.style_set_radius(s, lvgl.CONT_PART_MAIN, 5)

    -- -- 设置风格
    -- lvgl.obj_add_style(cont, lvgl.CONT_PART_MAIN, s)
    -- 按键1
    local btn1 = lvgl.btn_create(cont, nil)
    lvgl.obj_set_size(btn1,45,35)
    lvgl.obj_add_style(btn1, lvgl.CONT_PART_MAIN, stytle.style_activty_airCtr_btn)
    lvgl.obj_set_event_cb(btn1, TepLess)
    lvgl.obj_align(btn1, tepimg, lvgl.ALIGN_IN_LEFT_MID, -3, 0)
    creatLable(btn1,"-",lvgl.ALIGN_CENTER)
    --按键2
    local btn2 = lvgl.btn_create(cont, nil)
    lvgl.obj_set_size(btn2,45,35)
    lvgl.obj_add_style(btn2, lvgl.CONT_PART_MAIN, stytle.style_activty_airCtr_btn)
    lvgl.obj_set_event_cb(btn2, TepAdd)
    lvgl.obj_align(btn2, tepimg, lvgl.ALIGN_IN_RIGHT_MID, 10, 0)
    creatLable(btn2,"+",lvgl.ALIGN_CENTER)

end

function makeMode(cont)
    modeLabel = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(modeLabel, true)   
    lvgl.label_set_text(modeLabel, "#FFFFFF 模式")
    lvgl.obj_align(modeLabel,tepLabel, lvgl.ALIGN_IN_LEFT_MID, 0, 68) 

        -- 回调函数
    event_handler = function(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            local c=""
            lvgl.dropdown_get_selected_str(obj,c,#c)
            print("Option:",c)
        end
    end
    -- 创建下拉框
    local dd = lvgl.dropdown_create(cont, nil)
    lvgl.obj_set_size(dd,150,35)
    lvgl.dropdown_set_dir(dd, lvgl.DROPDOWN_DIR_DOWN)
    lvgl.dropdown_set_options(dd, 
    [[制冷
    除湿
    送风
    自动]])
    -- 设置对齐
    -- lvgl.obj_align(dd,  modeLabel,lvgl.ALIGN_IN_LEFT_MID, 100, -40)
    lvgl.obj_align(dd,  modeLabel,lvgl.ALIGN_IN_LEFT_MID, 100, 0)
    lvgl.obj_set_event_cb(dd, event_handler)
end


function init()
    makeRoot(backIcon)
    makeQR(contentBox)
    makePower(contentBox)
    makeFilter(contentBox)
    makeTep(contentBox)
    makeMode(contentBox)
    _G.AIRCTR=true
end


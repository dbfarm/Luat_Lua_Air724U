module(..., package.seeall)
--------------
require"service_update"

updateLock=false
lastIndex=0
lastLabelText="#FFFFFF 正在请求升级"

local function res(r)
    updateLock=false
    lastIndex=0
    lastLabelText="#FFFFFF 正在请求升级"
    -- if _G.UPDATA then 
    --     lvgl.obj_set_hidden(downLoadCont,true)
    --     lvgl.obj_set_hidden(proImg,false)
    --     lvgl.obj_set_hidden(fwImg,false)
    -- end
    if r then 
        if _G.UPDATA then 
            lvgl.label_set_text(label, "#FFFFFF 升级文件下载成功,正在重启")
            lvgl.obj_align(label, bar, lvgl.ALIGN_OUT_TOP_MID, 0, -10)
        end
        sys.timerStart(sys.restart,2000,"升级成功软件重启")
    else
        sys.timerStart(function()
            if not _G.UPDATA then return end
            lvgl.label_set_text(label, lastLabelText)
            lvgl.bar_set_value(bar,lastIndex, lvgl.ANIM_OFF)
            lvgl.obj_set_hidden(downLoadCont,true)
            lvgl.obj_set_hidden(proImg,false)
            lvgl.obj_set_hidden(fwImg,false)
        end,2000)
    end
end

local function updataFnc(code,status)
    log.info("update",code,status)
    if code ==1 then
        if not _G.UPDATA then return end
        lvgl.label_set_text(label, "#FFFFFF "..status)
        lvgl.bar_set_value(bar,100, lvgl.ANIM_OFF)
    elseif code ==0 then 
        lastIndex=status
        lastLabelText=string.format("#FFFFFF 下载进度%d",status).."%"
        if not _G.UPDATA then return end
        lvgl.bar_set_value(bar,lastIndex, lvgl.ANIM_ON)
        lvgl.label_set_text(label, lastLabelText)
    end
end

function makeRoot()
    lvgl.obj_clean(contentBox)
    -- statusLabel.setBackImg("/lua/backIcon.png")
    -- lvgl.btn_set_state(backimg,lvgl.STATE_DEFAULT)
    lvgl.obj_set_hidden(backimg,false)
    statusLabel.setWinTitle("远程升级")
end


-- projectEvent = function(obj, event)
--     if event == lvgl.EVENT_CLICKED then
--         if updateLock then return end
--         print("工程升级")
--         updateLock=true
--         lvgl.obj_set_hidden(spinner,false)
--         update.request(res)
--     end
-- end

firmwareEvent = function(obj, event)
    if event == lvgl.EVENT_CLICKED then
        if updateLock then return end
        updateLock=true
        lvgl.obj_set_hidden(proImg,true)
        lvgl.obj_set_hidden(fwImg,true)
        lvgl.obj_set_hidden(downLoadCont,false)
        service_update.request(res,nil,nil,nil,updataFnc)
    end
end
function makeButton(cont)
    proImg = lvgl.imgbtn_create(cont, nil)
    lvgl.imgbtn_set_src(proImg, lvgl.BTN_STATE_RELEASED, "/lua/project_updata.png")
    lvgl.imgbtn_set_src(proImg, lvgl.BTN_STATE_PRESSED, "/lua/project_updata.png")
    lvgl.obj_set_event_cb(proImg, firmwareEvent)
    lvgl.obj_align(proImg,cont, lvgl.ALIGN_CENTER, -130,0)

    fwImg = lvgl.imgbtn_create(cont, nil)
    lvgl.imgbtn_set_src(fwImg, lvgl.BTN_STATE_RELEASED, "/lua/firmware_updata.png")
    lvgl.imgbtn_set_src(fwImg, lvgl.BTN_STATE_PRESSED, "/lua/firmware_updata.png")
    lvgl.obj_set_event_cb(fwImg, firmwareEvent)
    lvgl.obj_align(fwImg,cont, lvgl.ALIGN_CENTER, 130,0)

    if updateLock then lvgl.obj_set_hidden(proImg,true) end
    if updateLock then lvgl.obj_set_hidden(fwImg,true) end
end

function makedownLoadCont(cont)

    downLoadCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(downLoadCont,false)
    lvgl.obj_set_size(downLoadCont, 854, 100)
    lvgl.obj_align(downLoadCont, cont, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.obj_add_style(downLoadCont, lvgl.CONT_PART_MAIN, stytle.stytle_divBox)
    if not updateLock then lvgl.obj_set_hidden(downLoadCont,true) end

    bar = lvgl.bar_create(downLoadCont, nil)
    lvgl.obj_set_size(bar, 800, 30)
    lvgl.obj_align(bar,downLoadCont, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.bar_set_value(bar, lastIndex, lvgl.ANIM_OFF)

    label=lvgl.label_create(downLoadCont, nil)
    lvgl.label_set_recolor(label, true)                     
    lvgl.label_set_text(label,lastLabelText)
    lvgl.obj_align(label, bar, lvgl.ALIGN_OUT_TOP_MID, 0, -10)
end

function uninit()
    _G.UPDATA=false
end

function init()
    makeRoot()
    makeButton(contentBox)
    makedownLoadCont(contentBox)
    _G.BACKFNC=uninit
    _G.UPDATA=true


end

-- sys.taskInit(function()
--     init()
-- end)

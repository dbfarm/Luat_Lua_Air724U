module(..., package.seeall)

require "audio"
require "pins"

pins.setup(pio.P0_7,1)
--------------

-- local function cbFncFile(result,prompt,head,filePath)
--     if result and filePath then
--         local size = io.fileSize(filePath)
--         log.info("testHttp.cbFncFile","fileSize="..size,"filePath",filePath)
--         lvgl.img_set_src(showImg, filePath)
--     end
--     --文件使用完之后，如果以后不再用到，需要自行删除
--     if filePath then os.remove(filePath) end
-- end

local downLoadIndex=0
local imgUrl="https://cdn.openluat-luatcommunity.openluat.com/images/20211125101844996_testimg.png"
local musicUrl="https://cdn.openluat-luatcommunity.openluat.com/attachment/20211206134508919_testmusic.mp3"
-- local imgFileName="tettetetettet.png"
local imgFileName=imgUrl:split('/')[#imgUrl:split('/')]
local musicFileName=musicUrl:split('/')[#musicUrl:split('/')]

local downloadFlag=false

function makeRoot()
    lvgl.obj_clean(contentBox)
    -- statusLabel.setBackImg("/lua/backIcon.png")
    -- lvgl.btn_set_state(backimg,lvgl.STATE_DEFAULT)
    lvgl.obj_set_hidden(backimg,false)
    statusLabel.setWinTitle("多媒体下载")
end

function makeBar(cont)
    _G.bar = lvgl.bar_create(cont, nil)
    lvgl.obj_add_style(bar, lvgl.BAR_PART_BG, stytle.style_activty_media_bar)
    lvgl.obj_add_style(bar, lvgl.BAR_PART_INDIC, stytle.style_activty_media_bar)
    -- 设置尺寸
    lvgl.obj_set_size(bar, 854, 15);
    -- 设置位置居中
    lvgl.obj_align(bar,cont, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
    -- 设置加载到的值
    lvgl.bar_set_value(bar, 0, lvgl.ANIM_OFF)


    _G.barLabel=lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(barLabel, true)
    lvgl.label_set_text(barLabel, "#FFFFFF 0%")
    lvgl.obj_align(barLabel, bar, lvgl.ALIGN_OUT_TOP_RIGHT,-5,-2)
    lvgl.bar_set_value(bar,downLoadIndex, lvgl.ANIM_OFF)

    if not (service_download.fileInTable(imgFileName) or service_download.fileInTable(musicFileName)) then
        lvgl.obj_set_hidden(barLabel,true)
        lvgl.obj_set_hidden(bar,true)
    end
    
    -- lvgl.obj_align(barLabel, bar, lvgl.ALIGN_OUT_TOP_RIGHT,-3,-5)
end


function makeImg(cont)
    -- local t=url:split('/')
    -- local imgFileName=t[#t]

    _G.showImg = lvgl.img_create(cont, nil)
    -- 设置图片显示的图像
    if io.exists(imgFileName) then 
        lvgl.img_set_src(showImg, imgFileName)
    else
        lvgl.img_set_src(showImg, "/lua/emptyImg.png")
    end
    -- 图片居中
    lvgl.obj_align(showImg, cont, lvgl.ALIGN_CENTER, -140, 0)
end

function imgDownloadFnc(index,imgFileName)
    downLoadIndex=index
    if index==100 then downLoadIndex=0 end
    if not _G.MEDIA then return end
    -- lvgl.obj_set_hidden(barLabel,false)
    -- lvgl.obj_set_hidden(bar,false)
    lvgl.bar_set_value(bar,index, lvgl.ANIM_OFF)
    lvgl.label_set_text(barLabel, string.format("#FFFFFF %d",index).."%")
    lvgl.obj_align(barLabel, bar, lvgl.ALIGN_OUT_TOP_RIGHT,-5,-2)
    if index ==100 then 
        lvgl.img_set_src(showImg,imgFileName)
        lvgl.obj_align(showImg, contentBox, lvgl.ALIGN_CENTER, -140, 0)
        lvgl.obj_set_hidden(barLabel,true)
        lvgl.obj_set_hidden(bar,true)
        downloadFlag=false
        -- downLoad=false
    end
end

function musicDownloadFnc(index,musicFileName)
    downLoadIndex=index
    if index==100 then downLoadIndex=0 end
    if not _G.MEDIA then return end
    -- lvgl.obj_set_hidden(barLabel,false)
    -- lvgl.obj_set_hidden(bar,false)
    lvgl.bar_set_value(bar,index, lvgl.ANIM_OFF)
    lvgl.label_set_text(barLabel, string.format("#FFFFFF %d",index).."%")
    lvgl.obj_align(barLabel, bar, lvgl.ALIGN_OUT_TOP_RIGHT,-5,-2)
    if index ==100 then 
        lvgl.img_set_src(showImg,"/lua/musicImg.png")
        lvgl.obj_align(showImg, contentBox, lvgl.ALIGN_CENTER, -140, 0)
        lvgl.obj_set_hidden(barLabel,true)
        lvgl.obj_set_hidden(bar,true)
        downloadFlag=false
        audio.play(1,"FILE",musicFileName,3)
        -- downLoad=false
    end
end

imgEvent = function(obj, event)
    if event == lvgl.EVENT_CLICKED then
        if service_download.fileInTable(imgFileName) or downloadFlag then return end
        downloadFlag=true
        print("IMAGE")
        -- downLoad=true
        lvgl.obj_set_hidden(barLabel,false)
        lvgl.obj_set_hidden(bar,false)

        lvgl.img_set_src(showImg, "/lua/emptyImg.png")
        lvgl.bar_set_value(bar, 0, lvgl.ANIM_OFF)
        lvgl.label_set_text(barLabel, "#FFFFFF 0%")
        lvgl.obj_set_hidden(barLabel,false)
        lvgl.obj_set_hidden(bar,false)
        service_download.downloadService(imgUrl,imgDownloadFnc)

        -- -- lvgl.img_set_src(showImg, "/lua/testimg.png")
        -- http.request("GET","https://cdn.openluat-luatcommunity.openluat.com/images/20211125101844996_testimg.png",nil,nil,nil,30000,cbFncFile,"download.png")
        -- lvgl.obj_align(showImg, nil, lvgl.ALIGN_CENTER, -140, 0)
    end
end

musicEvent = function(obj, event)
    if event == lvgl.EVENT_CLICKED then
        if service_download.fileInTable(musicFileName)  or downloadFlag then return end
        downloadFlag=true
        print("MUSIC")
        -- downLoad=true
        lvgl.obj_set_hidden(barLabel,false)
        lvgl.obj_set_hidden(bar,false)

        lvgl.img_set_src(showImg, "/lua/emptyImg.png")
        lvgl.bar_set_value(bar, 0, lvgl.ANIM_OFF)
        lvgl.label_set_text(barLabel, "#FFFFFF 0%")
        lvgl.obj_set_hidden(barLabel,false)
        lvgl.obj_set_hidden(bar,false)
        service_download.downloadService(musicUrl,musicDownloadFnc)
        -- audio.play(1,"FILE","/lua/call.mp3",3)
    end
end



function makeButton(cont)
    local img = lvgl.imgbtn_create(cont, nil)
    lvgl.imgbtn_set_src(img, lvgl.BTN_STATE_RELEASED, "/lua/imageDownLoad.png")
    lvgl.imgbtn_set_src(img, lvgl.BTN_STATE_PRESSED, "/lua/imageDownLoad.png")
    -- lvgl.obj_set_click(img,true)
    -- lvgl.img_set_src(img, "/lua/imageDownLoad.png")
    lvgl.obj_set_event_cb(img, imgEvent)
    lvgl.obj_set_pos(img, 583,106);

    local music = lvgl.imgbtn_create(cont, nil)
    lvgl.imgbtn_set_src(music, lvgl.BTN_STATE_RELEASED, "/lua/musicDownLoad.png")
    lvgl.imgbtn_set_src(music, lvgl.BTN_STATE_PRESSED, "/lua/musicDownLoad.png")
    -- lvgl.obj_set_click(music,true)
    -- lvgl.img_set_src(music, "/lua/musicDownLoad.png")
    lvgl.obj_set_event_cb(music, musicEvent)
    lvgl.obj_set_pos(music, 583,255);
end

function uninit()
    audio.stop()
    _G.MEDIA=false
end

function init()
    makeRoot()
    makeBar(contentBox)
    makeImg(contentBox)
    makeButton(contentBox)
    _G.BACKFNC=uninit
    _G.MEDIA=true
end
-- sys.taskInit(init, ...)


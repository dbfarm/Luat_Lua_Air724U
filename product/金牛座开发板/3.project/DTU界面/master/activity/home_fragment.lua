module(..., package.seeall)

local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end
-------------
local UI=UIConfig.UIParams
local moto=UI.moto
local wind=UI.wind
local energy=UI.energy
local area=UI.area
-------------
function makeLeftArea(cont)
    local leftArea=lvgl.cont_create(cont, nil)
    lvgl.obj_set_size(leftArea,215, 440)
    lvgl.obj_align(leftArea, nil, lvgl.ALIGN_IN_LEFT_MID, 0, 0)
    lvgl.obj_add_style(leftArea, lvgl.CONT_PART_MAIN, style.style_div)

    local function event_handler(obj, event)
        if event == lvgl.EVENT_CLICKED then
            BEEP()
            unInit()
            driver_fragment.Init()
            print("左按下")
        end
    end
------------顶部按钮-------------------------
    buttonLeft = lvgl.img_create(leftArea, nil)
    lvgl.img_set_src(buttonLeft, "/lua/head_icon.png")
    lvgl.obj_align(buttonLeft, nil, lvgl.ALIGN_IN_TOP_RIGHT, 0, 15)
    lvgl.obj_set_click(buttonLeft,true)
    lvgl.obj_set_event_cb(buttonLeft, event_handler)

    local icon=lvgl.img_create(buttonLeft, nil)
    lvgl.img_set_src(icon, "/lua/SB.bin")
    lvgl.obj_align(icon, nil, lvgl.ALIGN_IN_LEFT_MID, 10, 0)
    local label_buttonLeft = lvgl.label_create(buttonLeft, nil)
    lvgl.label_set_recolor(label_buttonLeft, true)
    lvgl.label_set_text(label_buttonLeft,"#FFFFFF 设备总览")
    lvgl.obj_align(label_buttonLeft,icon, lvgl.ALIGN_OUT_RIGHT_MID, 5, 0)

------------第一个区域-------------------------
    local cont1=lvgl.cont_create(leftArea, nil)
    lvgl.obj_set_size(cont1,210, 164)
    lvgl.obj_align(cont1,buttonLeft, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 20)
    lvgl.obj_add_style(cont1, lvgl.CONT_PART_MAIN, style.style_div)

    local backImg = lvgl.img_create(cont1, nil)
    lvgl.img_set_src(backImg, "/lua/msg_div.png")

    local label=lvgl.label_create(backImg, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,"#FFFFFF 电机负载")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)

    local spinner1 = lvgl.spinner_create(cont1, nil)
    lvgl.obj_set_size(spinner1, 90, 90)
    lvgl.spinner_set_arc_length(spinner1, 250)
    lvgl.spinner_set_spin_time(spinner1, 4000)
    lvgl.obj_align(spinner1, nil, lvgl.ALIGN_IN_LEFT_MID, 15, 10) 
    lvgl.obj_add_style(spinner1, lvgl.SPINNER_PART_INDIC, style.style_arc_front1)
    lvgl.obj_add_style(spinner1, lvgl.SPINNER_PART_BG, style.style_arc_bg_0)

    local spinner2 = lvgl.spinner_create(spinner1, nil)
    lvgl.spinner_set_dir(spinner2, 	lvgl.SPINNER_DIR_BACKWARD)
    lvgl.obj_set_size(spinner2, 78, 78)
    lvgl.spinner_set_arc_length(spinner2, 230)
    lvgl.spinner_set_spin_time(spinner2, 4000)
    lvgl.obj_align(spinner2, nil, lvgl.ALIGN_CENTER, 0, 0) 
    lvgl.obj_add_style(spinner2, lvgl.SPINNER_PART_INDIC, style.style_arc_front2)
    lvgl.obj_add_style(spinner2, lvgl.SPINNER_PART_BG, style.style_arc_bg_0)

    local spinner3 = lvgl.spinner_create(spinner2, nil)
    lvgl.obj_set_size(spinner3, 66, 66)
    lvgl.spinner_set_arc_length(spinner3, 200)
    lvgl.spinner_set_spin_time(spinner3, 4000)
    lvgl.obj_align(spinner3, nil, lvgl.ALIGN_CENTER, 0, 0) 
    lvgl.obj_add_style(spinner3, lvgl.SPINNER_PART_INDIC, style.style_arc_front3)
    lvgl.obj_add_style(spinner3, lvgl.SPINNER_PART_BG, style.style_arc_bg_0)

    Motolabel=lvgl.label_create(spinner3, nil)
    lvgl.label_set_recolor(Motolabel, true)
    lvgl.obj_set_style_local_text_font(Motolabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(Motolabel,string.format("#28e9ef %d%%",(moto.now/moto.P)*100))
    lvgl.obj_align(Motolabel, nil, lvgl.ALIGN_CENTER, 0, 0)
------------------------
    local c1=lvgl.cont_create(cont1, nil)
    lvgl.obj_set_size(c1,80, 25)
    lvgl.obj_align(c1,nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 40)
    lvgl.obj_add_style(c1, lvgl.CONT_PART_MAIN, style.style_div_blue)

    local motolabel1=lvgl.label_create(c1,nil)
    lvgl.label_set_recolor(motolabel1, true)
    lvgl.obj_set_style_local_text_font(motolabel1, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
    lvgl.label_set_text(motolabel1,"#1470cc 额定功率")
    lvgl.obj_align(motolabel1, nil, lvgl.ALIGN_CENTER, 0, 0)
----------------
    local c2=lvgl.cont_create(cont1, nil)
    lvgl.obj_set_size(c2,80, 25)
    lvgl.obj_align(c2,c1, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
    lvgl.obj_add_style(c2, lvgl.CONT_PART_MAIN, style.style_div_blue_lite)

    motoP=lvgl.label_create(c2,nil)
    lvgl.label_set_recolor(motoP, true)
    lvgl.obj_set_style_local_text_font(motoP, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
    lvgl.label_set_text(motoP,string.format("#18aaf2 %dKW",moto.P))
    lvgl.obj_align(motoP, nil, lvgl.ALIGN_CENTER, 0, 0)


---------------
    local c3=lvgl.cont_create(cont1, nil)
    lvgl.obj_set_size(c3,80, 25)
    lvgl.obj_align(c3,c2, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
    lvgl.obj_add_style(c3, lvgl.CONT_PART_MAIN, style.style_div_blue)

    local motolabel1=lvgl.label_create(c3,nil)
    lvgl.label_set_recolor(motolabel1, true)
    lvgl.obj_set_style_local_text_font(motolabel1, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
    lvgl.label_set_text(motolabel1,"#1470cc 当前功率")
    lvgl.obj_align(motolabel1, nil, lvgl.ALIGN_CENTER, 0, 0)

    -----------------------

    local c4=lvgl.cont_create(cont1, nil)
    lvgl.obj_set_size(c4,80, 25)
    lvgl.obj_align(c4,c3, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
    lvgl.obj_add_style(c4, lvgl.CONT_PART_MAIN, style.style_div_blue_lite)

    motoU=lvgl.label_create(c4,nil)
    lvgl.label_set_recolor(motoU, true)
    lvgl.obj_set_style_local_text_font(motoU, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
    lvgl.label_set_text(motoU,string.format("#18aaf2 %dKW",moto.now))
    lvgl.obj_align(motoU, nil, lvgl.ALIGN_CENTER, 0, 0)


    local moto_cb=function()
        lvgl.label_set_text(motoU,string.format("#18aaf2 %dKW",moto.now))
        lvgl.obj_align(motoU, nil, lvgl.ALIGN_CENTER, 0, 0)

        lvgl.label_set_text(motoP,string.format("#18aaf2 %dKW",moto.P))
        lvgl.obj_align(motoP, nil, lvgl.ALIGN_CENTER, 0, 0)

        lvgl.label_set_text(Motolabel,string.format("#28e9ef %d%%",(moto.now/moto.P)*100))
        lvgl.obj_align(Motolabel, nil, lvgl.ALIGN_CENTER, 0, 0)
    end

    topicTableAdd("UI_MOTO",moto_cb)
    


------------第二个区域-------------------------
    local cont2=lvgl.cont_create(leftArea, nil)
    lvgl.obj_set_size(cont2,210, 164)
    lvgl.obj_align(cont2,cont1, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 20)
    lvgl.obj_add_style(cont2, lvgl.CONT_PART_MAIN, style.style_div)

    local backImg = lvgl.img_create(cont2, nil)
    lvgl.img_set_src(backImg, "/lua/msg_div.png")

    local label=lvgl.label_create(backImg, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,"#FFFFFF 空调")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)

    local sendWindImg = lvgl.img_create(cont2, nil)
    lvgl.img_set_src(sendWindImg, "/lua/send_wind.png")
    lvgl.obj_align(sendWindImg,nil, lvgl.ALIGN_IN_LEFT_MID, 8, -5)

    local returnWindImg = lvgl.img_create(cont2, nil)
    lvgl.img_set_src(returnWindImg, "/lua/return_wind.png")
    lvgl.obj_align(returnWindImg,nil, lvgl.ALIGN_IN_RIGHT_MID, -8, -5)

    send_wind_label=lvgl.label_create(sendWindImg,nil)
    lvgl.obj_set_style_local_text_font(send_wind_label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_recolor(send_wind_label, true)
    lvgl.label_set_text(send_wind_label,string.format("#3fb05a %.1f",wind.send_wind))
    lvgl.obj_align(send_wind_label, nil, lvgl.ALIGN_IN_RIGHT_MID, -42, 8)

    local label=lvgl.label_create(cont2, nil)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,"#FFFFFF 送风")
    lvgl.obj_align(label, nil, lvgl.ALIGN_OUT_BOTTOM_MID, -50, -30)

    return_wind_label=lvgl.label_create(returnWindImg,nil)
    lvgl.obj_set_style_local_text_font(return_wind_label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_recolor(return_wind_label, true)
    lvgl.label_set_text(return_wind_label,string.format("#3fb05a %.1f",wind.return_wind))
    lvgl.obj_align(return_wind_label, nil, lvgl.ALIGN_IN_RIGHT_MID, -42, 8)

    local label=lvgl.label_create(cont2, nil)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,"#FFFFFF 回风")
    lvgl.obj_align(label, nil, lvgl.ALIGN_OUT_BOTTOM_MID, 50, -30)


    local wind_cb=function()
        lvgl.label_set_text(send_wind_label,string.format("#3fb05a %.1f",wind.send_wind))
        lvgl.obj_align(send_wind_label, nil, lvgl.ALIGN_IN_RIGHT_MID, -42, 8)

        lvgl.label_set_text(return_wind_label,string.format("#3fb05a %.1f",wind.return_wind))
        lvgl.obj_align(return_wind_label, nil, lvgl.ALIGN_IN_RIGHT_MID, -42, 8)
    end
    topicTableAdd("UI_WIND",wind_cb)

end

function makeRightArea(cont)
    local rightArea=lvgl.cont_create(cont, nil)
    lvgl.obj_set_size(rightArea,215, 440)
    lvgl.obj_align(rightArea, nil, lvgl.ALIGN_IN_RIGHT_MID, 0, 0)
    lvgl.obj_add_style(rightArea, lvgl.CONT_PART_MAIN, style.style_div)

    local function event_handler(obj, event)
        if event == lvgl.EVENT_CLICKED then
            BEEP()
            unInit()
            error_fragment.Init()
        end
    end
------------顶部按钮-------------------------
    buttonLRight = lvgl.img_create(rightArea, nil)
    lvgl.img_set_src(buttonLRight, "/lua/head_icon.png")
    lvgl.obj_align(buttonLRight, nil, lvgl.ALIGN_IN_TOP_LEFT, 0, 15)
    lvgl.obj_set_click(buttonLRight,true)
    lvgl.obj_set_event_cb(buttonLRight, event_handler)

    local icon=lvgl.img_create(buttonLRight, nil)
    lvgl.img_set_src(icon, "/lua/LD.bin")
    lvgl.obj_align(icon, nil, lvgl.ALIGN_IN_LEFT_MID, 0, 0)

    local label_buttonRight = lvgl.label_create(buttonLRight, nil)
    lvgl.label_set_recolor(label_buttonRight, true)
    lvgl.label_set_text(label_buttonRight,"#FFFFFF 告警信息")
    lvgl.obj_align(label_buttonRight, icon, lvgl.ALIGN_OUT_RIGHT_MID, -5, 0)

    local w_cont=lvgl.cont_create(buttonLRight, nil)
    lvgl.obj_set_size(w_cont,38, 20)
    lvgl.obj_set_style_local_text_font(w_cont, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.obj_add_style(w_cont, lvgl.CONT_PART_MAIN, style.style_w_cont)
    lvgl.obj_align(w_cont, nil, lvgl.ALIGN_IN_RIGHT_MID, -30, 0)

    local w_label = lvgl.label_create(w_cont, nil)
    lvgl.label_set_recolor(w_label, true)
    local function nn()
        local t=UIConfig.getMessage()
        lvgl.label_set_text(w_label,"#FFFFFF "..t.cnt)
        lvgl.obj_align(w_label, nil, lvgl.ALIGN_CENTER, 0, 0)
    end

    nn()
    topicTableAdd("UI_MESSAGE_CHANGE",nn)
------------第一个区域-------------------------
    local cont1=lvgl.cont_create(rightArea, nil)
    lvgl.obj_set_size(cont1,210, 164)
    lvgl.obj_align(cont1,buttonLRight, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 20)
    lvgl.obj_add_style(cont1, lvgl.CONT_PART_MAIN, style.style_div)

    local backImg = lvgl.img_create(cont1, nil)
    lvgl.img_set_src(backImg, "/lua/msg_div.png")

    local label=lvgl.label_create(backImg, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,"#FFFFFF 能量监测")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)

    local c1=lvgl.cont_create(cont1, nil)
    lvgl.obj_set_size(c1,185, 60)
    lvgl.obj_align(c1,nil, lvgl.ALIGN_IN_TOP_MID, 0, 25)
    lvgl.obj_add_style(c1, lvgl.CONT_PART_MAIN, style.style_div_blue_lite)

    local img=lvgl.img_create(c1, nil)
    lvgl.img_set_src(img, "/lua/E1.png")
    lvgl.obj_align(img, nil, lvgl.ALIGN_IN_LEFT_MID, 0, 0)

    local label=lvgl.label_create(c1, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_text(label,"#FFFFFF 有功功率")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 3)

    PKV=lvgl.label_create(c1, nil)
    lvgl.label_set_recolor(PKV, true)
    lvgl.obj_set_style_local_text_font(PKV, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(PKV,string.format("#18AAF2 %.2f",energy.kv))
    lvgl.obj_align(PKV, nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 20)

    local label=lvgl.label_create(c1, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_text(label,"#AAAAAA KWh")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 42)

    ---------------
    local c2=lvgl.cont_create(cont1, nil)
    lvgl.obj_set_size(c2,185, 60)
    lvgl.obj_align(c2,c1, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 7)
    lvgl.obj_add_style(c2, lvgl.CONT_PART_MAIN, style.style_div_blue_lite)

    local img=lvgl.img_create(c2, nil)
    lvgl.img_set_src(img, "/lua/E2.png")
    lvgl.obj_align(img, nil, lvgl.ALIGN_IN_LEFT_MID, 0, 0)

    local label=lvgl.label_create(c2, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_text(label,"#FFFFFF 无功功率")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 3)

    local PKVR=lvgl.label_create(c2, nil)
    lvgl.label_set_recolor(PKVR, true)
    lvgl.obj_set_style_local_text_font(PKVR, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(PKVR,string.format("#18AAF2 %.2f",energy.kvar))
    lvgl.obj_align(PKVR, nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 20)

    local label=lvgl.label_create(c2, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
    lvgl.label_set_text(label,"#AAAAAA Kvarh")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 42)

    local energy_cb=function()
        lvgl.label_set_text(PKVR,string.format("#18AAF2 %.2f",energy.kvar))
        lvgl.obj_align(PKVR, nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 20)
        lvgl.label_set_text(PKV,string.format("#18AAF2 %.2f",energy.kv))
        lvgl.obj_align(PKV, nil, lvgl.ALIGN_IN_TOP_RIGHT, -15, 20)
    end

    topicTableAdd("UI_ENERGY",energy_cb)

------------第二个区域-------------------------
    local cont2=lvgl.cont_create(rightArea, nil)
    lvgl.obj_set_size(cont2,210, 164)
    lvgl.obj_align(cont2,cont1, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 20)
    lvgl.obj_add_style(cont2, lvgl.CONT_PART_MAIN, style.style_div)

    local backImg = lvgl.img_create(cont2, nil)
    lvgl.img_set_src(backImg, "/lua/msg_div.png")

    local label=lvgl.label_create(backImg, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,"#FFFFFF 告警统计")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)

    local c1=lvgl.cont_create(cont2, nil)
    lvgl.obj_set_size(c1,140, 140)
    lvgl.obj_add_style(c1, lvgl.CONT_PART_MAIN, style.style_div)
    lvgl.obj_align(c1, nil, lvgl.ALIGN_IN_RIGHT_MID, 5, 10)

    local c2=lvgl.cont_create(cont2, nil)
    lvgl.obj_set_size(c2,80, 130)
    lvgl.obj_add_style(c2, lvgl.CONT_PART_MAIN, style.style_div)
    -- lvgl.cont_set_layout(c2, lvgl.LAYOUT_COLUMN_MID)
    lvgl.obj_align(c2, nil, lvgl.ALIGN_IN_TOP_LEFT, 10, 30)

    local function makeinfo(cont,info,dotstyle,textcolor,txt,dr)
        local c=lvgl.cont_create(cont, nil)
        lvgl.obj_add_style(c, lvgl.CONT_PART_MAIN, style.style_div)
        lvgl.obj_set_size(c,80, 20)
        if dr then lvgl.obj_align(c, dr, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 4) end

        local dot=lvgl.cont_create(c, nil)        
        lvgl.obj_add_style(dot, lvgl.CONT_PART_MAIN, dotstyle)
        lvgl.obj_set_size(dot,13, 13)
        lvgl.obj_align(dot, nil, lvgl.ALIGN_IN_LEFT_MID, 2, 0)

        local label=lvgl.label_create(c, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,string.format("%s %s",textcolor,info))
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 20, 0)

        local numlab=lvgl.label_create(c, nil)
        lvgl.label_set_recolor(numlab, true)
        lvgl.obj_set_style_local_text_font(numlab, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
        lvgl.label_set_text(numlab,"#4fb9db "..txt)
        lvgl.obj_align(numlab, nil, lvgl.ALIGN_IN_RIGHT_MID, -8, 0)

        return c,numlab
    end
    local t=UIConfig.getMessage()
    local urgentCont,urgent=makeinfo(c2,"紧急",style.style_arc_urgent,"#f43b42",#t.urgent.data)
    local errorCont,error=makeinfo(c2,"严重",style.style_arc_error,"#fb8357",#t.error.data,urgentCont)
    local warnCont,warn=makeinfo(c2,"重要",style.style_arc_warn,"#f8c900",#t.warn.data,errorCont)
    local nomalCont,nomal=makeinfo(c2,"一般",style.style_arc_nomal,"#75bc52",#t.nomal.data,warnCont)
    local infoCont,info=makeinfo(c2,"提示",style.style_arc_info,"#4fb9db",#t.info.data,nomalCont)

    local makeArc=function(s,e,sty,cont)
        local arc = lvgl.arc_create(cont, nil)
        lvgl.obj_set_size(arc, 130, 130)
        lvgl.obj_align(arc, nil, lvgl.ALIGN_CENTER, 0, 0)
        -- lvgl.arc_set_bg_angles(arc, 0, 360)
        lvgl.arc_set_angles(arc, s, e)
        lvgl.obj_add_style(arc, lvgl.CONT_PART_MAIN, style.style_div)
        lvgl.obj_add_style(arc, lvgl.indev_get_emu_touch and lvgl.CONT_PART_MAIN or lvgl.ARC_PART_MAIN, style.style_arc_bg_0)
        lvgl.obj_add_style(arc, lvgl.ARC_PART_INDIC, sty)
    end

    local cavasC=function()
        sys.publish("UI_MESSAGE_CHANGE")
        lvgl.obj_clean(c1)
        local t=UIConfig.getMessage()
        local label=lvgl.label_create(c1, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
        lvgl.label_set_text(label,"#4fb9db "..t.cnt)
        lvgl.obj_align(label, nil, lvgl.ALIGN_CENTER, 0, -15)

        local label=lvgl.label_create(c1, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
        lvgl.label_set_text(label,"#FFFFFF 告警统计")
        lvgl.obj_align(label, nil, lvgl.ALIGN_CENTER, 0, 12)

        for k, v in pairs(t) do
            if type(v)=="table" and #v.data>0 then
                makeArc(v.start,v.ending,v.style,c1)
            end
        end
        -----------
        lvgl.label_set_text(urgent,"#4fb9db "..#t.urgent.data)
        lvgl.label_set_text(error,"#4fb9db "..#t.error.data)
        lvgl.label_set_text(warn,"#4fb9db "..#t.warn.data)
        lvgl.label_set_text(nomal,"#4fb9db "..#t.nomal.data)
        lvgl.label_set_text(info,"#4fb9db "..#t.info.data)
        -----------
    end
    cavasC()

    topicTableAdd("UI_MESSAGE",cavasC)
end

function makeMidArea(cont)
    local MidArea=lvgl.cont_create(cont, nil)
    lvgl.obj_set_size(MidArea,400, 440)
    lvgl.obj_align(MidArea, nil, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.obj_add_style(MidArea, lvgl.CONT_PART_MAIN, style.style_div)

    local function event_handler(obj, event)
        if event == lvgl.EVENT_CLICKED then
            BEEP()
            unInit()
            setting_fragment.Init()
        end
    end


    local roomImg = lvgl.img_create(MidArea, nil)
    lvgl.img_set_src(roomImg, "/lua/room.png")
    lvgl.obj_align(roomImg, nil, lvgl.ALIGN_CENTER, 0, -70)
    lvgl.obj_set_click(roomImg,true)
    lvgl.obj_set_event_cb(roomImg, event_handler)
-------
    local A_area=lvgl.img_create(roomImg, nil)
    lvgl.img_set_src(A_area, "/lua/icon.bin")
    lvgl.obj_align(A_area, nil, lvgl.ALIGN_IN_TOP_LEFT, 68, 10)

    local B_area=lvgl.img_create(roomImg, nil)
    lvgl.img_set_src(B_area, "/lua/icon.bin")
    lvgl.obj_align(B_area, nil, lvgl.ALIGN_IN_TOP_LEFT, 27, 80)

    local C_area=lvgl.img_create(roomImg, nil)
    lvgl.img_set_src(C_area, "/lua/icon.bin")
    lvgl.obj_align(C_area, nil, lvgl.ALIGN_IN_TOP_LEFT,100, 73)

    local D_area=lvgl.img_create(roomImg, nil)
    lvgl.img_set_src(D_area, "/lua/icon.bin")
    lvgl.obj_align(D_area, nil, lvgl.ALIGN_IN_TOP_LEFT,130, 45)

    local E_area=lvgl.img_create(roomImg, nil)
    lvgl.img_set_src(E_area, "/lua/icon.bin")
    lvgl.obj_align(E_area, nil, lvgl.ALIGN_IN_TOP_LEFT,210, 15)

    local F_area=lvgl.img_create(roomImg, nil)
    lvgl.img_set_src(F_area, "/lua/icon.bin")
    lvgl.obj_align(F_area, nil, lvgl.ALIGN_IN_TOP_LEFT,300, 20)

    local G_area=lvgl.img_create(roomImg, nil)
    lvgl.img_set_src(G_area, "/lua/icon.bin")
    lvgl.obj_align(G_area, nil, lvgl.ALIGN_IN_TOP_LEFT,340,65)

    local setopa=function(img,f)
        sys.taskInit(function(img,f)            
            if not lvgl.indev_get_emu_touch then
                if f and lvgl.obj_get_style_image_opa(img)==0xff then  return end
                if not f and lvgl.obj_get_style_image_opa(img)==0x00 then  return end
            end
            local index=0xff
            local ending=0x00
            if f then
                index=index+ending
                ending=index-ending
                index=index-ending
            end
            local step=(index>ending) and -51 or 17
        for i = index, ending,step do
            if not _G.home_flag then return end
            lvgl.obj_set_style_local_image_opa(img, lvgl.IMG_PART_MAIN, lvgl.STATE_DEFAULT, i)
            sys.wait(5)
        end
        end, img,f)
    end

    local area_hiden=function()
        setopa(A_area,area.A)
        setopa(B_area,area.B)
        setopa(C_area,area.C)
        setopa(D_area,area.D)
        setopa(E_area,area.E)
        setopa(F_area,area.F)
        setopa(G_area,area.G)
    end

    -- sys.timerLoopStart(function(img) print(lvgl.obj_get_style_image_opa(img)) end, 100, A_area)

    area_hiden()

    topicTableAdd("UI_AREA",area_hiden)
    
------------


    ----------------------------
    local msgArea=lvgl.cont_create(MidArea, nil)
    lvgl.obj_set_size(msgArea,400, 130)
    lvgl.obj_align(msgArea, nil, lvgl.ALIGN_IN_BOTTOM_MID, 0, -20)
    lvgl.obj_add_style(msgArea, lvgl.CONT_PART_MAIN, style.style_temp)

    local backImg = lvgl.img_create(msgArea, nil)
    lvgl.img_set_src(backImg, "/lua/bottom_div.png")

    local label=lvgl.label_create(backImg, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,"#FFFFFF 实时告警信息")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)

    local msgBox=lvgl.cont_create(msgArea, nil)
    lvgl.obj_set_size(msgBox,380, 100)
    lvgl.obj_align(msgBox, nil, lvgl.ALIGN_IN_BOTTOM_MID, 0, -8)
    lvgl.obj_add_style(msgBox, lvgl.CONT_PART_MAIN, style.style_div)

    local makeitem=function(cont,t,dc)
        local itencnt=lvgl.cont_create(cont, nil)
        lvgl.obj_set_size(itencnt,380, 30)
        lvgl.obj_add_style(itencnt, lvgl.CONT_PART_MAIN, style.style_div_blue)

        if dc then lvgl.obj_align(itencnt, dc, lvgl.ALIGN_OUT_TOP_MID, 0, -5) end

        local color={
            urgent={
                text="紧急",
                color="f43b42"
            },
            error={
                text="严重",
                color="fb8357"
            },
            warn={
                text="重要",
                color="f8c900"
            },
            nomal={
                text="一般",
                color="75bc52"
            },
            info={
                text="提示",
                color="4fb9db"
            },
}

        local function m_style(cor)
            local s= lvgl.style_t()
            lvgl.style_init(s)
            lvgl.style_set_bg_color(s, lvgl.CONT_PART_MAIN,lvgl.color_hex(cor))
            lvgl.style_set_radius(s, lvgl.CONT_PART_MAIN, 0)
            lvgl.style_set_border_width(s, lvgl.CONT_PART_MAIN, 0)
            lvgl.style_set_bg_opa(s, lvgl.CONT_PART_MAIN,255)
        
            lvgl.style_set_pad_left(s, lvgl.CONT_PART_MAIN,5)
            lvgl.style_set_pad_right(s, lvgl.CONT_PART_MAIN,5)
            lvgl.style_set_pad_top(s, lvgl.CONT_PART_MAIN,3)
            lvgl.style_set_pad_bottom(s, lvgl.CONT_PART_MAIN,3)
            lvgl.style_set_pad_inner(s, lvgl.CONT_PART_MAIN,0)
        
            lvgl.style_set_margin_top(s, lvgl.CONT_PART_MAIN,0)
            lvgl.style_set_margin_bottom(s, lvgl.CONT_PART_MAIN,0)
            return s
        end

        local label=lvgl.label_create(itencnt, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,"#FFFFFF "..color[t.level].text)
        lvgl.obj_add_style(label, lvgl.LABEL_PART_MAIN, m_style("0x"..color[t.level].color))
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 5, 0)


        local label=lvgl.label_create(itencnt, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,string.format("#%s %s",color[t.level].color,t.text))
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 70, 0)

        local tClock = os.date("*t")
        local date=string.format("%02d:%02d:%02d",tClock.hour,tClock.min,tClock.sec)

        local label=lvgl.label_create(itencnt, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label,"#FFFFFF "..date)
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -10, 0)
        return itencnt
    end

    local massageTable={}

    function ss(obj,kill)
        sys.taskInit(function(obj)
            local step=7
            for i = 1, 35, step do
                if not _G.home_flag then return end
                lvgl.obj_set_pos(obj, lvgl.obj_get_x(obj),lvgl.obj_get_y(obj)+step) 
                sys.wait(5)
            end
            if kill then lvgl.obj_del(obj) end
        end,obj)
    end

    local addMessage=function(message)
        if #massageTable==0 then
            -- massageTable[1]=makeitem(msgBox)
            table.insert(massageTable,1,makeitem(msgBox,message,nil))
            return
        end

        local temp=massageTable[1]
        table.insert(massageTable,1,makeitem(msgBox,message,temp))
        for k, v in pairs(massageTable) do
            if k>=4 then
                ss(v,true)
                table.remove (massageTable,k)
            end
            ss(v)
        end

        
    end

    topicTableAdd("UI_ERROR",addMessage)


end

function Init()
    _G.home_flag=true
    lvgl.obj_clean(_G.body)
    makeLeftArea(_G.body)
    makeRightArea(_G.body)
    makeMidArea(_G.body)
end

function unInit()
    _G.home_flag=false
    print("取消订阅")
    for id, callback in pairs(topicTable) do
        print("---",id)
        sys.unsubscribe(id, callback)
    end
end
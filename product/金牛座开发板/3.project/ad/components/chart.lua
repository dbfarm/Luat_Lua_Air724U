module(..., package.seeall)

function create(p, v)
    obj = lvgl.chart_create(p, nil)
    lvgl.obj_set_click(obj, v.click)
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end
    -- 浮标个数， 修改线条中的点数。
    -- 默认值为10。注意：当将外部缓冲区分配给序列时，这也会影响处理的点数。
    lvgl.chart_set_point_count(obj, v.point_count)
    -- 分割线
    -- 水平和垂直分隔线的数量,默认设置为3条水平分割线和5条垂直分割线。
    -- 注意：不包括上下两条边的线，只有中间的分割线
    -- 例如范围是0-100，应设置横线9条，每条横线都在整数上，10，20
    -- 而竖线依据设置了多少个点，如果是20个点，则设置18
    if v.line_count then
        lvgl.chart_set_div_line_count(obj, v.line_count.hdiv_num,
                                      v.line_count.vdiv_num)
    end
    -- 在y方向上指定最小值和最大值。点的值将按比例缩放。默认范围是：0..100。
    -- lvgl.chart_set_y_range(lvgl.obj_t*chart, lvgl.chart_axis_taxis, 
    -- lvgl.coord_tymin, lvgl.coord_tymax)
    -- lvgl.CHART_AXIS_PRIMARY_Y
    -- lvgl.CHART_AXIS_SECONDARY_Y
    -- _lvgl.CHART_AXIS_LAST
    if v.range then
        lvgl.chart_set_y_range(obj, lvgl.CHART_AXIS_PRIMARY_Y, v.range.y_min,
                               v.range.y_max)
    end
    -- lvgl.chart_set_next 可以以两种方式运行，具体取决于更新模式：
    -- lvgl.CHART_UPDATE_MODE_SHIFT – 将旧数据向左移动，然后向右添加新数据。
    -- lvgl.CHART_UPDATE_MODE_CIRCULAR – 循环添加新数据（如ECG图）。
    -- lvgl.chart_set_update_mode(lvgl.obj_t*chart, lvgl.chart_update_mode_tupdate_mode)
    if v.update_mode then lvgl.chart_set_update_mode(obj, v.update_mode) end
    -- lvgl.CHART_TYPE_NONE – 不显示任何数据。它可以用来隐藏线条。
    -- lvgl.CHART_TYPE_LINE – 在两点之间画线。
    -- lvgl.CHART_TYPE_COLUMN – 绘制列。
    if v.type then lvgl.chart_set_type(obj, v.type) end
    -- 刻度线和标签
    -- 刻度和标签可以添加到轴上。
    -- lvgl.chart_set_x_tick_text(chart, list_of_values, num_tick_marks, lvgl.CHART_AXIS_...) 
    -- 设置x轴上的刻度和文本。 list_of_values 是一个字符串，带有 '\n' 终止文本（期望最后一个），
    -- 其中包含用于刻度的文本。例如。 const char * list_of_values = "first\nsec\nthird" 。 
    -- list_of_values 可以为 NULL 。 如果设置了 list_of_values ，则 num_tick_marks 告诉两个标签之间的刻度数。
    -- 如果 list_of_values 为 NULL ，则它指定滴答声的总数。
    -- 主刻度线绘制在放置文本的位置，次刻度线绘制在其他位置。 
    -- 不要画最后的刻度
    -- lvgl.CHART_AXIS_SKIP_LAST_TICK
    -- 画最后一个刻度
    --  lvgl.CHART_AXIS_DRAW_LAST_TICK
    -- 以倒序绘制刻度标签
    --  lvgl.CHART_AXIS_INVERSE_LABELS_ORDER
    -- lvgl.chart_set_x_tick_length(chart, major_tick_len, minor_tick_len) 设置x轴上刻度线的长度。
    -- y轴也存在相同的功能： lvgl.chart_set_y_tick_text 和 lvgl.chart_set_y_tick_length 。
    if v.x_tick then
        local value = v.x_tick
        lvgl.chart_set_x_tick_texts(obj, value.text, value.num, value.axis)
        lvgl.chart_set_x_tick_length(obj, value.major, value.minor)
    end
    if v.y_tick then
        local value = v.y_tick
        lvgl.chart_set_y_tick_texts(obj, value.text, value.num, value.axis)
        lvgl.chart_set_y_tick_length(obj, value.major, value.minor)
    end
    if v.sec_y_tick then
        local value = v.sec_y_tick
        lvgl.chart_set_secondary_y_tick_texts(obj, value.text, value.num,
                                              value.axis)
        lvgl.chart_set_secondary_y_tick_length(obj, value.major, value.minor)
    end
    --  向图表添加任意数量的线条。它为包含所选颜色的 lvgl.chart_u series_t
    --  结构分配数据，如果不使用外部数组，如果分配了外部数组，
    --  则与该线条关联的任何内部点都将被释放，而序列指向外部数组。
    local series = {}
    if v.series then
        for i = 1, #v.series do
            series[i] = {}
            series[i].obj = lvgl.chart_add_series(obj, lvgl.color_hex(
                                                      v.series[i].color))
            if v.series[i].start_point then
                lvgl.chart_set_x_start_point(obj, series[i].obj,
                                             v.series[i].start_point)
            end
            -- 可以使用以下函数从外部数据源更新图表线条： 
            if v.series[i].init_v then
                -- 使用 lvgl.chart_init_points(chart, ser, value) 将所有点初始化为给定值。
                lvgl.chart_init_points(obj, series[i].obj, v.series[i].init_v)
            end
            if v.series[i].array then
                -- -- 其中array是lvgl.coord_t与point_cnt元素的外部数组。
                -- -- 注意：更新外部数据源后，应调用 lvgl.chart_refresh(chart) 来更新图表。
                lvgl.chart_set_ext_array(obj, series[i].obj, v.series[i].array)
            end
            -- 使用 lvgl.chart_set_point_id(chart, ser, value, id) ,
            -- 其中id是您要更新的点的索引
            -- 单独设置某一点时，应调用 lvgl.chart_refresh(chart) 来更新图表。

            -- lvgl.chart_set_next
            -- lvgl.chart_set_next(chart, ser, value) 
        end
    end
    -- lvgl.chart_refresh                    (obj, aa)
    -- lvgl.chart_remove_series              (obj, aa)
    -- lvgl.chart_set_series_axis            (obj, aa)
    -- lvgl.chart_set_x_start_point          (obj, aa)
    if v.align_to or v.align or v.align_x or v.align_y then
        lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                       v.align_x or 0, v.align_y or 0)
    end
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    -- 图表的主要部分称为 lvgl.CHART_PART_BG ，它使用所有典型的背景属性。
    -- 文本样式属性确定轴文本的样式，而线属性确定刻度线的样式。
    -- 填充值在侧面增加了一些空间，因此使序列区域更小。填充也可用于为轴文本和刻度线留出空间。
    -- 该系列的背景称为 lvgl.CHART_PART_SERIES_BG ，它位于主要背景上。在此部分上绘制了分隔线和系列数据。
    -- 除典型的背景样式属性外，分割线还使用线型属性。填充值指示此零件与轴文本之间的间隔。
    -- lvgl.CHART_PART_SERIES 可以引用该系列的样式。对于列类型，使用以下属性：
    -- 半径：数据点的半径
    -- padding_inner：相同x坐标的列之间的间隔
    -- 如果是线型图，则使用以下属性：
    -- 线属性来描述线
    -- 点的大小半径
    -- bg_opa：线条下方区域的整体不透明度
    -- bg_main_stop：的％bg_opa在顶部以创建一个alpha褪色（0：在顶部透明，255：bg_opa在顶部）
    -- bg_grad_stop：底部bg_opa的百分比以创建alpha渐变（0：底部透明，255：bg_opa顶部）
    -- bg_drag_dir：应该 lvgl.GRAD_DIR_VER 允许通过bg_main_stop和bg_grad_stop进行Alpha淡入
    -- lvgl.CHART_PART_CURSOR 引用游标。可以添加任意数量的光标，并且可以通过与行相关的样式属性来设置其外观。
    -- 创建游标时设置游标的颜色，并用该值覆盖 line_color 样式。
    if v.style then
        lvgl.obj_add_style(obj, lvgl.CHART_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.CHART_PART_CURSOR, create_style(v.style.cursor))
        lvgl.obj_add_style(obj, lvgl.CHART_PART_SERIES,
                           create_style(v.style.series))
        lvgl.obj_add_style(obj, lvgl.CHART_PART_SERIES_BG,
                           create_style(v.style.series_bg))
    end

    return {obj = obj, series = series}
end
test_data = {
    {
        13, 18, 28, 35, 40, 49, 52, 55, 58, 60, 62, 66, 71, 76, 77, 78, 79, 81,
        83, 84, 85, 85, 85, 85, 85, 89, 89, 90, 89, 82, 77, 74, 72, 69, 67, 64,
        62, 60, 59, 57, 57, 55, 54, 53, 52, 51, 50, 48, 48, 47, 46, 44, 42, 41,
        39, 33, 24, 23, 20, 17, 16, 15, 11, 13, 18, 28, 35, 40, 49, 52, 55, 58,
        60, 62, 66, 71, 76, 77, 78, 79, 81, 83, 84, 85, 85, 85, 85, 85, 89, 89,
        90, 89, 82, 77, 74, 72, 69, 67, 64, 62, 60, 59, 57, 57, 55, 54, 53, 52,
        51, 50, 48, 48, 47, 46, 44, 42, 41, 39, 33, 24, 23, 20, 17, 16, 15, 11,
        13, 18, 28, 35, 40, 49, 52, 55, 58, 60, 62, 66, 71, 76, 77, 78, 79, 81,
        83, 84, 85, 85, 85, 85, 85, 89, 89, 90, 89, 82, 77, 74, 72, 69, 67, 64,
        62, 60, 59, 57, 57, 55, 54, 53, 52, 51, 50, 48, 48, 47, 46, 44, 42, 41,
        39, 33, 24, 23, 20, 17, 16, 15, 11, 13, 18, 28, 35, 40, 49, 52, 55, 58,
        60, 62, 66, 71, 76, 77, 78, 79, 81, 83, 84, 85, 85, 85, 85, 85, 89, 89,
        90, 89, 82, 77, 74, 72, 69, 67, 64, 62, 60, 59, 57, 57, 55, 54, 53, 52,
        51, 50, 48, 48, 47, 46, 44, 42, 41, 39, 33, 24, 23, 20, 17, 16, 15, 11
    }, {
        89, 82, 77, 74, 72, 69, 67, 64, 62, 60, 59, 57, 57, 55, 54, 53, 52, 51,
        50, 48, 48, 47, 46, 44, 42, 41, 39, 33, 24, 23, 20, 17, 16, 15, 11, 13,
        18, 28, 35, 40, 49, 52, 55, 58, 60, 62, 66, 71, 76, 77, 78, 79, 81, 83,
        84, 85, 85, 85, 85, 85, 89, 89, 90, 89, 82, 77, 74, 72, 69, 67, 64, 62,
        60, 59, 57, 57, 55, 54, 53, 52, 51, 50, 48, 48, 47, 46, 44, 42, 41, 39,
        33, 24, 23, 20, 17, 16, 15, 11, 13, 18, 28, 35, 40, 49, 52, 55, 58, 60,
        62, 66, 71, 76, 77, 78, 79, 81, 83, 84, 85, 85, 85, 85, 85, 89, 89, 90,
        89, 82, 77, 74, 72, 69, 67, 64, 62, 60, 59, 57, 57, 55, 54, 53, 52, 51,
        50, 48, 48, 47, 46, 44, 42, 41, 39, 33, 24, 23, 20, 17, 16, 15, 11, 13,
        18, 28, 35, 40, 49, 52, 55, 58, 60, 62, 66, 71, 76, 77, 78, 79, 81, 83,
        84, 85, 85, 85, 85, 85, 89, 89, 90, 89, 82, 77, 74, 72, 69, 67, 64, 62,
        60, 59, 57, 57, 55, 54, 53, 52, 51, 50, 48, 48, 47, 46, 44, 42, 41, 39,
        33, 24, 23, 20, 17, 16, 15, 11, 13, 18, 28, 35, 40, 49, 52, 55, 58, 60,
        62, 66, 71, 76, 77, 78, 79, 81, 83, 84, 85, 85, 85, 85, 85, 89, 89, 90
    }, {
        77, 74, 72, 69, 67, 64, 62, 60, 59, 57, 57, 55, 54, 53, 52, 51, 50, 48,
        48, 47, 46, 44, 42, 41, 39, 39, 39, 39, 33, 24, 23, 20, 17, 16, 16, 16,
        16, 15, 11, 13, 18, 28, 35, 40, 49, 52, 55, 58, 60, 62, 66, 71, 76, 77,
        78, 79, 81, 83, 84, 85, 85, 85, 85, 77, 74, 72, 69, 67, 64, 62, 60, 59,
        57, 57, 55, 54, 53, 52, 51, 50, 48, 48, 47, 46, 44, 42, 41, 39, 39, 39,
        39, 33, 24, 23, 20, 17, 16, 16, 16, 16, 15, 11, 13, 18, 28, 35, 40, 49,
        52, 55, 58, 60, 62, 66, 71, 76, 77, 78, 79, 81, 83, 84, 85, 85, 85, 85,
        77, 74, 72, 69, 67, 64, 62, 60, 59, 57, 57, 55, 54, 53, 52, 51, 50, 48,
        48, 47, 46, 44, 42, 41, 39, 39, 39, 39, 33, 24, 23, 20, 17, 16, 16, 16,
        16, 15, 11, 13, 18, 28, 35, 40, 49, 52, 55, 58, 60, 62, 66, 71, 76, 77,
        78, 79, 81, 83, 84, 85, 85, 85, 85, 77, 74, 72, 69, 67, 64, 62, 60, 59,
        57, 57, 55, 54, 53, 52, 51, 50, 48, 48, 47, 46, 44, 42, 41, 39, 39, 39,
        39, 33, 24, 23, 20, 17, 16, 16, 16, 16, 15, 11, 13, 18, 28, 35, 40, 49,
        52, 55, 58, 60, 62, 66, 71, 76, 77, 78, 79, 81, 83, 84, 85, 85, 85, 85
    }
}

chart_data = {
    W = LCD_W,
    H = LCD_H,
    click = false,
    range = {y_min = 0, y_max = 100},
    line_count = {hdiv_num = 9, vdiv_num = 18},
    point_count = #test_data[1],
    type = lvgl.CHART_TYPE_LINE,
    -- type = lvgl.CHART_TYPE_COLUMN,
    update_mode = lvgl.CHART_UPDATE_MODE_CIRCULAR,
    x_tick = {
        text = "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10",
        num = 10,
        axis = lvgl.CHART_AXIS_DRAW_LAST_TICK,
        major = 10,
        minor = 5
    },
    y_tick = {
        text = "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10",
        num = 10,
        axis = lvgl.CHART_AXIS_INVERSE_LABELS_ORDER,
        major = 10,
        minor = 5
    },
    -- sec_y_tick = {
    --     text = "0\n10\n20\n30\n40\n50\n60\n70\n80\n90\n100",
    --     num = 10,
    --     axis = lvgl.CHART_AXIS_INVERSE_LABELS_ORDER,
    --     major = 10,
    --     minor = 5
    -- },
    series = {
        {color = 0xff0000, _init_v = 0}, {color = 0x0000ff, _start_point = 50},
        {color = 0x00ff00, _array = test_data[3]}
    },
    style = {
        bg = {
            line = {
                width = 2,
                color = 0xff0000,
                opa = 100,
                gap = 10,
                rounded = false
            },
            border = {color = 0x0f0f0f, width = 2, opa = 30},
            shadow = {spread = 30, color = 0xff00f0},
            pad = {left = 50, bottom = 50}, -- 设置左，下两个方向的空隙留给坐标轴
            text = {font = style.font24, color = 0x00ff},
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            }
        },
        series = {
            bg = {
                grad = {
                    bg_grad_dir = lvgl.GRAD_DIR_VER,
                    color = 0xffffff,
                    main = 255,
                    grad = 0
                },
                color = 0xff00ff,
                opa = 150,
                W = 0,
                H = 0
            },
            pad = {inner = 0}, -- 列类型使用，设置每列之间的距离
            line = {width = 2, opa = 255, rounded = false}
        },
        series_bg = {
            pad = {left = 5, bottom = 5, right = 5},
            bg = {
                radius = 10,
                color = 0xff0ff0,
                grad = {color = 0x0fffff},
                opa = 150

            },
            line = {width = 1, opa = 100, gap = 10, rounded = false}
        }
    }

}
-- create(lvgl.scr_act(), chart_data)

function test()
    sys.taskInit(function()
        chart_obj = create(lvgl.scr_act(), chart_data)
        local series = chart_obj.series
        local obj = chart_obj.obj
        for i = 1, #test_data[1] do
            for j = 1, #series do
                -- lvgl.chart_set_point_id(obj, series[j].obj, test_data[j][i], i - 1)
                -- lvgl.chart_refresh(obj)
                lvgl.chart_set_next(obj, series[j].obj, test_data[j][i])
            end
            sys.wait(200)
        end
    end)
end

-- test()

module(..., package.seeall)

function create(p, v)
    local obj = lvgl.linemeter_create(p, nil)
    if v.click then lvgl.obj_set_click(obj, true) end
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end
    lvgl.linemeter_set_value(obj, 40)
    lvgl.linemeter_set_mirror(obj, true)
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    -- 线表只有一个主要部分，称为 LV_LINEMETER_PART_MAIN 。
    -- 它使用所有典型的背景属性绘制矩形或圆形背景，并使用line和scale属性绘制比例线。
    -- 活动行（与较小的值相关，即当前值）的颜色从line_color到scale_grad_color。
    -- 最后（当前值之后）的行设置为scale_end_color color。
    lvgl.obj_add_style(obj, lvgl.LINEMETER_PART_MAIN, create_style(v.style))

end
line_meter_data = {
    W = 200,
    H = 200,
    click = false,
    style = {
        border = {color = 0x0f0f0f, width = 2, opa = 30},
        shadow = {spread = 30, color = 0xff00f0},
        bg = {
            radius = 10,
            color = 0x0f0ff0,
            opa = 150,
            grad = {color = 0x0f0f0f}
        }
    }
}
-- create(lvgl.scr_act(), line_meter_data)

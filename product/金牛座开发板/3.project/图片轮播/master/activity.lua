module(..., package.seeall)
---------------------------

require "audio"
require "pins"

pins.setup(pio.P0_7,1)

t={
    {
        img="/lua/1.jpg",
        music="/lua/A.mp3",
    },
    {
        img="/lua/2.jpg",
        music="/lua/B.mp3",
    }
}

i=0


function padd()
    i=i+1
    i=i%2
    -- 设置图片显示的图像
    lvgl.img_set_src(index, t[i+1].img)
    playMusic(t[i+1].music)
end

sys.subscribe("end", padd)


function playMusic(src)
    audio.play(0,"FILE",src,1,function ()
        sys.publish("end")
    end)
end

function simulation()
    sys.taskInit(function()
        while true do
            i=i+1
            lvgl.img_set_src(index, t[i].img)
            sys.wait(2000)
            if i >= 2 then
                i = 0
            end
        end
    end)
end

function init()
    bg = lvgl.img_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(bg, 860,480)
    lvgl.img_set_src(bg,"/lua/bg.jpg")
-- 图片居中
    lvgl.obj_align(bg, nil, lvgl.ALIGN_CENTER, 0, 0)

    index=lvgl.img_create(bg, nil)
    lvgl.obj_set_size(index, 400,209)
    lvgl.obj_align(index, nil, lvgl.ALIGN_CENTER, 0, 0)
    if not lvgl.indev_get_emu_touch then
        padd()
    elseif lvgl.indev_get_emu_touch then
        simulation()
    end
end
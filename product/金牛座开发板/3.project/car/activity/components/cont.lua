module(..., package.seeall)

function create(p, v)
    c = lvgl.cont_create(p, nil)
    lvgl.obj_set_click(c, v.click or true)
    lvgl.obj_set_size(c, v.W, v.H)
    -- lvgl.obj_set_auto_realign(Titlecont, true)                   
    lvgl.obj_align(c, v.align_to or nil, v.align or lvgl.ALIGN_IN_TOP_MID,
                   v.align_x or 0, v.align_y or 0)
    lvgl.obj_add_style(c, lvgl.CONT_PART_MAIN, create_style(v.style))
    return c
end

function create_style(v)
    local s = lvgl.style_t()

    lvgl.style_init(s)

    lvgl.style_set_radius(s, lvgl.STATE_DEFAULT, v.radius or 0)
    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))

    lvgl.style_set_bg_grad_color(s, lvgl.STATE_DEFAULT,
                                 lvgl.color_hex(v.bg_grad_color or 0xFFFFFF))

    lvgl.style_set_bg_grad_dir(s, lvgl.STATE_DEFAULT,
                               v.bg_grad_dir or lvgl.GRAD_DIR_VER)

    lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color or 0xFFFFFF))

    lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.border_width or 0)
    lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.border_opa or 255)

    -- 透明度
    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.bg_opa or 255)

    -- 内边距
    lvgl.style_set_pad_left(s, lvgl.STATE_DEFAULT, v.pad_left or 0)
    lvgl.style_set_pad_right(s, lvgl.STATE_DEFAULT, v.pad_right or 0)
    lvgl.style_set_pad_top(s, lvgl.STATE_DEFAULT, v.pad_top or 0)
    lvgl.style_set_pad_bottom(s, lvgl.STATE_DEFAULT, v.pad_bottom or 0)
    lvgl.style_set_pad_inner(s, lvgl.STATE_DEFAULT, v.pad_inner or 0)

    -- 外边距
    lvgl.style_set_margin_left(s, lvgl.STATE_DEFAULT, v.margin_left or 0)
    lvgl.style_set_margin_right(s, lvgl.STATE_DEFAULT, v.margin_right or 0)
    lvgl.style_set_margin_top(s, lvgl.STATE_DEFAULT, v.margin_top or 0)
    lvgl.style_set_margin_bottom(s, lvgl.STATE_DEFAULT, v.margin_bottom or 0)
    return s
end

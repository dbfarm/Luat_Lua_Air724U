module(..., package.seeall)

function create_style(v)
    local s = lvgl.style_t()

    lvgl.style_init(s)

    lvgl.style_set_radius(s, lvgl.STATE_DEFAULT, v.radius or 0)
    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))
    -- lvgl.style_set_bg_grad_color (s, lvgl.STATE_DEFAULT, v.grad_color or 0xFFFFFF)
    -- lvgl.style_set_bg_grad_dir   (s, lvgl.STATE_DEFAULT, lvgl.GRAD_DIR_VER)
    -- lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
    --                             v.border_color or 0xFFFFFF)
    -- lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.border_width or 0)
    -- lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.border_opa or 255)

    -- -- 透明度
    -- lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.opa or 0)

    lvgl.style_set_text_font(s, lvgl.STATE_DEFAULT, v.font or style.font24)
    return s
end

function create(p, v)
    local mb = lvgl.msgbox_create(p, nil)
    lvgl.obj_set_size(mb, v.W or 400, v.H or 400)
    lvgl.obj_align(mb, v.align_to, v.align or lvgl.ALIGN_CENTER, v.align_x or 0,
                   v.align_y or 40)
    lvgl.msgbox_set_text(mb, v.msg_text or "消息框")
    lvgl.msgbox_set_anim_time(mb, v.anim_time or 1000)
    lvgl.msgbox_start_auto_close(mb, v.auto_close or 5000)

    lvgl.obj_add_style(mb, lvgl.MSGBOX_PART_MAIN, create_style(v.style))

    return mb
end


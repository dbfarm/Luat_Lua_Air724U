module(..., package.seeall)
require "http"

iconTable = {
    zz = "/lua/zuozhuan.bin",
    yz = "/lua/youzhuan.bin",
    pzz = "/lua/pianzuozhuan.bin",
    pyz = "/lua/pianyouzhuan.bin",
    zhz = "/lua/zuohouzhuan.bin",
    yhz = "/lua/youhouzhuan.bin",
    zzdt = "/lua/zuozhuandiaotou.bin",
    zx = "/lua/zhixing.bin"
}
-- 更新时间
function update_time()
    local t = os.date("*t")
    local dt = string.format("#FFFFFF %02d-%02d-%02d", t.year, t.month, t.day)

    lvgl.label_set_text(home_date, dt)
    lvgl.obj_align(home_date, nil, lvgl.ALIGN_IN_TOP_MID, -150, 30)

    dt = string.format("#FFFFFF %02d:%02d", t.hour, t.min)

    lvgl.label_set_text(home_time, dt)
    lvgl.obj_align(home_time, nil, lvgl.ALIGN_IN_TOP_MID, 0, 30)
end

_G.contentBodyBG = img.create(lvgl.scr_act(),
                              {click = false, src = "/lua/bgbg.jpg"})

_G.contentBox = cont.create(lvgl.scr_act(), {
    align = lvgl.ALIGN_CENTER,
    W = LCD_W,
    H = LCD_H,
    style = {bg_opa = 0}
})

-- labelx = label.create(contentBox, {
--     recolor = true,
--     align = lvgl.ALIGN_IN_TOP_MID,
--     text = "正在规划路线",
--     align_x = 0,
--     align_y = 70,
--     mode = lvgl.LABEL_LONG_EXPAND,
--     font = style.font24
-- })

cont_1 = cont.create(contentBox, {
    align = lvgl.ALIGN_CENTER,
    W = 150,
    H = 240,
    style = {radius = 30, bg_opa = 100}
})

tip_img = img.create(cont_1, {
    click = false,
    zoom = 256,
    src = "/lua/emp.bin",
    align = lvgl.ALIGN_CENTER,
    style = {opa = 255, recolor = 0x00cdFF}
})

road = label.create(cont_1, {
    recolor = true,
    align = lvgl.ALIGN_IN_TOP_MID,
    text = "正在定位",
    align_y = 15,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})

distance = label.create(cont_1, {
    recolor = true,
    align = lvgl.ALIGN_IN_BOTTOM_MID,
    text = "--米",
    align_x = 0,
    align_y = -15,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font48
})

_G.left_led = img.create(contentBox, {
    click = false,
    src = "/lua/left_led.bin",
    align = lvgl.ALIGN_IN_TOP_LEFT,

    style = {opa = 255, recolor = 0xcdcdcd},
    align_x = 40,
    align_y = 10
})

_G.right_led = img.create(contentBox, {
    click = false,
    src = "/lua/right_led.bin",
    style = {opa = 255, recolor = 0xcdcdcd},
    -- angle = 1800,
    align = lvgl.ALIGN_IN_TOP_RIGHT,
    align_x = -40,
    align_y = 10
})

home_date = label.create(contentBox, {

    recolor = true,
    align = lvgl.ALIGN_IN_TOP_MID,
    align_x = -150,
    align_y = 30,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})
home_weather = label.create(contentBox, {

    recolor = true,
    align = lvgl.ALIGN_IN_TOP_MID,
    text = "#FFFFFF 晴22摄氏度\n #FFFFFF上海",
    align_x = 150,
    align_y = 30,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})
home_time = label.create(contentBox, {

    recolor = true,
    align = lvgl.ALIGN_IN_TOP_MID,
    align_y = 30,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font68
})

-- 更新时间
sys.timerLoopStart(update_time, 1000)

_G.left_arc = arc.create(contentBox, {
    R = 360,
    align = lvgl.ALIGN_IN_LEFT_MID,
    align_y = 20,
    align_x = -10,
    rotation = 140,
    bg_start = 0,
    bg_end = 260,
    arc_start = 0,
    arc_end = 260,
    range_s = 0,
    range_e = 140,
    type = lvgl.ARC_TYPE_REVERSE,
    style = {
        bg = {
            bg_opa = 0,
            bg_color = 0x0099CC,
            line_color = 0x0099CC,
            border_width = 0,
            line_width = 30
        },
        indic = {line_color = 0xFF6666, line_width = 30}
    }
})

-- _G.right_arc = arc.create(contentBox, {
--     R = 360,
--     align = lvgl.ALIGN_IN_RIGHT_MID,
--     align_y = 20,
--     align_x = 8,
--     rotation = 145,
--     bg_start = 0,
--     bg_end = 250,
--     range_s = 10,
--     range_e = 40,
--     style = {
--         bg = {
--             bg_opa = 0,
--             bg_color = 0xFFFFFF,
--             line_color = 0x0000FF,
--             border_width = 0,
--             line_width = 30
--         },
--         indic = {line_color = 0x0000FF, line_width = 30}
--     }
-- })

_G.right_arc = arc.create(contentBox, {
    R = 360,
    align = lvgl.ALIGN_IN_RIGHT_MID,
    align_y = 20,
    align_x = 8,
    rotation = 140,
    bg_start = 0,
    bg_end = 60,
    range_s = 10,
    range_e = 40,
    value = 0,
    style = {
        bg = {
            bg_opa = 0,
            bg_color = 0xFF1111,
            line_color = 0x009999,
            line_opa = 255,
            border_width = 0,
            line_width = 30
        },
        indic = {line_color = 0xFF0033, line_opa = 255, line_width = 30}
    }
})

_G.right_arc_1 = arc.create(contentBox, {
    R = 360,
    align = lvgl.ALIGN_IN_RIGHT_MID,
    align_y = 20,
    align_x = 8,
    rotation = 140,
    bg_start = 60,
    bg_end = 120,
    range_s = 40,
    range_e = 70,
    style = {
        bg = {
            bg_opa = 0,
            bg_color = 0xFFFFFF,
            line_color = 0x009999,
            line_opa = 255,
            border_width = 0,
            line_width = 30
        },
        indic = {line_color = 0xFF6666, line_opa = 255, line_width = 30}
    }
})

_G.right_arc_2 = arc.create(contentBox, {
    R = 360,
    align = lvgl.ALIGN_IN_RIGHT_MID,
    align_y = 20,
    align_x = 8,
    rotation = 140,
    bg_start = 120,
    bg_end = 180,
    range_s = 70,
    range_e = 100,
    style = {
        bg = {
            bg_opa = 0,
            bg_color = 0xFFFFFF,
            line_color = 0x009999,
            line_opa = 255,
            border_width = 0,
            line_width = 30
        },
        indic = {line_color = 0xFFCCCC, line_opa = 255, line_width = 30}
    }
})

_G.right_arc_3 = arc.create(contentBox, {
    R = 360,
    align = lvgl.ALIGN_IN_RIGHT_MID,
    align_y = 20,
    align_x = 8,
    rotation = 140,
    bg_start = 180,
    bg_end = 240,
    range_s = 100,
    range_e = 130,
    style = {
        bg = {
            bg_opa = 0,
            bg_color = 0xFFFFFF,
            line_color = 0x009999,
            line_opa = 255,
            border_width = 0,
            line_width = 30
        },
        indic = {line_color = 0xFFFF00, line_opa = 255, line_width = 30}
    }
})
_G.right_arc_4 = arc.create(contentBox, {
    R = 360,
    align = lvgl.ALIGN_IN_RIGHT_MID,
    align_y = 20,
    align_x = 8,
    rotation = 140,
    bg_start = 240,
    bg_end = 260,
    range_s = 130,
    range_e = 140,
    style = {
        bg = {
            bg_opa = 0,
            bg_color = 0xFFFFFF,
            line_color = 0x009999,
            line_opa = 255,
            border_width = 0,
            line_width = 30
        },
        indic = {line_color = 0xFFF0F0, line_opa = 255, line_width = 30}
    }
})

left_label = label.create(left_arc, {
    recolor = true,
    align = lvgl.ALIGN_CENTER,
    text = "#FFFFFF 000",
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font68
})

left_label_1 = label.create(left_arc, {
    recolor = true,
    align = lvgl.ALIGN_OUT_BOTTOM_MID,
    align_to = left_label,
    text = "#F000F0 KM/H",
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})

right_label = label.create(right_arc, {

    recolor = true,
    align = lvgl.ALIGN_CENTER,
    text = "#FFFFFF 000",
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font68
})

right_label_1 = label.create(right_arc, {

    recolor = true,
    align = lvgl.ALIGN_OUT_BOTTOM_MID,
    align_to = right_label,
    text = "#00FFFF 剩余里程KM",
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})

function change_rate(rate)
    local label_rate
    if rate < 10 then
        label_rate = "00" .. rate
    elseif rate < 100 then
        label_rate = "0" .. rate
    else
        label_rate = rate
    end

    lvgl.label_set_text(left_label, "#FFFFFF " .. label_rate or "")
    lvgl.label_set_text(right_label, "#FFFFFF " .. label_rate or "")

    lvgl.obj_align(left_label, nil, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.obj_align(left_label, nil, lvgl.ALIGN_CENTER, 0, 0)

    lvgl.arc_set_value(left_arc, rate)

    lvgl.arc_set_value(right_arc, rate)
    lvgl.arc_set_value(right_arc_1, rate)
    lvgl.arc_set_value(right_arc_2, rate)
    lvgl.arc_set_value(right_arc_3, rate)
    lvgl.arc_set_value(right_arc_4, rate)
end

-- sys.subscribe("RATE", change_rate)

count = 0

-- sys.timerLoopStart(function()
--     count = count + 1
--     count = count % 100
--     sys.publish("RATE", count)
-- end, 10)
index = true

-- 切换转向灯
close_led = img.create_style({recolor = 0xcdcdcd})

open_led = img.create_style({recolor = 0xf4ea2a})

sys.taskInit(function()
    while true do
        change_rate(count)
        if index then
            count = count + 2
        else
            count = count - 2
        end
        if count >= 140 or count <= 0 then
            if index then
                lvgl.obj_add_style(left_led, lvgl.IMG_PART_MAIN, open_led)
                lvgl.obj_add_style(right_led, lvgl.IMG_PART_MAIN, open_led)
            else
                lvgl.obj_add_style(left_led, lvgl.IMG_PART_MAIN, close_led)
                lvgl.obj_add_style(right_led, lvgl.IMG_PART_MAIN, close_led)
            end
            index = not index

        end

        sys.wait(10)
    end

end)

home_label = label.create(contentBox, {
    recolor = true,
    align = lvgl.ALIGN_IN_BOTTOM_MID,
    text = "#FFCCCC 欢迎使用LuatOS电动车",
    align_y = -30,
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font68
})

home_left_label = label.create(contentBox, {

    recolor = true,
    align = lvgl.ALIGN_IN_BOTTOM_LEFT,
    text = "#FFFFFF 小计里程# #FF0000 0007# #FFFFFF KM",
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})

home_right_label = label.create(contentBox, {

    recolor = true,
    align = lvgl.ALIGN_IN_BOTTOM_RIGHT,
    text = "#FFFFFF 总里程# #0000F0 0996# #FFFFFF KM",
    mode = lvgl.LABEL_LONG_EXPAND,
    font = style.font24
})
function changeIcon(text)
    if tip_img then
        if text == "左转" then
            lvgl.img_set_src(tip_img, iconTable.zz)
        elseif text == "右转" then
            lvgl.img_set_src(tip_img, iconTable.yz)
        elseif text == "偏左转" then
            lvgl.img_set_src(tip_img, iconTable.pzz)
        elseif text == "偏右转" then
            lvgl.img_set_src(tip_img, iconTable.pyz)
        elseif text == "左后转" then
            lvgl.img_set_src(tip_img, iconTable.zhz)
        elseif text == "右后转" then
            lvgl.img_set_src(tip_img, iconTable.yhz)
        elseif text == "左转掉头" then
            lvgl.img_set_src(tip_img, iconTable.zzdt)
        else
            lvgl.img_set_src(tip_img, iconTable.zx)
        end
    end
end

_G.contents = {
    {
        content = "从起点朝东南,行进100米",
        img = "直行",
        rode = "道路A"
    }, {content = "前方50米右转", img = "右转", rode = "道路A"},
    {content = "左转,进入道路B", img = "左转", rode = "道路A"},
    {content = "前方200米右转", img = "直行", rode = "道路B"},
    {content = "前方50米左转", img = "左转", rode = "道路B"},
    {content = "前方200米偏左转", img = "偏左转", rode = "道路C"},
    {content = "前方50米偏左转", img = "偏左转", rode = "道路C"},
    {content = "前方200米偏右转", img = "偏右转", rode = "道路B"},
    {content = "前方50米偏右转", img = "偏右转", rode = "道路B"},
    {content = "前方200米偏右转", img = "偏右转", rode = "道路B"},
    {content = "前方50米偏右转", img = "偏右转", rode = "道路B"},
    {content = "前方200米偏右转", img = "偏右转", rode = "道路B"},
    {content = "前方50米偏右转", img = "偏右转", rode = "道路B"},
    {content = "前方100米到达终点", img = "直行", rode = "道路B"}, {
        content = "你已经在目的地附近,本次导航结束",
        img = "emp.bin",
        rode = "目的地"
    }
}
_G.rode_distance = 0

function change_content()
    rode_distance = rode_distance - 1
    if rode_distance < 1 then sys.publish("next_rode") end
    lvgl.label_set_text(distance, rode_distance .. "米" or "--米")
end

local test
function change_map(index)

    changeIcon(contents[index].img)
    lvgl.label_set_text(road, contents[index].rode)
    rode_distance = 5
    test = sys.timerLoopStart(change_content, 100)

end

sys.taskInit(function()
    for i = 1, #contents do
        change_map(i)
        sys.waitUntil("next_rode", 50000)
    end

    sys.timerStop(test)

end)

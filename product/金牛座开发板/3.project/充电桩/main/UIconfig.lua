module(..., package.seeall)

-- local temp={
--     empty={
--         img="/lua/empty.png",
--         status="#3292ff 空闲"
--     },
--     error={
--         img="/lua/error.png",
--         status="#f64040 故障"
--     },
--     ready={
--         img="/lua/ready.png",
--         status="#22b573 已插电"
--     },
--     pause={
--         img="/lua/pause.png",
--         status="#ffa81e 充电暂停"
--     },
--     lighting={
--         img="/lua/lighting.png",
--         status="#16ad4b 充电中"
--     },
--     ending={
--         img="/lua/end.png",
--         status="#d81e06 充电完成"
--     }
-- }

-- UI={
--     gun={
--         left={
--             img="/lua/empty.png",
--             status="空闲"
--         },
--         right={
--         	img="/lua/lighting.png",
--         	status="加油"
--         }
--     },

--     QR="http://openluat.com"
-- }


local msg={
    empty={
        img="/lua/empty.png",
        status="#3292ff 空闲"
    },
    error={
        img="/lua/error.png",
        status="#f64040 故障"
    },
    ready={
        img="/lua/ready.png",
        status="#22b573 已插电"
    },
    pause={
        img="/lua/pause.png",
        status="#ffa81e 充电暂停"
    },
    lighting={
        img="/lua/lighting.png",
        status="#16ad4b 充电中"
    },
    ending={
        img="/lua/end.png",
        status="#d81e06 充电完成"
    }
}

UI={
    gun={
        left={
            status="empty",
            para={
                V=229.1,
                A=32.1,
                P=6.95,
                USEDPOWER=7.023,
                USEDTIME=60,
                USEDMONEY=5.23
            }
        },
        right={
            status="lighting",
            para={
                V=239.1,
                A=32.1,
                P=7.95,
                USEDPOWER=8.565,
                USEDTIME=46,
                USEDMONEY=6.23
            }
        }
    },
    QR="https://hmi.wiki.luatos.com"
}

function getStatus(gun)
	return msg[gun.status].img, msg[gun.status].status
end

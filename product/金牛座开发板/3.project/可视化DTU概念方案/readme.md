[TOC]



# 使用硬件

* **722金牛座大屏开发板，RS485型温湿度传感器**

# 操作步骤

## 一.下载对应core

下载地址:https://doc.openluat.com/article/1334/0

固件使用大于等于3205且是RDA8910_BT_FLOAT这个版本

## 二. luatools配置脚本

![](https://cdn.openluat-luatcommunity.openluat.com/images/20211213170501101_4.png)

## 三.参数配置

### 一.基本参数

![](https://cdn.openluat-luatcommunity.openluat.com/images/20211213165112917_1.png)

### 二.串口参数

![](https://cdn.openluat-luatcommunity.openluat.com/images/20211213165205297_2.png)

### 三.网络通道参数

![](https://cdn.openluat-luatcommunity.openluat.com/images/20211213165213851_3.png)



### 四.数据流

* 发送数据流模板（处理接收到的传感器数据）

```lua
function 
    local str = ...
    local tem
        str = str:sub(6,7)
        a,str = pack.unpack(str,">h")
        num1 = str/10
        num2 = str%10
        tem = string.format( "%d.%d",num1,num2)
        log.info("温度",tem)
        sys.publish("ANIM_CHANGE",tem)
      return tem
end
```

* 接收数据流模板（服务器下发自定义指令获取传感器温湿度）

```lua
function 
    local str = ...
    if str== "aa" then--自定义指令
        uart.write(1,string.fromHex("010300000002C40B"))
    end
    return str
end
```



### 五.任务

* 任务一（UI）

```lua
function
    local Length = 854
    local Width = 480
-- local picture = {"#1E90FF .#", "#1E90FF ..#", "#1E90FF ...#"}
    local picture = {".", "..", "..."}
   local label = {}
   local k = 1
   local tem
   pm.wake("testUartTask")
   sys.timerStart(function() 
    lvgl.init(function()
    end, tp.input)
    lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)
    end,3000)
    sys.wait(3000)
    sys.publish("OPENLIANGT",true)
    style_white = lvgl.style_t()
    lvgl.style_init(style_white)
    lvgl.style_set_text_color(style_white,lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
    style_black = lvgl.style_t()
    lvgl.style_init(style_black)
    lvgl.style_set_text_color(style_black,lvgl.STATE_DEFAULT, lvgl.color_hex(0x000000))
    style_blue = lvgl.style_t()
    lvgl.style_init(style_blue)
    lvgl.style_set_text_color(style_blue,lvgl.STATE_DEFAULT, lvgl.color_hex(0x00477D))
    cont_style = lvgl.style_t()
    lvgl.style_init(cont_style)
    lvgl.style_set_radius(cont_style, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(cont_style, lvgl.CONT_PART_MAIN, 0)
    contain = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.disp_load_scr(contain)
    lvgl.obj_add_style(contain, lvgl.CONT_PART_MAIN, cont_style)
    lvgl.obj_set_size(contain, Length, Width)
    lvgl.obj_align(contain, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)
    -----------页面一-------------------
    img = lvgl.img_create(contain, nil)
    -- -- 设置图片显示的图像
    lvgl.img_set_src(img, "/lua/page1.jpg")
    -- -- 图片居中
    lvgl.obj_align(img, contain, lvgl.ALIGN_CENTER, 0, 0)

    local a = true
    label11 = lvgl.label_create(contain, nil)
    local font = lvgl.font_load("/lua/opposans_b_40.bin")
    for i = 1, 2 do
        for j = 1, 3 do
            sys.wait(500)
            lvgl.label_set_recolor(label11, true)
            lvgl.obj_add_style(label11,lvgl.LABEL_PART_MAIN,style_blue)
            lvgl.label_set_text(label11, tostring(picture[j]))
            lvgl.obj_set_width(label11, 100)

            lvgl.label_set_align(label11, lvgl.LABEL_ALIGN_CENTER)
            if a then
                lvgl.obj_align(label11, img, lvgl.ALIGN_IN_BOTTOM_MID, 8, -156)
                a = false
            end
            lvgl.obj_set_style_local_text_font(label11, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font)
        end
    end
    sys.wait(500)
    lvgl.obj_del(img)
    lvgl.obj_del(label11)
    ----------页面二--------------------
    local font2 = lvgl.font_load("/lua/opposans_b_20.bin")
    -- 横坐标
    local abs = {260, 489, 706}
    -- local ratioabs = {160, 259, 160,260}
    -- 纵坐标
    -- local ratioord = {160, 160, 221,221}
    local ord = {155, 267, 380}
    img2 = lvgl.img_create(contain, nil)
    -- -- 设置图片显示的图像
    lvgl.img_set_src(img2, "/lua/page2.jpg")
    -- -- 图片居中
    lvgl.obj_align(img2, contain, lvgl.ALIGN_CENTER, 0, 0)

    for i = 1, 4 do
        local ratioabs = {145, 242, 145,242}
        local ratioord = {150, 150, 210,210}
        for j = 1, 4 do
            if i==2 then
                ratioabs[j]= ratioabs[j] + 415
            end
            if i==3 then
                ratioord[j] = ratioord[j] + 197
            end
            if i==4 then
                ratioabs[j]= ratioabs[j] + 415
                ratioord[j] = ratioord[j] + 197
            end
            label[k] = lvgl.label_create(contain, nil)
            lvgl.obj_add_style(label[k],lvgl.LABEL_PART_MAIN,style_black)
            lvgl.label_set_recolor(label[k], true)
            -- lvgl.label_set_text(label[i], "#ffffff "..i.. "#\n #ffffff 20#\n #ffffff 5#\n #ffffff 80#")
            lvgl.label_set_text(label[k], i)
            lvgl.obj_set_width(label[k], 100)
            lvgl.label_set_align(label[k], lvgl.LABEL_ALIGN_CENTER)
            lvgl.obj_align(label[k], img2, lvgl.ALIGN_IN_TOP_LEFT, ratioabs[j], ratioord[j])
            lvgl.obj_set_style_local_text_font(label[k], lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font2)
            k = k+1
        end
    end

    while true do
        _,tem = sys.waitUntil("ANIM_CHANGE")
        local tem2 = math.random(40)
        lvgl.label_set_text(label[3], tem)
        lvgl.label_set_text(label[4], tem2)
    end

end
```

* 任务二（定时给传感器发送获取温湿度的指令）

```lua
function
    local UART_ID = 1
    while true do
        sys.wait(5000)
        a = string.fromHex("010300000002C40B")
        log.info("发送数据",a)
        uart.write(UART_ID,a)
    end
end
```



## 实现结果

在配置好DTU平台参数，烧录代码之后，用手握住485温湿度传感器，可以看到722金牛座大屏开发板上的温度与上传服务器端的数据一致且在小幅度上升

演示视频：https://cdn.openluat-luatcommunity.openluat.com/attachment/20211213171707176_bc8dfaa3f71cee6b0245e8236fe95545.mp4


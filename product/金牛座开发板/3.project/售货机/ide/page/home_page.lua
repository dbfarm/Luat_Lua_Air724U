module(..., package.seeall)

focu_menu = 1

focu_sttyle_data = {border = {width = 0}, bg = {radius = 20, opa = 80}}
normal_style_data = {border = {width = 0}, bg = {radius = 20, opa = 20}}

focu_sttyle = cont.create_style(focu_sttyle_data)
normal_style = cont.create_style(normal_style_data)

function create_list(v)
    drinks_data = v.drinks_data
    lvgl.obj_clean(right_cont_body)
    for i = 1, #drinks_data do
        if i == 1 then
            align_ = lvgl.ALIGN_IN_TOP_LEFT
            align_to_ = right_cont_body
        elseif i > 1 and i <= 5 then
            index = i - 1
            align_to_ = drinks_data[index].cont
            align_ = lvgl.ALIGN_OUT_RIGHT_MID
        elseif i > 5 then
            index = i - 5
            align_to_ = drinks_data[index].cont
            align_ = lvgl.ALIGN_OUT_BOTTOM_MID
        end
        drinks_data[i].cont = cont.create(right_cont_body, {
            W = LCD_W * 2 / 15,
            H = LCD_W * 2 / 15,
            align = align_,
            align_to = align_to_,
            click = true,
            event = function(obj, event)
                if (event == lvgl.EVENT_CLICKED) then
                    choose_page.create({goods_info_data = drinks_data[i]})
                end
            end,
            style = {
                border = {width = 3, color = 0xccffcc},
                bg = {radius = 0, color = 0xffffff}
            }
        })

        drinks_data[i].img = img.create(drinks_data[i].cont, {
            align = lvgl.ALIGN_IN_TOP_MID,
            src = "/lua/" .. drinks_data[i].src..".jpg"
        })

        drinks_data[i].price_label = label.create(drinks_data[i].cont, {
            text = "#ff0000 " .. drinks_data[i].price .. "元",
            align = lvgl.ALIGN_IN_BOTTOM_LEFT,
            recolor = true,
            align_x = 5,
            align_y = -5,
            style = {text = {font = style.font16}}
        })

        drinks_data[i].name_label = label.create(drinks_data[i].cont, {
            text = drinks_data[i].name,
            align = lvgl.ALIGN_OUT_TOP_LEFT,
            align_to = drinks_data[i].price_label,
            style = {text = {font = style.font16}}
        })

    end
end

function create()
    lvgl.obj_clean(right_cont)
    right_cont_head = cont.create(right_cont, {
        W = LCD_W * 2 / 3,
        H = LCD_H * 3 / 24,
        align = lvgl.ALIGN_IN_TOP_RIGHT,
        click = false,
        style = {border = {width = 0}, bg = {opa = 0}}
    })

    menu_cont = cont.create(right_cont_head, {
        W = LCD_W * 11 / 36,
        H = LCD_H * 3 / 24,
        align = lvgl.ALIGN_IN_RIGHT_MID,
        click = false,
        layout = lvgl.LAYOUT_PRETTY_MID,
        style = {
            border = {width = 0},
            pad = {left = 10, right = 10, top = 0, bottom = 0},
            bg = {radius = 20, color = 0x2471A3}
        }
    })

    menu_data = {
        {text = "全部", src = "quanbu", data = "all_goods"},
        {text = "零食", src = "lingshi", data = "all_foods"},
        {text = "饮料", src = "yinliao", data = "all_drinks"},
        {text = "其他", src = "qita", data = "other_goods"}
    }

    for i = 1, #menu_data do

        menu_data[i].cont = cont.create(menu_cont, {
            W = LCD_W * 2 / 36,
            H = LCD_H * 3 / 24,
            click = true,
            event = function(obj, event)
                if (event == lvgl.EVENT_CLICKED) then
                    if focu_menu then
                        lvgl.obj_add_style(menu_data[focu_menu].cont,
                                           lvgl.CONT_PART_MAIN, normal_style)
                    end
                    create_list({drinks_data = nvm.get(menu_data[i].data)})
                    focu_menu = i
                    lvgl.obj_add_style(menu_data[i].cont, lvgl.CONT_PART_MAIN,
                                       focu_sttyle)
                end
            end,
            style = i == 1 and focu_sttyle_data or normal_style_data
        })

        menu_data[i].img = img.create(menu_data[i].cont, {
            align = lvgl.ALIGN_IN_TOP_MID,
            align_y = 5,
            src = "/lua/" .. menu_data[i].src .. ".bin",
            style = {recolor = 0xffffff}
        })

        menu_data[i].label = label.create(menu_data[i].cont, {
            text = menu_data[i].text,
            -- recolor = true,
            align = lvgl.ALIGN_IN_BOTTOM_MID,
            align_y = -10,
            style = {text = {font = style.font18, color = 0xffffff}}
        })
    end

    right_cont_body = cont.create(right_cont, {
        W = LCD_W * 2 / 3,
        H = LCD_H * 17 / 24,
        align = lvgl.ALIGN_OUT_BOTTOM_MID,
        align_to = right_cont_head,
        click = false,
        style = {border = {width = 0}, bg = {color = 0xffffff}}
    })

    create_list({drinks_data = nvm.get("all_goods")})

    right_cont_foot = cont.create(right_cont, {
        W = LCD_W * 2 / 3,
        H = LCD_H * 2 / 12,
        align = lvgl.ALIGN_IN_BOTTOM_RIGHT,
        click = false,
        style = {border = {width = 0}, bg = {color = 0xccccff}}
    })

    foot_data = {
        {
            src = "quhuo.png",
            text = "点我取货",
            color = 0x66BB6A,

            align = lvgl.ALIGN_IN_LEFT_MID,
            event = pick_up_page.create
        }, {
            src = "xiai.bin",
            text = "点我帮助",
            color = 0x1E88E5,
            align = lvgl.ALIGN_CENTER,
            event = help_page.create
        }, {
            src = "huodong-copy.bin",
            text = "更多活动",
            color = 0xF44336,
            align = lvgl.ALIGN_IN_RIGHT_MID,
            event = help_page.create
        }
    }

    for i = 1, #foot_data do
        foot_data[i].cont = cont.create(right_cont_foot, {
            W = LCD_W * 2 / 9,
            H = LCD_H * 2 / 12,
            align = foot_data[i].align,
            click = true,
            event = function(obj, event)
                if (event == lvgl.EVENT_CLICKED) then
                    foot_data[i].event()
                end
            end,
            style = {
                border = {width = 0},
                bg = {opa = 150, radius = 20, color = foot_data[i].color}
            }
        })

        foot_data[i].img = img.create(foot_data[i].cont, {
            align = lvgl.ALIGN_IN_TOP_MID,
            align_y = 5,
            src = "/lua/" .. foot_data[i].src,
            style = {recolor = 0xffffff}
        })

        foot_data[i].label = label.create(foot_data[i].cont, {
            text = foot_data[i].text,
            align = lvgl.ALIGN_IN_BOTTOM_MID,
            align_y = -5,
            style = {text = {font = style.font18, color = 0xffffff}}
        })
    end

end

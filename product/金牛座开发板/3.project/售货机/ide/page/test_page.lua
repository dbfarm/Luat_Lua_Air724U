module(..., package.seeall)
_G.parent_cont = cont.create(lvgl.scr_act(), {
    W = LCD_W,
    H = LCD_H,
    click = false,
    style = {border = {width = 0}, bg = {color = 0xffcccc}}
})
cont_data = {}
row_num = 11
local align_ = lvgl.ALIGN_IN_TOP_LEFT
local align_to_ = parent_cont
local index
local default_bg_color = 0xffff
local active_bg_color = 0xff0000

default_bg_style = set_bg(nil, {radius = 0, color = default_bg_color})
active_bg_style = set_bg(nil, {radius = 0, color = active_bg_color})
for i = 1, 16 * row_num do
    if i > 1 and i <= row_num then
        -- 第一行位置
        index = i - 1
        align_to_ = cont_data[index].cont
        align_ = lvgl.ALIGN_OUT_RIGHT_MID
    elseif i > row_num then
        -- 其他行数据
        index = i - row_num
        align_to_ = cont_data[index].cont
        align_ = lvgl.ALIGN_OUT_BOTTOM_MID
    end
    cont_data[i] = {}
    cont_data[i].cont = cont.create(parent_cont, {
        W = LCD_W / row_num,
        H = LCD_W / row_num,
        align = align_,
        align_to = align_to_,
        align_x = i == 1 and 3 or 0,
        style = {
            border = {width = 1, color = 0xccffcc},
            bg = {radius = 0, color = default_bg_color}
        }
    })
end

l_data={{1,1,1,1}}

lvgl.obj_add_style(cont_data[40].cont, lvgl.CONT_PART_MAIN, active_bg_style)

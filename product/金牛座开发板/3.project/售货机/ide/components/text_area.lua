module(..., package.seeall)

function ta_event_cb(obj, event)

    if event == lvgl.EVENT_CLICKED then
        lvgl.keyboard_set_textarea(kb, ta)
    elseif event == lvgl.EVENT_INSERT then
        str = lvgl.event_get_data()
        print(str)
    end
end

function create_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 设置文字样式
    if v.text then s = set_text(s, v.text) end
    return s
end

function create(p, v)
    local obj = lvgl.textarea_create(p, nil)
    lvgl.obj_set_click(obj, v.click or true)
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end
    -- lvgl.obj_set_auto_realign(Titlecont, true)                   
    lvgl.obj_align(obj, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    lvgl.textarea_set_text(obj, v.text or "")
    if v.pwd then lvgl.textarea_set_pwd_mode(obj, true) end
    if v.one_line then lvgl.textarea_set_one_line(obj, true) end
    if v.cursor_hidden then lvgl.textarea_set_cursor_hidden(obj, true) end
    -- lvgl.obj_set_pos(obj, 5, 20)

    if v.event then lvgl.obj_set_event_cb(obj, v.event) end

    -- LV_LABEL_ALIGN_LEFT/CENTER/RIGHT
    lvgl.textarea_set_text_align(obj, v.text_align or lvgl.LABEL_ALIGN_CENTER)

    -- 添加样式
    -- TEXTAREA_PART_BG
    -- TEXTAREA_PART_CURSOR
    -- TEXTAREA_PART_EDGE_FLASH
    -- TEXTAREA_PART_PLACEHOLDER
    -- TEXTAREA_PART_SCROLLBAR
    lvgl.obj_add_style(obj, lvgl.TEXTAREA_PART_BG, create_style(v.style))

    -- kb = lvgl.keyboard_create(p, nil)

    -- lvgl.keyboard_set_textarea(kb, obj)
    -- lvgl.keyboard_set_cursor_manage(kb, true)

    return obj

end

-- create(lvgl.scr_act(), {})

module(..., package.seeall)

require "common_style"

require "style"

require "label"
require "cont"
require "btn"
require "img"
require "arc"
require "bar"

require "btn_matrix"
require "msg_box"
require "tb"
require "tab_view"

require "gauge"

require "calendar"

require "canvas"

require "slider"
require "switch"

require "signal"
require "qrcode"

require "text_area"
require "keyboard"
require "list"

require "page"
require "custom_msg"

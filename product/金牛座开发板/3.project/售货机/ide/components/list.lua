module(..., package.seeall)
function create()
    List1 = lvgl.list_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(List1, 150, 200)
    lvgl.obj_align(List1, Father, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
    lvgl.list_add_btn(List1, lvgl.SYMBOL_NEW_LINE, "星期一")
    btn1 = lvgl.list_add_btn(List1, lvgl.SYMBOL_DIRECTORY, "Open")
    lvgl.list_add_btn(List1, lvgl.SYMBOL_CLOSE, "Delete")
    lvgl.list_add_btn(List1, lvgl.SYMBOL_EDIT, "Edit")
    lvgl.list_add_btn(List1, lvgl.SYMBOL_USB, "USB")
    lvgl.list_add_btn(List1, lvgl.SYMBOL_GPS, "GPS")
    lvgl.list_add_btn(List1, lvgl.SYMBOL_STOP, "Stop")
    lvgl.list_add_btn(List1, lvgl.SYMBOL_VIDEO, "Video")

    Label1 = lvgl.label_create(lvgl.scr_act(), nil)
    lvgl.label_set_text(Label1,
                        "获取列表的文本:" .. lvgl.list_get_btn_text(btn1) ..
                            "\n获取标签对象:" ..
                            tostring(lvgl.list_get_btn_label(btn1)) ..
                            "\n获取图像对象:" ..
                            tostring(lvgl.list_get_btn_img(btn1)) ..
                            "\n从列表中按钮索引:" ..
                            tostring(lvgl.list_get_btn_index(List1, btn1)) ..
                            "\n获取列表中的按钮个数:" ..
                            lvgl.list_get_size(List1) ..
                            "\n获取列表的布局:" ..
                            tostring(lvgl.list_get_layout(List1)) ..
                            "\n获取列表的滚动条模式:" ..
                            lvgl.list_get_scrollbar_mode(List1) ..
                            "\n获取滚动传播属性:" ..
                            tostring(lvgl.list_get_scroll_propagation(List1)) ..
                            "\n获取滚动传播属性:" ..
                            tostring(lvgl.list_get_edge_flash(List1)) ..
                            "\n获取滚动动画持续时间:" ..
                            lvgl.list_get_anim_time(List1))
    lvgl.obj_align(Label1, nil, lvgl.ALIGN_IN_TOP_LEFT, 200, 0)

end
-- create()

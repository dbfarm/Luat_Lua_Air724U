PROJECT = "components"
VERSION = "1.0.1"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"

require "log"
require "sys"
require "http"
require "misc"
require "pins"
require "mipi_lcd_GC9503"
-- require "mipi"
-- require "color_lcd_spi_st7789"
require "tp"

require "config"
require "nvm"
require "symbol"
require "testUart1"

_G.URL = "127.0.0.1"

nvm.init("config.lua")

lvgl.init(function()

    if lvgl.indev_get_emu_touch then
        _G.LCD_W, _G.LCD_H = lvgl.disp_get_lcd_info()
    else
        lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)
        _G.LCD_H, _G.LCD_W = lvgl.disp_get_lcd_info()
    end

    spi.setup(spi.SPI_1, 1, 1, 8, 5000000, 1)
    -- io.mount(io.SDCARD)
    -- tp.input
end, tp.input)

require "components"

require "keycfg"

require "home"

sys.init(0, 0)
sys.run()

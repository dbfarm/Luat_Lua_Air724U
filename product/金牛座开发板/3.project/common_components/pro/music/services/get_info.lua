require "http"

local function cbFnc(result, prompt, head, body)
    log.info("testHttp.cbFnc result", result)
    log.info("testHttp.cbFnc prompt", prompt)
    if result and head then
        for k, v in pairs(head) do
            log.info("testHttp.cbFnc head", k .. ": " .. v)
        end
    end
    if result and body then
        log.info("testHttp.cbFnc", "boby:" .. body, "bodyLen=" .. body:len())
        s = json.decode(body)
        for k, v in pairs(s.data[1]) do print(k, v) end
    end
end

local url_str = "127.0.0.1"

http.request("GET", url_str .. "/song/url?id=347230", nil, nil, nil, nil, cbFnc)

-- http.request("GET","www.lua.org",nil,nil,nil,30000,cbFnc)
-- http.request("GET","http://www.lua.org",nil,nil,nil,30000,cbFnc)
-- http.request("GET","http://www.lua.org:80",nil,nil,nil,30000,cbFnc,"download.bin")
-- http.request("GET","www.lua.org/about.html",nil,nil,nil,30000,cbFnc)
-- http.request("GET","www.lua.org:80/about.html",nil,nil,nil,30000,cbFnc)
-- http.request("GET","http://wiki.openluat.com/search.html?q=123",nil,nil,nil,30000,cbFnc)
-- http.request("POST","www.test.com/report.html",nil,{Head1="ValueData1"},"BodyData",30000,cbFnc)
-- http.request("POST","www.test.com/report.html",nil,{Head1="ValueData1",Head2="ValueData2"},{[1]="string1",[2] ={file="/ldata/test.jpg"},[3]="string2"},30000,cbFnc)
-- http.request("GET","https://www.baidu.com",{caCert="ca.crt"})
-- http.request("GET","https://www.baidu.com",{caCert="ca.crt",clientCert = "client.crt",clientKey = "client.key"})
-- http.request("GET","https://www.baidu.com",{caCert="ca.crt",clientCert = "client.crt",clientKey = "client.key",clientPassword = "123456"})

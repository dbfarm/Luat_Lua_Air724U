module(..., package.seeall)
--------------
require"ril"
require"ntp"

-- --bTimeSyned ：时间是否已经成功同步过
local bTimeSyned=false

--注册基站时间同步的URC消息处理函数
ril.regUrc("+NITZ", function()    
    log.info("cell.timeSync") 
    bTimeSyned = true
end)

local weekTable={"日","一","二","三","四","五","六"}

sys.taskInit(function()
    while not bTimeSyned do
        sys.wait(50)
    end
    while true do
        local tClock = os.date("*t")
        local date=string.format("%d-%02d-%02d %02d:%02d",tClock.year,tClock.month,tClock.day,tClock.hour,tClock.min)
        -- print(date)
        sys.publish("SYSTEM_CHANGE_TIME",date)
        sys.wait(500)
    end
end)

module(..., package.seeall)

require "pins"

local A_9V_OK=pins.setup(pio.P0_10)
local B_9V_OK=pins.setup(pio.P0_24)

local A_6V_OK=pins.setup(pio.P0_9)
local B_6V_OK=pins.setup(pio.P0_23)

local function openPWM(p)
    local pin = (p == "A" and 12 or 27)
    log.info("PWM 打开", p)
    pio.pin.pwm(pin, 533, 467, -1)
end

local function hightPWM(p)
    local pin = (p == "A" and 12 or 27)
    log.info("PWM 关闭", p)
    pio.pin.pwm(pin, 1000, 0, -1)
end

local function OK_9V(p)
    local pin=(p == "A" and A_9V_OK() or B_9V_OK())
    -- print(pin==1 and true or false)
    return pin==1 and true or false
end

local function OK_6V(p)
    local pin=(p == "A" and A_6V_OK() or B_6V_OK())
    -- print(pin==1 and true or false)
    return pin==1 and true or false
end

local function charge(p)
    log.info(p,"合闸充电")
end

local function getUsedPower(p)
    local cmd=(p == "A" and "cmdA" or "cmdB")
    log.info("查询",p,"使用的电量")
    local usedower=getPower(cmd) or nil --重写此方法
    return usedower
end


local function getElectricCurrent(p)
    local cmd=(p == "A" and "cmdA" or "cmdB")
    log.info("查询",p,"瞬时电流")
    local e=getElectric(cmd) or nil --重写此方法
    return e
end


gun = {
    A = {
        money=0,
        name="gun_A",
        power=true,
        UIstat=UIconfig.UI.gun.left,
        -- status=UIconfig.gun.left.status,
        charge=function() charge("A") end,
        OK_9V=function() return OK_9V("A") end,
        OK_6V=function() return OK_6V("A") end,
        -- OK_6V=function() return true end,
        -- OK_9V=function() return true end,
        usedPower=function() return getUsedPower("A") end,
        electric=function() return getElectricCurrent("A") end,
        pwm = {
            open = function() openPWM("A") end,
            high = function() hightPWM("A") end
        }
    },
    B = {
        money=0,
        name="gun_B",
        power=false,
        UIstat=UIconfig.UI.gun.right,
        charge=function() charge("B") end,
        OK_9V=function() return OK_9V("B") end,
        OK_6V=function() return OK_6V("B") end,
        usedPower=function() return getUsedPower("B") end,
        electric=function() return getElectricCurrent("A") end,
        pwm = {
            open = function() openPWM("B") end,
            high = function() hightPWM("B") end
        }
    }
}

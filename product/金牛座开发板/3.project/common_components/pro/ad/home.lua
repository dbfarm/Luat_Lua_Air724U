module(..., package.seeall)
_G.home_cont = cont.create(lvgl.scr_act(), {
    W = LCD_W,
    H = LCD_H,
    click = false,
    style = {border = {width = 0}, bg = {color = 0xffcccc}}
})
label_data = {
    W = 200,
    H = 100,
    text = "123456789012345678901234567890",
    mode = lvgl.LABEL_LONG_SROLL,
    align = lvgl.ALIGN_CENTER,
    style = {
        pad = {all = 0},
        border = {color = 0xff0000, width = 40, opa = 255},
        shadow = {spread = 100, width = 100, color = 0x00ff00},
        outline = {color = 0x0000ff, opa = 255, width = 40},
        bg = {
            radius = 0,
            color = 0xccffcc,
            opa = 250,
            grad = {color = 0xffffff, main = 60, grad = 300}
        },
        text = {
            font = style.font96,
            color = 0,
            letter_space = 0,
            line_space = 20
        }
    }
}

label.create(home_cont, label_data)

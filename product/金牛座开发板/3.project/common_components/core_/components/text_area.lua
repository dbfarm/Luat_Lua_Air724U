module(..., package.seeall)

require "pinyin"

function ta_event_cb(obj, event)

    if event == lvgl.EVENT_CLICKED then
        lvgl.keyboard_set_textarea(kb, ta)
    -- elseif (event == lvgl.EVENT_INSERT) then
    --     hh = lvgl.textarea_get_text(obj)
    --     print("in" .. hh)
    elseif (event == lvgl.EVENT_VALUE_CHANGED) then
        hh = lvgl.textarea_get_text(obj)
        print("change-" .. hh)
        gg = pinyin.getPinyin(hh)
        print(gg)
    end
end

function create(p, v)
    local obj = lvgl.textarea_create(p, nil)
    lvgl.obj_set_click(obj, v.click or true)
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end
    -- lvgl.obj_set_auto_realign(Titlecont, true)                   
    lvgl.obj_align(obj, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    lvgl.textarea_set_text(obj, v.text or "")
    if v.pwd then lvgl.textarea_set_pwd_mode(obj, true) end
    if v.one_line then lvgl.textarea_set_one_line(obj, true) end
    if v.cursor_hidden then lvgl.textarea_set_cursor_hidden(obj, true) end
    -- lvgl.obj_set_pos(obj, 5, 20)

    if v.event then lvgl.obj_set_event_cb(obj, v.event) end

    -- LV_LABEL_ALIGN_LEFT/CENTER/RIGHT
    lvgl.textarea_set_text_align(obj, v.text_align or lvgl.LABEL_ALIGN_CENTER)

    -- 添加样式
    -- TEXTAREA_PART_BG
    -- TEXTAREA_PART_CURSOR
    -- TEXTAREA_PART_EDGE_FLASH
    -- TEXTAREA_PART_PLACEHOLDER
    -- TEXTAREA_PART_SCROLLBAR
    if v.style then
        lvgl.obj_add_style(obj, lvgl.TEXTAREA_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.TEXTAREA_PART_CURSOR,
                           create_style(v.style.cursor))
        lvgl.obj_add_style(obj, lvgl.TEXTAREA_PART_EDGE_FLASH,
                           create_style(v.style.edge_flash))
        lvgl.obj_add_style(obj, lvgl.TEXTAREA_PART_PLACEHOLDER,
                           create_style(v.style.placeholder))
        lvgl.obj_add_style(obj, lvgl.TEXTAREA_PART_SCROLLBAR,
                           create_style(v.style.bar))
    end

    -- kb = lvgl.keyboard_create(p, nil)

    -- lvgl.keyboard_set_textarea(kb, obj)
    -- lvgl.keyboard_set_cursor_manage(kb, true)

    return obj

end

local textarea_data = {
    event = ta_event_cb,
    style = {
        bg = {bg = {color = 0xff0000, opa = 100}},
        cursor = {bg = {color = 0x00ff00}},
        edge_flash = {bg = {color = 0x0000ff}},
        bar = {bg = {color = 0xcccccc}},
        placeholder = {
            bg = {color = 0},
            value = {str = "ssssssssssssssssssssss"}
        }

    }
}
-- create(lvgl.scr_act(), textarea_data)

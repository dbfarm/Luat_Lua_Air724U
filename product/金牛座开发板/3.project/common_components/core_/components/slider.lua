module(..., package.seeall)

function create(p, v)

    obj = lvgl.slider_create(p, nil)
     if v.click then
        lvgl.obj_set_click(obj, true)
    else
        lvgl.obj_set_click(obj, false)
    end
    if v.H and v.W then lvgl.obj_set_size(obj, v.W, v.H) end
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    -- 值和范围
    -- 要设置初始值，请使用lvgl.slider_set_value(slider, new_value,lvgl.ANIM_ON/OFF) 。
    -- lvgl.slider_set_anim_time(slider, anim_time) 设置动画时间（以毫秒为单位）。
    -- 要指定范围（最小，最大值），可以使用lvgl.slider_set_range(slider, min , max) 。
    lvgl.slider_set_range(obj, v.range_s or 0, v.range_e or 100)
    -- 对称范围
    -- 除普通类型外，滑块还可以配置为两种其他类型：
    -- lvgl.SLIDER_TYPE_NORMAL 普通型
    -- lvgl.SLIDER_TYPE_SYMMETRICAL 将指标对称地绘制为零（从零开始，从左到右）
    -- lvgl.SLIDER_TYPE_RANGE 允许为左（起始）值使用附加旋钮。 （可与lvgl.slider_set/get_left_value() 一起使用）
    -- 可以使用lvgl.slider_set_type(slider,lvgl.SLIDER_TYPE_...) 更改类型
    if v.type then lvgl.slider_set_type(obj, v.type) end
    -- 仅旋钮模式
    -- 通常，可以通过拖动旋钮或单击滑块来调整滑块。在后一种情况下，旋钮移动到所单击的点，并且滑块值相应地变化。
    -- 在某些情况下，希望将滑块设置为仅在拖动旋钮时做出反应。
    -- 通过调用lvgl.obj_set_adv_hittest(slider, true); 启用此功能。
    if v.hittest then lvgl.obj_set_adv_hittest(obj, true) end
    -- 除了 通用事件 ，滑杆还支持以下 特殊事件 ：
    -- lvgl.EVENT_VALUE_CHANGED 在使用键拖动或更改滑块时发送。
    -- 拖动滑块时（仅当释放时）连续发送事件。使用lv_slider_is_dragged确定滑块是被拖动还是刚刚释放。
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    -- lvgl.obj_set_event_cb(obj, v.cb or event_handler)
    if v.style then
        lvgl.obj_add_style(obj, lvgl.SLIDER_PART_BG,
                           create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.SLIDER_PART_INDIC,
                           create_style(v.style.indic))
        lvgl.obj_add_style(obj, lvgl.SLIDER_PART_KNOB,
                           create_style(v.style.knob))
    end
    return obj
end
slider_data = {
    W = 300,
    H = 100,
    style = {
        bg = {
            outline = {width = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 10, color = 0xff0000, grad = {color = 0x0f0f0f}}
        },
        indic = {
            outline = {width = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 10, color = 0x00ff00, grad = {color = 0x0f0f0f}}
        },
        knob = {
            pad = {all = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 50, color = 0x0000ff, grad = {color = 0x0f0f0f}}
        }
    }
}
-- create(lvgl.scr_act(), slider_data)

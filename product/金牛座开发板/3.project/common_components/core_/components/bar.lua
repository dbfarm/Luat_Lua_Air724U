module(..., package.seeall)

function create(p, v)
    local obj = lvgl.bar_create(p, nil)

    -- 动画的时间以毫秒为单位。先设置动画时间，才能生效
    lvgl.bar_set_anim_time(obj, v.anim_time or 10000)
    -- 设置进度条的范围，默认是1-100
    if v.range then lvgl.bar_set_range(obj, v.range.min, v.range.max) end

    -- 设置进度条的值
    if v.value then
        lvgl.bar_set_value(obj, v.value.value, v.value.anim or lvgl.ANIM_ON)
    end

    -- 设置进度条的起始值
    if v.start_value then
        lvgl.bar_set_start_value(obj, v.start_value.value,
                                 v.start_value.anim or lvgl.ANIM_ON)
    end
    -- BAR_TYPE_CUSTOM
    -- BAR_TYPE_NORMAL
    -- BAR_TYPE_SYMMETRICAL 对称地绘制为零（从零开始，从左至右绘制）
    -- 没看出效果
    lvgl.bar_set_type(obj, v.type or lvgl.BAR_TYPE_NORMAL)

    lvgl.obj_set_size(obj, v.W or 400, v.H or 40)

    if v.style then
        -- 进度条的主要部分称为 lvgl.BAR_PART_BG ，它使用典型的背景样式属性。
        -- lvgl.BAR_PART_INDIC 是一个虚拟部件，还使用了所有典型的背景属性。
        -- 默认情况下，指示器的最大尺寸与背景的尺寸相同，
        -- 但是在其中设置正的填充值 lvgl.BAR_PART_BG 将使指示器变小。
        -- （负值会使它变大）如果在指标上使用了值样式属性，
        -- 则将根据指标的当前大小来计算对齐方式。
        -- 例如，中心对齐的值始终显示在指示器的中间，而不管其当前大小如何。
        lvgl.obj_add_style(obj, lvgl.BAR_PART_BG,
                           create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.BAR_PART_INDIC,
                           create_style(v.style.indic))
    end

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    return obj
end

bar_data = {
    anim_time = 1000,
    range = {min = 0, max = 1000},
    value = {value = 800, anim = lvgl.ANIM_ON},
    start_value = {value = 300, anim = lvgl.ANIM_ON},
    style = {
        bg = {
            outline = {width = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 50, color = 0xff0000, grad = {color = 0x0f0f0f}}
        },
        indic = {
            outline = {width = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            bg = {radius = 10, color = 0x00ff00, grad = {color = 0x0f0f0f}}
        }
    }
}
-- create(lvgl.scr_act(), bar_data)

module(..., package.seeall)

function create(p, v)
    local obj = lvgl.img_create(p, nil)
    if v.H and v.W then lvgl.obj_set_size(obj, v.W, v.H) end
    -- 缩放不可用
    if v.zoom then lvgl.img_set_zoom(obj, v.zoom) end
    lvgl.img_set_src(obj, "/lua/" .. v.src)
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end
    if v.pivot_x and v.pivot_y then
        lvgl.img_set_pivot(obj, v.pivot_x, v.pivot_y)
    end
    if v.page_glue then lvgl.page_glue_obj(obj, true) end
    if v.angle then lvgl.img_set_angle(obj, v.angle) end
    lvgl.obj_set_auto_realign(obj, true)

    -- if v.click then
    --     lvgl.obj_set_click(obj, true)
    -- else
    --     lvgl.obj_set_click(obj, false)
    -- end
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    lvgl.obj_add_style(obj, lvgl.IMG_PART_MAIN, create_style(v.style))
    lvgl.obj_align(obj, v.align_to or p, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    return obj
end

-- local img_data = {src = "bg_5.jpg"}

-- create(lvgl.scr_act(), img_data)


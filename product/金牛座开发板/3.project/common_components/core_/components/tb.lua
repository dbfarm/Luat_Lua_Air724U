module(..., package.seeall)

-- 表格点击事件回调
function event_handler(obj, event)
    res, row, col = lvgl.table_get_pressed_cell(obj)
    if (event == lvgl.EVENT_CLICKED) then
        log.info("table", event)

        log.info("table", row, col)
    end
end

function create(p, v)
    -- 创建一个 Table
    local obj = lvgl.table_create(p, nil)
    if v.click then
        lvgl.obj_set_click(obj, true)
    else
        lvgl.obj_set_click(obj, false)
    end

    -- 设置 Table 的行数
    lvgl.table_set_row_cnt(obj, #v.content_array[1])
    -- 设置 Table 的列数
    lvgl.table_set_col_cnt(obj, #v.content_array)

    lvgl.obj_set_event_cb(obj, v.cb or event_handler)

    -- 设置单元格内容与第几种样式样式
    -- 第一种为默认样式，不特殊设置样式的单元格，默认为第一样式
    for i = 1, #v.content_array do
        for j = 1, #v.content_array[i] do
            lvgl.table_set_cell_value(obj, j - 1, i - 1,
                                      v.content_array[i][j].text)
            lvgl.table_set_cell_type(obj, j - 1, i - 1,
                                     v.content_array[i][j].type)
        end
    end

    -- 设置 Table 的显示大小
    -- lvgl.obj_set_size(obj, v.W or 150, v.H or 60)
    -- 设置 Table 的位置
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    -- 添加样式
    if v.style then
        if v.style.cell then
            for i = 1, #v.style.cell do
                lvgl.obj_add_style(obj, v.style.cell[i].part,
                                   create_style(v.style.cell[i].style))
            end
        end

        if v.style.bg then
            lvgl.obj_add_style(obj, lvgl.TABLE_PART_BG, create_style(v.style.bg))
        end
    end

    return obj
end

local tb_array = {
    {
        {text = "阵营", type = 1}, {text = "博派", type = 4},
        {text = "狂派", type = 3}, {text = "狂派", type = 3},
        {text = "狂派", type = 3}, {text = "狂派", type = 3},
        {text = "狂派", type = 3}, {text = "狂派", type = 3},
        {text = "狂派", type = 3}

    }, {
        {text = "首领", type = 1}, {text = "擎天柱", type = 4},
        {text = "威震天", type = 3}, {text = "威震天", type = 3},
        {text = "威震天", type = 3}, {text = "威震天", type = 3}

    }, {
        {text = "哈哈哈哈", type = 2}, {text = "哈哈哈哈", type = 2},
        {text = "哈哈哈哈", type = 2}, {text = "哈哈哈哈", type = 2},
        {text = "哈哈哈哈", type = 2}, {text = "哈哈哈哈", type = 2}

    }, {
        {text = "哈哈哈哈", type = 2}, {text = "哈哈哈哈", type = 2},
        {text = "哈哈哈哈", type = 2}, {text = "哈哈哈哈", type = 2},
        {text = "哈哈哈哈", type = 2}, {text = "哈哈哈哈", type = 2}

    }
}

local tb_data = {
    W = 200,
    H = 300,
    content_array = tb_array,
    style = {
        cell = {
            {
                part = lvgl.TABLE_PART_CELL1,
                style = {
                    bg = {
                        radius = 10,
                        color = 0x0f0ff0,
                        opa = 150,
                        grad = {color = 0x0f0f0f}
                    },
                    text = {font = style.font24}
                }
            }, {
                part = lvgl.TABLE_PART_CELL2,
                style = {
                    bg = {
                        radius = 10,
                        color = 0xf0f000,
                        opa = 150,
                        grad = {color = 0xff0f0f}
                    },
                    text = {font = style.font24}
                }
            }, {
                part = lvgl.TABLE_PART_CELL3,
                style = {
                    bg = {
                        radius = 10,
                        color = 0xff0ff0,
                        opa = 150,
                        grad = {color = 0x0f0f0f}
                    },
                    text = {font = style.font24}
                }
            }, {
                part = lvgl.TABLE_PART_CELL4,
                style = {
                    bg = {
                        radius = 10,
                        color = 0x00fff0,
                        opa = 150,
                        grad = {color = 0x0f0f0f}
                    },
                    text = {font = style.font24}
                }
            }
        },
        bg = {
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            }
        }
    }

}

local page_data = {
    W = LCD_W,
    H = LCD_H,
    mode = lvgl.SCRLBAR_MODE_ON,
    style = {
        bg = {
            bg = {color = 0xff0000},
            pad = {all = 0},
            border = {color = 0xff0000, width = 40, opa = 255},
            shadow = {spread = 100, width = 100, color = 0x00ff00},
            outline = {color = 0x0000ff, opa = 255, width = 40}
        },
        lable = {bg = {color = 0x00ff00}},
        edge_flash = {bg = {color = 0x0000ff}},
        bar = {bg = {W = 10, H = 10, color = 0}}
    }
}

-- local test_page = page.create(lvgl.scr_act(), page_data)

-- tb.create(test_page, tb_data)

-- sys.timerStart(function() lvgl.obj_del(test_page) end, 5000)

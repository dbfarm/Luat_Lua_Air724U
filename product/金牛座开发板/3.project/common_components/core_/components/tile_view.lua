module(..., package.seeall)

function create(p, v)
    obj = lvgl.tileview_create(p, nil)
    -- if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end
    -- if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    -- if v.click then lvgl.obj_set_click(obj, v.click) end

    label_data = {
        text = "1234567890",
        -- align = lvgl.ALIGN_CENTER,
        style = {
            border = {color = 0x0ff00f, width = 10, opa = 200},
            shadow = {spread = 10, width = 10, color = 0xff00f0},
            line = {color = 0xafccfc, width = 30, opa = 255, rounded = false},
            outline = {color = 0xff0000, opa = 100, width = 20},
            bg = {
                radius = 50,
                color = 0x0f0ff0,
                opa = 250,
                grad = {color = 0x0f0f0f, main = 60, grad = 300}
            },
            text = {
                -- font = font50,
                color = 0xff0000,
                letter_space = 0,
                line_space = 20
            }
        }
    }

    local valid_pos = {{x = 0, y = 0}, {x = 0, y = 2}}

    lvgl.tileview_set_valid_positions(obj, valid_pos)
    lvgl.tileview_set_edge_flash(obj, true)
    local tile_list = {}
    -- for i = 1, #valid_pos do
    --     table.insert(tile_list, lvgl.obj_create(obj, nil))
    --     lvgl.obj_set_size(tile_list[i], 400, 400)
    --     lvgl.tileview_add_element(obj, tile_list[i])
    -- end
    tile1 = lvgl.obj_create(obj, nil)
    lvgl.obj_set_size(tile1, 400, 400)
    
    lvgl.tileview_add_element(obj, tile1)
    tile2 = lvgl.obj_create(obj, nil)
    lvgl.obj_set_size(tile2, 400, 400)
    lvgl.obj_align(tile2, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 400, v.align_y or 400)
    lvgl.tileview_add_element(obj, tile2)
    tile3 = lvgl.obj_create(obj, nil)
    lvgl.obj_set_size(tile3, 400, 400)
    lvgl.obj_align(tile3, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 400)
    lvgl.tileview_add_element(obj, tile3)

    if v.style then
        lvgl.obj_add_style(obj, lvgl.TILEVIEW_PART_BG,
                           create_style(v.style.main))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_EDGE_FLASH,
                           create_style(v.style.edge_flash))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_SCROLLABLE,
                           create_style(v.style.lable))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_SCROLLBAR,
                           create_style(v.style.bar))
    end

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

end

-- local cont_data = {
--     W = 400,
--     H = 400,
--     click = false,
--     style = {
--         border = {color = 0x0f0f0f, width = 2, opa = 30},
--         shadow = {spread = 30, color = 0xff00f0},
--         bg = {
--             radius = 10,
--             color = 0xff00ff,
--             opa = 250
--             -- grad = {color = 0x0f0f0f}
--         }
--     }
-- }
-- local cont_obj = cont.create(lvgl.scr_act(), cont_data)

-- create(cont_obj, {
--     style = {
--         -- main = {bg = {radius = 10, color = 0x00ffff, opa = 150}},
--         -- bg = {bg = {radius = 10, color = 0x00ffff, opa = 150}},
--         -- scrollable = {bg = {radius = 10, color = 0x0f0ff0, opa = 255}},
--         -- indic = {bg = {radius = 10, color = 0x00ff00, opa = 255}}
--     }
-- })

-- function tileview()

--     lvgl.obj_clean(lvgl.scr_act())

--     pos = {{x = 0, y = 0}, {x = 0, y = 2}}

--     t1 = lvgl.tileview_create(lvgl.scr_act(), nil)
--     lvgl.obj_set_size(t1, 854,480)
--     lvgl.tileview_set_valid_positions(t1, pos)
--     -- lvgl.tileview_set_valid_positions(t1, pos, 1)
--     tc1 = lvgl.obj_create(t1, nil)
--     lvgl.obj_set_size(tc1, 854,480)
--     lvgl.tileview_add_element(t1, tc1)
--     lvgl.obj_align(t1, nil, lvgl.ALIGN_IN_LEFT_MID, 0, 0)
--     label = lvgl.label_create(tc1, nil)

--     lvgl.label_set_text(label, "小白测试dfjsak123#! @&*")
--     lvgl.obj_align(label, nil, lvgl.ALIGN_CENTER, 0, 0)
--     tc2 = lvgl.list_create(t1, nil)
--     lvgl.obj_set_size(tc2, 854,480)
--     lvgl.obj_align(tc2, t1, lvgl.ALIGN_IN_TOP_MID, 0, 300)
--     lvgl.list_set_scroll_propagation(tc2, false)
--     lvgl.list_set_scrollbar_mode(tc2, lvgl.SCROLLBAR_MODE_DRAG)
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_DIRECTORY, "测试one")
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_USB, "测试two")
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_GPS, "测试three")
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_STOP, "测试four")
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_VIDE0, "测试five")
--     lvgl.list_add_btn(tc2, nil, "测试six")
--     lvgl.list_add_btn(tc2, nil, "测试seven")

--     t2 = lvgl.tileview_create(lvgl.scr_act(), t1)
--     lvgl.obj_align(t2, nil, lvgl.ALIGN_IN_RIGHT_MID, 0, 0)
-- end

-- function tileview_s()

--     lvgl.obj_clean(lvgl.scr_act())

--     pos = lvgl.point_t()
--     pos.x = 0
--     pos.y = 0

--     t1 = lvgl.tileview_create(lvgl.scr_act(), nil)
--     lvgl.obj_set_size(t1, 400, 400)
--     lvgl.tileview_set_valid_positions(t1, pos, 1)
--     tc1 = lvgl.obj_create(t1, nil)
--     lvgl.obj_set_size(tc1, 400, 300)
--     lvgl.tileview_add_element(t1, tc1)
--     lvgl.obj_align(t1, nil, lvgl.ALIGN_IN_LEFT_MID, 0, 0)
--     label = lvgl.label_create(tc1, nil)

--     lvgl.label_set_text(label, "小白测试dfjsak123#! @&*")
--     lvgl.obj_align(label, nil, lvgl.ALIGN_CENTER, 0, 0)
--     tc2 = lvgl.list_create(t1, nil)
--     lvgl.obj_set_size(tc2, 300, 400)
--     lvgl.obj_align(tc2, t1, lvgl.ALIGN_IN_TOP_MID, 0, 300)
--     lvgl.list_set_scroll_propagation(tc2, false)
--     lvgl.list_set_scrollbar_mode(tc2, lvgl.SCROLLBAR_MODE_DRAG)
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_DIRECTORY, "测试one")
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_USB, "测试two")
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_GPS, "测试three")
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_STOP, "测试four")
--     lvgl.list_add_btn(tc2, lvgl.SYMBOL_VIDE0, "测试five")
--     lvgl.list_add_btn(tc2, nil, "测试six")
--     lvgl.list_add_btn(tc2, nil, "测试seven")

--     t2 = lvgl.tileview_create(lvgl.scr_act(), t1)
--     lvgl.obj_align(t2, nil, lvgl.ALIGN_IN_RIGHT_MID, 0, 0)
-- end
-- tileview()

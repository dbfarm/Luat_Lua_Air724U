module(..., package.seeall)

function create(p, v)

    line1 = lvgl.line_create(lvgl.scr_act())
    lvgl.line_set_points(line1, {
        {x = 5, y = 5}, {x = 70, y = 70}, {x = 120, y = 10}, {x = 180, y = 60},
        {x = 240, y = 10}
    })
    lvgl.obj_align(line1, nil, lvgl.ALIGN_CENTER, 0, -300)
    -- lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
    --                v.align_x or 0, v.align_y or 0)
    -- if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    -- 生产线只有一个主要部分，称为 lvgl.LABEL_PART_MAIN 。它使用所有线型属性。
    -- lvgl.obj_add_style(obj, lvgl.LINE_PART_MAIN, create_style(v.style))

end

create(lvgl.scr_act(), {
    W = 100,
    H = 100,
    points = {
        {x = 5, y = 5}, {x = 70, y = 70}, {x = 120, y = 10}, {x = 180, y = 60},
        {x = 240, y = 10}
    },
    style = {width = 2, color = 0x00ff00, opa = 255}
})


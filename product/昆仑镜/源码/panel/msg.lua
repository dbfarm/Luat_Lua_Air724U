-- 关机提示 页面
module(..., package.seeall)

function keyExitShort() 
    airui.show("menu")
end

function keyPowerShort() 
    log.info("msg", "关机")
    rtos.poweroff()
end

function keyOkShort() 
    log.info("msg", "关机")
    rtos.poweroff()
end

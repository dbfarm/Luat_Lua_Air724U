-- 日期页面
module(..., package.seeall)

-- 日期字符串
function cldStr()
    local t = misc.getClock() or {year=2021, month=12,day=12,hour=12,min=12,sec=12}
    return  string.format("%02d:%02d", t.hour, t.min),
            string.format("%04d年%02d月%02d日", t.year, t.month, t.day),
            tools.nongLi(t.year, t.month, t.day)
end

-- 显示日期
function showTxt()
    page.cldTime, page.cldDate, page.cldNL = cldStr()
end

function exit()
    sys.timerStop(showTxt)
end

function enter()
    showTxt()
    sys.timerLoopStart(showTxt, 2000)
end

-- 占卜 页面
module(..., package.seeall)

-- 算一卦
function haveAction()
    shakeEnd()
    local t = tools.oracle()
    for i = 1, page.orcN do page["orc" .. i] = t[i] end
end

local count = 0
local bgtime = 0
-- 晃动力度
local shakeF = 5
-- 晃动次数
local shakeN = 10
-- 晃动时间
local shakeT = 5

shakeImgT = 0
function shakeImg()
    local t = {
        -200, -100, 0,  100,  
        200,  100, 0, -100, 
    }
    shakeImgT = shakeImgT % #t
    shakeImgT = shakeImgT + 1
    page.oracleAngle = t[shakeImgT]
    
end

function shakeBg()
    shakeImgT = 0
    page.oracleOpa = 128
    page.oracleBg = page.src.shake
    for i = 1, page.orcN do page["orc" .. i] = "" end
    sys.timerLoopStart(shakeImg, 40)
    if tools.testMode then
        sys.timerStart(haveAction, 3000)
    end
end

function shakeEnd()
    page.oracleAngle = 0
    page.oracleOpa = 50
    page.oracleBg = page.src.oracle
    sys.timerStop(shakeImg)
end

function run()
    local x, y, z = myiic.getGyro()
    local ax, ay, az = myiic.getGyro()
    local mx, my, mz = myiic.getGyro()
    if not x then return end
    if x > shakeF or y > shakeF or z > shakeF then
        if (bgtime - os.time() < shakeT) then
            count = count + 1
        else
            count = 0
        end
        bgtime = os.time()
    else
        log.info("x, y, z", x, y, z)
    end
    if count > shakeN then
        haveAction()
        count = 0
    end
end

function enter()
    shakeBg()
    sys.timerLoopStart(run, 50)
end

function keyOkShort()   
    shakeBg()
end

function exit() 
    shakeEnd()
    sys.timerStop(run) 
end


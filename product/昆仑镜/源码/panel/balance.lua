-- 平衡页面
module(..., package.seeall)

-- 采集的晃动值的上下限
local qq = nvm.get("qq") or 3000

-- 滤波
function fof(a)
    local fofA = a or 0.01
    local fofLast = 0
    return function(n)
        n = n or 0
        local ret = n*fofA+fofLast*(1-fofA)
        fofLast = n
        return ret
    end
end

-- 向量
function vector(a)
    -- 加速度
    local va = a or 0.05
    local now = 0
    return function(n)
        now = range(now + n * va, -qq, qq)
        return now
    end
end

-- 生成数据处理函数
function getFunc(avgA, fofA, vA)
    -- 数据平滑处理
    local f1 = fof(fofA)
    -- 是否有加速度
    local f2 = vector(vA)
    return function(n)
        return mapping(f2(f1(n)))
    end
end

-- 别让球滚出去
function range(n, min, max)
    -- 只判断了方形
    return math.max(math.min(n, max), min)
end

function mapping(n)
    min = 0
    max = 220
    min2 = -qq
    max2 = qq
    n = range(n, min2, max2)
    return min+(max-min)*(n-min2)/(max2-min2)
end

funcX = getFunc()
funcY = getFunc()

function run()
    local y, x = myiic.getAccel()
    if not (x and y) then 
        log.info("iic", "读取失败")
        return 
    end
    page.balanceBallX, page.balanceBallY = funcX(-x), funcY(y)
end

function enter()  
    page.balanceBallX, page.balanceBallY = 120, 120
    if tools.testMode then
        return
    end
    sys.timerLoopStart(run, 50)
end

function exit() 
    sys.timerStop(run)
end

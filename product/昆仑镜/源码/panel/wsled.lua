-- 彩灯 页面
module(..., package.seeall)
-- led 灯
local timerId = nil

-- 当前模式
local modeNow = 1

-- 当前状态时间
local tm = 1

-- 各种亮法

function modeFunc1(r, g, b)
    tm = tm + 1
    tm = tm % 9
    local t = {0, 0, 0}
    local one = {g, r, b}
    tools.ledFlush({
        tm > 0 and one or t, 
        tm > 1 and one or t, 
        tm > 2 and one or t, 
        tm > 3 and one or t, 
        tm > 4 and one or t, 
        tm > 5 and one or t, 
        tm > 6 and one or t, 
        tm > 7 and one or t, 
    })
end

function modeFunc2(t1, t2)
    tm = tm + 1
    tm = tm % 10
    if tm==0 then
        tools.ledFlush({
            t1, t2, t1, t2, t1, t2, t1, t2, 
        })
    elseif tm==5 then
        tools.ledFlush({
            t2, t1, t2, t1, t2, t1, t2, t1, 
        })
    end
end

function modeFunc3(i)
    tm = tm + 1
    tm = tm % 20
    if tm%2==1 then
        local t
        if i==1 then
            t = {tm*10, 0, 0}
        elseif i==2 then
            t = {0, tm*10, 0}
        else
            t = {0, 0, tm*10}
        end
        tools.ledFlush({
            t, t, t, t, t, t, t, t
        })
    end
end

function modeFunc4(one)
    tm = tm + 1
    tm = tm % 8
    local t = {0, 0, 0}
    tools.ledFlush({
        tm==0 and one or t, 
        tm==1 and one or t, 
        tm==2 and one or t, 
        tm==3 and one or t, 
        tm==4 and one or t, 
        tm==5 and one or t, 
        tm==6 and one or t, 
        tm==7 and one or t, 
    })
end

function modeFunc5(t, ltm)
    if not ltm then
        tm = tm + 1
        ltm = tm
    else
        ltm = ltm + 1
    end
    ltm = ltm % 8
    tools.ledFlush({
        t[(ltm+1)%8+1],
        t[(ltm+2)%8+1],
        t[(ltm+3)%8+1],
        t[(ltm+4)%8+1],
        t[(ltm+5)%8+1],
        t[(ltm+6)%8+1],
        t[(ltm+7)%8+1],
        t[(ltm+8)%8+1],
    })
end

-- 慢速
function modeFunc6(t)
    tm = tm + 1
    tm = tm % 64
    if tm%8==0 then
        local i = math.floor(tm/8)
        tools.ledFlush({
            t[(i+1)%8+1],
            t[(i+2)%8+1],
            t[(i+3)%8+1],
            t[(i+4)%8+1],
            t[(i+5)%8+1],
            t[(i+6)%8+1],
            t[(i+7)%8+1],
            t[(i+8)%8+1],
        })
    end
end

function begin()
    for tm=0, 7 do
        tm = tm + 1
        tm = tm % 9
        local t = {0, 0, 0}
        tools.ledFlush({
            tm > 0 and {0  , 0  , 200} or t, 
            tm > 1 and {0  , 200, 0}   or t, 
            tm > 2 and {200, 0  , 0}   or t, 
            tm > 3 and {100, 100, 0}   or t, 
            tm > 4 and {100, 0  , 100} or t, 
            tm > 5 and {0  , 10 , 100} or t, 
            tm > 6 and {50 , 50 , 100} or t, 
            tm > 7 and {100, 100, 50}  or t, 
        })
        sys.wait(100)
    end
end

-- 模式
local mode = {
    function()
        modeFunc2({0, 200, 0}, {0, 0, 200})
    end,
    function()
        modeFunc2({0, 200, 0}, {200, 0, 0})
    end,
    function()
        modeFunc2({200, 0, 0}, {0, 0, 200})
    end,
    function()
        modeFunc3(1)
    end,
    function()
        modeFunc3(2)
    end,
    function()
        modeFunc3(3)
    end,
    function()
        modeFunc4({0, 200, 0})
    end,
    function()
        modeFunc4({200, 0, 0})
    end,
    function()
        modeFunc4({0, 0, 200})
    end,
    function()
        modeFunc1(200, 0, 0)
    end,
    function()
        modeFunc1(0, 200, 0)
    end,
    function()
        modeFunc1(0, 0, 200)
    end,
    function()
        tm = tm + 1
        tm = tm % 9
        local t = {0, 0, 0}
        tools.ledFlush({
            tm > 0 and {0  , 0  , 200} or t, 
            tm > 1 and {0  , 200, 0}   or t, 
            tm > 2 and {200, 0  , 0}   or t, 
            tm > 3 and {100, 100, 0}   or t, 
            tm > 4 and {100, 0  , 100} or t, 
            tm > 5 and {0  , 10 , 100} or t, 
            tm > 6 and {50 , 50 , 100} or t, 
            tm > 7 and {100, 100, 50}  or t, 
        })
    end,
    function()
        local a = {200, 0, 0}
        local b = {0, 0, 0}
        modeFunc5({
            a, a, a, a, b, b, b, b,
        })
    end,
    function()
        local a = {0, 200, 0}
        local b = {0, 0, 0}
        modeFunc5({
            a, a, a, a, b, b, b, b,
        })
    end,
    function()
        local a = {0, 0, 200}
        local b = {0, 0, 0}
        modeFunc5({
            a, a, a, a, b, b, b, b,
        })
    end,
    function()
        local a = {0, 0, 200}
        local b = {0, 200, 0}
        modeFunc5({
            a, a, a, a, b, b, b, b,
        })
    end,
    function()
        local a = {200, 0, 0}
        local b = {0, 200, 0}
        modeFunc5({
            a, a, a, a, b, b, b, b,
        })
    end,
    function()
        local a = {200, 0, 0}
        local b = {0, 0, 200}
        modeFunc5({
            a, a, a, a, b, b, b, b,
        })
    end,
    function()
        modeFunc5({
            {0  , 0  , 200},
            {0  , 200, 0}  ,
            {200, 0  , 0}  ,
            {100, 100, 0}  ,
            {100, 0  , 100},
            {0  , 10 , 100},
            {50 , 50 , 100},
            {100, 100, 50} ,
        })
    end,
}

-- 亮
function light()
    page.ledMode = "模式"..tools.n2n(modeNow)
    if timerId then
        sys.timerStop(timerId)
        timerId = nil
    end
    timerId = sys.timerLoopStart(mode[modeNow], 100)
end

-- 灭
function unlight()
    if timerId then
        sys.timerStop(timerId)
        timerId = nil
    end
    tools.ledClose()
end

function keyUpShort()  
    modeNow = modeNow - 1
    if modeNow < 1 then
        modeNow = #mode
    end
    light()
end

function keyDownShort()  
    modeNow = modeNow + 1
    if modeNow > #mode then
        modeNow = 1
    end
    light()
end

function enter()  
    timerId = nil
    modeNow = 1
    tm = 1
    light()
end

function exit() 
    unlight()
end
